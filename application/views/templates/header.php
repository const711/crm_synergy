<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <title><?php echo $title ?></title>
        <link rel="stylesheet" type="text/css" href="<?= base_url("css/bootstrap.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url("css/style.css"); ?>?t=<?php echo(microtime(true)); ?>">
        <?php if (isset($other_css)): ?>
            <?php foreach ($other_css as $filename): ?>
                <link rel="stylesheet" type="text/css" href="<?= base_url($filename); ?>?t=<?php echo(microtime(true)); ?>">
            <?php endforeach; ?>
        <?php endif; ?>
        <script type="text/javascript" src="<?= base_url('js/jquery-1.12.1.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('js/bootstrap.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('js/main.js'); ?>?t=<?php echo(microtime(true)); ?>"></script>
        <?php if (isset($other_js)): ?>
            <?php foreach ($other_js as $filename): ?>
                <script type="text/javascript" src="<?= base_url($filename); ?>?t=<?php echo(microtime(true)); ?>"></script>
            <?php endforeach; ?>
        <?php endif; ?>
        <script type="text/javascript">
            var base_url = '<?= base_url(); ?>';
        </script>
    </head>
    <body>
        <div class="wrapper">
            <div class="container_project">
                <header>
                    <div class="top_menu">
                        <nav class="nav_top_menu">
                            <ul class="top_menu_links">
                                <li <?php if (($this->uri->segment(1) == 'search_detail') or ( empty($this->uri->segment(1)))) { ?>class="active"<?php } ?>>
                                    <a href="<?= base_url('search_detail/index'); ?>">Поиск по коду</a>
                                </li>
                                <li <?php if ($this->uri->segment(1) == 'orders_widget') { ?>class="active"<?php } ?>>
                                    <a href="<?= base_url('orders_widget/list_orders'); ?>">Список сделок</a>
                                </li>
                            </ul>
                        </nav>
                        <div class="cart-btn">
                            <a href="<?= base_url('shopping_cart/list_details_in_cart'); ?>">
                                <img src="<?= base_url('images/cart_big.gif'); ?>">
                            </a>
                            <?php if (isset($count_rows_details_shopping_cart)) { ?>
                                <?php if ($count_rows_details_shopping_cart > 0) { ?>
                                    <span class="count_rows_details_cart">
                                <?php } else { ?>
                                    <span class="count_rows_details_cart hide_counter">
                                <?php } ?>
                                    <?= $count_rows_details_shopping_cart ?>
                                </span>
                            <?php } ?>
                        </div>
                        <div class="body_search_form_top">
                            <?php echo form_open('search_detail/search_detail_top_form', array('id' => 'search_form_top', 'class' => 'search_form_top', 'autocomplete' => 'off')); ?>
                            <div class="input-group">
                                <input type="search" name="article_detail" class="form-control" placeholder="Поиск кода">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit" name="search_form_top"><span class="glyphicon glyphicon-search"></span></button>
                                </span>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="nickname_row">
                        <div class="nickname">Здравствуйте,
                            <strong><?= $this->session->userdata['manager_data']['last_name'] . ' ' . $this->session->userdata['manager_data']['name'] ?></strong>
                            (<?= $this->session->userdata['manager_data']['email'] ?> )
                            <a class="auth-form__a" href="<?= base_url('authorization/logout') ?>">Выйти</a>
                        </div>
                    </div>
                </header>