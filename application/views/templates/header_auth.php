<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <title><?php echo $title ?></title>
        <link rel="stylesheet" type="text/css" href="<?= base_url("css/bootstrap.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url("css/style.css"); ?>">
        <?php if (isset($other_css)): ?>
            <?php foreach ($other_css as $filename): ?>
                <link rel="stylesheet" type="text/css" href="<?= base_url($filename); ?>">
            <?php endforeach; ?>
        <?php endif; ?>
        <script type="text/javascript" src="<?= base_url('js/jquery-1.12.1.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('js/bootstrap.js'); ?>"></script>
        <?php if (isset($other_js)): ?>
            <?php foreach ($other_js as $filename): ?>
                <script type="text/javascript" src="<?= base_url($filename); ?>"></script>
            <?php endforeach; ?>
        <?php endif; ?>
        <script type="text/javascript">
            var base_url = '<?= base_url(); ?>';
        </script>
    </head>
    <body>
        <div class="wrapper">
            <div class="container_project_auth">