<div>
    <div class="div_form_login_project">
        <?php echo form_open('', array('id' => 'form_login_project')); ?>
            <div class="form-group">
                <label for="email_login">Email</label>
                <input type="email" class="form-control" id="email_login" name="email_login" placeholder="Email" value="<?php if(isset($email_login)){echo $email_login;} ?>">
            </div>
            <div class="form-group">
                <label for="password_login">Пароль</label>
                <input type="password" class="form-control" id="password_login" name="password_login" placeholder="Password" value="<?php if(isset($password_login)){echo $password_login;} ?>">
            </div>
            <div class="form_error">
                <?php if (isset($error_mass)) { ?>
                    <?= $error_mass ?>
                <?php } ?>
            </div>
            <button type="submit" class="btn btn-success">Войти</button>
        </form>
    </div>
</div>