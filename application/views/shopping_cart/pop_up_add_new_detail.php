<div class="modal fade" id="pop_up_add_new_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Добавление новой детали</h4>
            </div>
            <div class="modal-body">
                <div class="modal_add_new_detail_body">
                    <?php echo form_open('', array('id' => 'form_add_new_detail')); ?>
                    <div class="main_form_error form_error"></div>
                    <br>
                    <div class="form-group">
                        <label for="article">Артикул</label>
                        <input type="text" class="form-control" id="article" name="article" placeholder="Артикул">
                        <span class="form_error article_form_error"></span>
                    </div>
                    <div class="form-group">
                        <label for="brand">Бренд</label>
                        <input type="text" class="form-control" id="brand" name="brand" placeholder="Бренд">
                        <span class="form_error brand_form_error"></span>
                    </div>
                    <div class="form-group">
                        <label for="name">Название</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Название">
                        <span class="form_error name_form_error"></span>
                    </div>
                    <div class="form-group">
                        <label for="provider">Поставщик</label>
                        <select class="form-control"  name="provider" id="provider">
                            <option value=''>Выберите поставщика</option>
                            <?php if (!empty($list_providers)) { ?>
                                <?php foreach ($list_providers as $provider) { ?>
                                    <option value="<?= $provider['id'] ?>"><?= $provider['name'] ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <span class="form_error provider_form_error"></span>
                    </div>
                    <div class="form-group select_type_warehouse">
                        <label for="count">Тип склада</label>
                        <select class="form-control"  name="type_warehouse" id="type_warehouse">
                        </select>
                        <span class="form_error type_warehouse_form_error"></span>
                    </div>
                    <div class="form-group">
                        <label for="price_from_provider">Цена от поставщика</label>
                        <input type="text" class="form-control" id="price_from_provider" name="price_from_provider" placeholder="Цена от поставщика">
                        <span class="form_error price_from_provider_form_error"></span>
                    </div>
                    <div class="form-group">
                        <label for="price">Цена</label>
                        <input type="text" class="form-control" id="price" name="price" placeholder="Цена">
                        <span class="form_error price_form_error"></span>
                    </div>
                    <div class="form-group">
                        <label for="count">Количество</label>
                        <input type="text" class="form-control" id="count" name="count" placeholder="Количество">
                        <span class="form_error count_form_error"></span>
                    </div>
                    <div>
                        <button name="submit" class="btn btn-primary">Отправить</button>
                    </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>