<div class="white_background_page div_content_shopping_cart ">
    <?= $pop_up_add_new_detail ?>
    <h3 class="title_h3"><?= $title ?></h3>
    <?php if (isset($shopping_cart['details']) && !empty($shopping_cart['details'])) { ?>
        <?php echo form_open('shopping_cart/list_details_in_cart', array('id' => 'form_shopping_cart')); ?>
        <div>
            <input class="inp_id_deal_amo form-control" type="text" name="id_deal_amo" placeholder="id сделки">
            <input class="inp_delivery_date form-control" type="text" name="delivery_date" placeholder="Дата доставки">
            <div class="clear">
            </div>
        </div>
        <div>
            <table class="table_details list_shopping_cart">
                <thead>
                    <tr class="table_details_head">
                        <th>
                            <input id="check_all_rows" name="check_all_rows" type="checkbox">
                        </th>
                        <th>
                            Артикул
                        </th>
                        <th>
                            Бренд
                        </th>
                        <th>
                            Название
                        </th>
                        <th>
                            Цена от поставщика
                        </th>
                        <th>
                            Поставщик
                        </th>
                        <th>
                            Срок доставки
                        </th>
                        <th>
                            Кол. на складе
                        </th>
                        <th>
                            Цена
                        </th>
                        <th>
                            Количество
                        </th>
                        <th>
                            Сумма
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sum = 0;
                    $base_total_sum = 0;
                    $profit = 0;
                    $counter_details = 0;
                    $discount = $shopping_cart['discount'];
                    ?>
                    <?php $exist_end_order = 0; ?>
                    <?php foreach ($shopping_cart['details'] as $detail) { ?>
                        <?php
                        $counter_details++;

                        if ($shopping_cart['percent'] > 0) {
                            $price = $detail['price'] + ($detail['price'] * $shopping_cart['percent'] / 100);
                        } elseif ($shopping_cart['percent'] < 0) {
                            $discount = $discount + ($detail['price'] * ($shopping_cart['percent'] * -1) / 100);
                            $price = $detail['price'];
                        } else {
                            $price = $detail['price'];
                        }
                        ?>
                        <tr class="tr_detail_row_cart detail_row_cart_<?= $detail['id_detail'] ?> <?php if ($counter_details == count($shopping_cart['details'])) { ?>last_row_order<?php } ?>">
                            <td>
                                <input type="checkbox" class="ckb_row_detail" name="selected_details[]" value="<?= $detail['id_detail'] ?>">
                            </td>
                            <td>
                                <?= $detail['article'] ?>
                            </td>
                            <td>
                                <?= $detail['brand'] ?>
                            </td>
                            <td>
                                <?= $detail['name'] ?>
                            </td>
                            <td>
                                <?= $detail['price_from_provider'] ?>
                            </td>
                            <td class="td_provider_name">
                                <?= $detail['provider_name'] ?>
                            </td>
                            <td>
                                <?php if (!empty($detail['delivery_period'])) { ?>
                                    <?= $detail['delivery_period'] ?> дн.
                                <?php } else { ?>
                                    -
                                <?php } ?>
                            </td>
                            <td>
                                <?php if (!empty($detail['quantity_from_provider'])) { ?>
                                    <?= $detail['quantity_from_provider'] ?>
                                <?php } else { ?>
                                    -
                                <?php } ?>
                            </td>
                            <td class="cell_item_price item_price_<?= $detail['id_detail'] ?>" data-price_without_percent="<?= $detail['price'] ?>" data-item_price="<?= $price ?>">
                                <span class="price_value" data-id_detail="<?= $detail['id_detail'] ?>"><?= number_format($price, 2, '.', '') ?></span>
                                <input type="text" class="price_input" name="price_input" placeholder="Цена" value="<?= number_format($price, 2, '.', '') ?>">
                                <img class="img_price_change" data-id_detail="<?= $detail['id_detail'] ?>" src="<?= base_url('images/tick.png'); ?>" title="Изменить цену" alt="Изменить цену">
                            </td>
                            <?php if (!isset($detail['status'])) { ?>
                                <td>
                                    <div class="control_counter">
                                        <img class="img_minus_blue" src="<?= base_url('images/minus-blue.png'); ?>" data-id_detail="<?= $detail['id_detail'] ?>" data-code_offer="<?= $detail['code_offer'] ?>" title="Уменьшить количество">
                                        <input type="text" class="inp_count_items inp_<?= $detail['id_detail'] ?>" data-is_custom="<?= $detail['is_custom'] ?>" data-id_detail="<?= $detail['id_detail'] ?>" data-code_offer="<?= $detail['code_offer'] ?>" data-old_count="<?= $detail['count'] ?>" value="<?= $detail['count'] ?>">
                                        <img class="img_plus_blue" src="<?= base_url('images/plus-blue.png'); ?>" data-is_custom="<?= $detail['is_custom'] ?>" data-id_detail="<?= $detail['id_detail'] ?>" data-code_offer="<?= $detail['code_offer'] ?>" title="Увеличить количество">
                                    </div>
                                </td>
                            <?php } else { ?>
                                <td>
                                    <?= anchor(base_url('search_detail/list_brands/' . $detail['article']), 'Обновить заказ!'); ?>
                                </td>

                                <?php $exist_end_order++ ?>
                            <?php } ?>
                            <td class="cell_item_total_price item_total_price_<?= $detail['id_detail'] ?>" data-item_total_price="<?= $price * $detail['count'] ?>">
                                <?= number_format($price * $detail['count'], 2, '.', '') ?>
                            </td>
                            <td>
                                <img class="detail_changed img_is_changed <?php if ($detail['is_changed'] != 1) { ?>hide_img_is_changed<?php } ?>" data-id_detail="<?= $detail['id_detail'] ?>" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">
                            </td>
                        </tr>

                        <?php
                        $base_total_sum = $base_total_sum + $detail['base_price'] * $detail['count'];
                        $sum = $sum + $price * $detail['count'];
                        ?>
                    <?php } ?>
                    <?php
                    $total_sum = $sum - $discount;

                    if ($base_total_sum > 3000) {
                        $profit = $total_sum - $base_total_sum;

                        if ($profit < -300) {
                            $profit = $profit + 300;
                        } elseif (($profit < 0) && ($profit > -300)) {
                            $profit = 0;
                        }
                    } else {
                        $profit = $total_sum - $base_total_sum;
                    }
                    ?>
                    <tr class="discount_row <?= (isset($shopping_cart['discount']) && ($shopping_cart['discount'] > 0)) ? '' : 'hide_discount_row' ?>">
                        <td colspan="8">
                            &nbsp;
                        </td>
                        <td colspan="3">
                            <strong>СКИДКА:</strong> <span class="discount_value"><?= $shopping_cart['discount'] ?></span><span class="discount_currency"> руб.</span>

                            <input type="text" class="discount_input" name="discount_input" placeholder="Наценка %" value="<?= $shopping_cart['discount'] ?>">
                            <img class="img_discount_change" src="<?= base_url('images/tick.png'); ?>" title="Изменить наценку" alt="Изменить наценку">
                        </td>
                        <td>
                            <img class="discount_changed img_is_changed <?php if ($shopping_cart['discount_changed'] != 1) { ?>hide_img_is_changed<?php } ?>" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">
                            <img class="img_discount_delete" src="<?= base_url('images/delete.png'); ?>" title="Удалить наценку" alt="Удалить наценку">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            &nbsp;
                        </td>
                        <td colspan="2" class="td_total_sum">
                            ИТОГО: <span class="total_sum" data-total_sum="<?= $total_sum ?>"><?= number_format($total_sum, 2, '.', '') ?></span> руб.
                        </td>
                        <td colspan="2" class="td_profit_tag">
                            (ПРИБЫЛЬ: <strong class="profit_tag <?= ($profit < 0) ? 'profit_bad' : 'profit_good' ?>"><?= number_format($profit, 2, '.', '') ?> руб.</strong>)
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="div_btn_shopping_cart">
            <div class="div_clear_shopping_cart">
                <button name="submit" class="btn btn-primary btn_clear_shopping_cart">Удалить выбранное</button>
                <a class="btn btn-success btn_add_new_detail" href="javascript:void(0);" role="button">Добавить новую деталь</a>
            </div>

            <?php if ($exist_end_order == 0) { ?>
                <div class="div_confirm_order">
                    <a class="loader_confirm_order" href="javascript:void(0);" role="button">
                        <img src="<?= base_url('images/loader.gif'); ?>" title="Создается заказ" alt="Создается заказ">
                    </a>
                    <a class="btn btn-danger btn_confirm_order" href="javascript:void(0);" role="button">Оформить заказ</a>
                </div>
                <div class="div_add_discount">
                    <a class="btn btn-default btn_add_discount" href="javascript:void(0);" role="button">Скидка - 300 руб.</a>
                </div>
                <div class="div_price_percent">
                    <input type="text" class="input_add_price_percent" name="price_percent" placeholder="Наценка %" value="<?php
                    if ($shopping_cart['percent'] != 0) {
                        echo $shopping_cart['percent'];
                    }
                    ?>">
                    <a class="btn btn-success btn_add_price_percent" href="javascript:void(0);" role="button">Добавить %</a>
                    <div class="clear"></div>
                </div>
            <?php } else { ?>
                <span class="message_exist_end_order">Нельзя оформить заказ, т.к. есть недействительное предложение</span>
            <?php } ?>
        </div>
        </form>
    <?php } else { ?>
        <div class="empty_shopping_cart">
            <strong>
                Корзина пуста!
            </strong>
        </div>
    <?php } ?>
</div>