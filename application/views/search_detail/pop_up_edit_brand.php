<div class="modal fade" id="pop_up_edit_brand" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Изменение данных бренда</h4>
            </div>
            <div class="modal-body">
                <div class="modal_merge_brands_body">
                    <div class="form_error"></div>
                    <br>
                    <?php echo form_open('', array('id' => 'form_edit_brand')); ?>
                    <input type="hidden" name="id_form_edit_brand" id="id_form_edit_brand" class="id_form_edit_brand" value="">
                    <div class="form_field_body">
                        <div class="label_part">
                            <label for="country_form_edit_brand">Страна</label>
                        </div>
                        <div class="field_part">
                            <input name="country_form_edit_brand" id="country_form_edit_brand" value="">
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="form_field_body">
                        <div class="label_part">
                            <label for="link_form_edit_brand">Ссылка</label>
                        </div>
                        <div class="field_part">
                            <input name="link_form_edit_brand" id="link_form_edit_brand" value="">
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="form_field_body">
                        <div class="label_part">
                            <label for="description_form_edit_brand">Описание</label>
                        </div>
                        <div class="field_part">
                            <textarea rows="7" cols="50" name="description_form_edit_brand" id="description_form_edit_brand"></textarea>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="form_field_body">
                        <div class="label_part">
                            <label for="recommended_form_edit_brand">Рекомендованный</label>
                        </div>
                        <div class="field_part">
                            <select name="recommended_form_edit_brand" id="recommended_form_edit_brand">
                                <option value="0">Нет</option>
                                <option value="1">Да</option>
                            </select>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="form_field_body">
                        <div class="label_part">
                            <label for="rating_form_edit_brand">Рейтинг</label>
                        </div>
                        <div class="field_part">
                            <select name="rating_form_edit_brand" id="rating_form_edit_brand">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <button name="submit" class="btn btn-primary">Отправить</button>
                    </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>