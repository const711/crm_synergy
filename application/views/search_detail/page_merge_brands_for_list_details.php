<div class="control_panel_merge_brands_for_list_details">
    <div class="body_control_panel_buttons">
        <ul class="control_panel_buttons">
            <li>
                <a href="<?= base_url('search_detail/list_details/' . $article . '/' . $key_group . '?private_data=' . $private_data) ?>">
                    <img src="<?= base_url('images/merge.png'); ?>" title="Объединить бренды">
                </a>
            </li>
        </ul>
    </div>
    <div class="merge_brands_activated">
        Режим редактирования брендов ВКЛЮЧЕН!
    </div>
</div>
<div class="clear"></div>
<div class="white_background_page div_list_merge_brands_for_list_details">
    <div class="modal fade" id="pop_up_merge_brands_for_list_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Список объединяемых брендов</h4>
                </div>
                <div class="modal-body">
                    <div class="modal_merge_brands_body">
                        <p>Будут объединены следующие бренды:</p>
                        <div class="list_merge_brands"></div>
                        <?php echo form_open('', array('id' => 'form_name_group_for_list_details', 'autocomplete' => 'off')); ?>
                        <div>
                            <div class="body_select_name_group_brand">
                                <div>
                                    <label for="name_group">Название группы</label>
                                </div>
                                <div class="select_name_group_brand"></div>
                            </div>
                            <div class="body_select_brand_for_description">
                                <div>
                                    <label for="name_group">Описание бренда</label>
                                </div>
                                <div class="select_brand_for_description">
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <br>
                        <div class="form_error"></div>
                        <div class="selected_description_brand"></div>
                        <div>
                            <button type="button" class="btn btn-success btn_name_group">Объединить</button>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    <article>
        <h5 class="title_h5"><?php echo $title; ?></h5>

        <div>
            <?php if (!empty($list_brands)) { ?>
                <?php echo form_open('', array('id' => 'form_merge_brands_for_list_details', 'autocomplete' => 'off')); ?>
                <input type="hidden" name="article_search" id="article_search" value="<?= $article ?>">
                <input type="hidden" name="key_group_search" id="key_group_search" value="<?= $key_group ?>">
                <input type="hidden" name="private" id="private" value="<?= $private_data ?>">
                <table class="table_details">
                    <thead>
                        <tr>
                            <th>
                                <a href="<?= base_url('search_detail/list_merge_brands_for_list_details/' . $article . '/' . $key_group . '?field_sort=' . $sort_links['article']['field_name'] . '&type_sort=' . $sort_links['article']['type_sort'] . '&private_data=' . $private_data) ?>">
                                    Артикул
                                </a>
                            </th>
                            <th>
                                <a href="<?= base_url('search_detail/list_merge_brands_for_list_details/' . $article . '/' . $key_group . '?field_sort=' . $sort_links['brand']['field_name'] . '&type_sort=' . $sort_links['brand']['type_sort'] . '&private_data=' . $private_data) ?>">
                                    Бренд
                                </a>
                            </th>
                            <th class="cell_detail_name">
                                <a href="<?= base_url('search_detail/list_merge_brands_for_list_details/' . $article . '/' . $key_group . '?field_sort=' . $sort_links['name']['field_name'] . '&type_sort=' . $sort_links['name']['type_sort'] . '&private_data=' . $private_data) ?>">
                                    Товар
                                </a>
                            </th>
                        </tr>
                    </thead>
                    <?php foreach ($list_brands as $key => $brand): ?>
                        <tr class="brand_row" data-id_row_brand='<?= $key ?>'>
                            <td> 
                                <input type="checkbox" name="selected_merge_brands[]" class="checkbox_brand" id="checkbox_for_row_<?= $key ?>" data-article="<?= $brand['article'] ?>" data-brand_name="<?= mb_strtoupper(htmlspecialchars($brand['brand'])) ?>" data-brand_type="<?= $brand['type'] ?>" value="<?= mb_strtoupper(htmlspecialchars($brand['brand'])) ?> **<?= $brand['article'] ?>**">
                                <strong><?= mb_strtoupper($brand['article']) ?></strong>
                            </td>
                            <td>
                                <strong><?= mb_strtoupper($brand['brand']) ?></strong>
                            </td>
                            <td>
                                <?= $brand['name'] ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </table>
                <br>
                <div>
                    <button type="button" class="btn btn-success btn_merge_brands">Приравнять бренды</button>
                </div>
                </form>
            <?php } else { ?>
                <table class="table_details">
                    <tbody>
                        <tr class="empty_row_table">
                            <td>
                                Записей не обнаружено.
                            </td>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </article>
</div>