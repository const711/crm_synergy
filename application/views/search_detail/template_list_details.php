<table class="search_list_details table_details">
    <tbody>
        <tr>
            <td class="title" colspan="3">
                <h5 class="title_h5">Запрошенная деталь (артикул: <?= $article ?>; бренд: <?= $group_name ?>):</h5>
            </td>
            <td colspan="7" class="td_sort_list_details">
                <div class="sort_list_details">
                    <div class="title_sort_list_details">Сортировка:</div>
                    <div class="dropdown">
                        <div class="select">
                            <?php if ($type_sort == 1) { ?>
                                <span>Типу склада (1 → 3)</span>
                            <?php } elseif ($type_sort == 2) { ?>
                                <span>Цене (дешевле → дороже)</span>
                            <?php } elseif ($type_sort == 3) { ?>
                                <span>Наименованию (по алфавиту)</span>
                            <?php } elseif ($type_sort == 4) { ?>
                                <span>По сроку дост.</span>
                            <?php } elseif ($type_sort == 5) { ?>
                                <span>Популярности (сначала популярные)</span>
                            <?php } else { ?>
                                <span>Типу склада (1 → 3)</span>
                            <?php } ?>

                            <i class="fa fa-chevron-left"></i>
                        </div>
                        <input type="hidden" name="gender" value="<?= $type_sort ?>">
                        <ul class="dropdown-menu">
                            <li id="1">
                                Типу склада (1 → 3)
                            </li>
                            <li id="2">
                                Цене (дешевле → дороже)
                            </li>
                            <li id="3">
                                Наименованию (по алфавиту)
                            </li>
                            <li id="4">
                                По сроку дост.
                            </li>
                            <li id="5">
                                Популярности (сначала популярные)
                            </li>
                        </ul>
                    </div>
                </div>
            </td>
        </tr>
        <tr class="table_details_head">
            <td>
                Артикул
            </td>
            <td>
                Бренд
            </td>
            <td>
                Описание
            </td>
            <td>
                Возвр.
            </td>
            <td>
                Поставщ.
            </td>
            <td>
                Срок.
            </td>
            <td>
                Розн.
            </td>
            <td>
                Опт.
            </td>
            <td colspan="2">
                Склад(Кратн.)
            </td>
        </tr>
        <?php if (isset($list_details['original']) and ! empty($list_details['original'])) { ?>
            <?php $number_offer = 0; ?>
            <?php foreach ($list_details['original']['offers'] as $key => $offer): ?>
                <?php
                if ($offer['warehouse_type'] == 1) {
                    $img_warehouse_type = "box_green.png";
                } elseif ($offer['warehouse_type'] == 2) {
                    $img_warehouse_type = "box_orange.png";
                } else {
                    $img_warehouse_type = "box_red.png";
                }
                ?>
                <tr class="element_<?= $list_details['original']['brand_id'] ?> row_offer offer_for_detail_<?= $list_details['original']['id'] ?> <?php if ($number_offer >= 4) { ?>hide_offer hidden_offers_detail_<?= $list_details['original']['id'] ?><?php } ?>" data-id_offer="<?= $offer['id'] ?>">
                    <td class="cell_article">
                        <div class="div_cell_article">
                            <input type="checkbox" name="selected_offers[]" class="checkbox_offer" id="checkbox_for_row_offer_<?= $offer['id'] ?>" value="<?= $offer['id'] ?>">
                            <?php if ($number_offer == 0) { ?>
                                <a href="http://www.exist.ru/price.aspx?pcode=<?= $list_details['original']['article'] ?>" class="a_exist" target="_blank">
                                    <img class="icon_exist" src="<?= base_url('images/exst.png'); ?>" title="exist.ru">
                                </a>
                                <a href="https://www.google.ru/search?q=<?= $list_details['original']['brand'] ?>%20<?= $list_details['original']['article'] ?>&newwindow=1&source=lnms&tbm=isch" class="a_google_search_img" target="_blank">
                                    <img class="icon_google_search_img" src="<?= base_url('images/perm_media.png'); ?>" title="Поиск изображения в google">
                                </a>
                                <?php if ($private_data == 'show') { ?>
                                    <strong><?= anchor(base_url('/search_detail/search_detail_by_article_key_group/' . $list_details['original']['article'] . '/' . $list_details['original']['key_group']), mb_strtoupper($list_details['original']['article']), array('target' => '_blank')) ?></strong>
                                <?php } else { ?>
                                    *******
                                <?php } ?>
                            <?php } else { ?>
                                &nbsp;
                            <?php } ?>
                        </div>
                    </td>
                    <td class="cell_brand">
                        <div class="div_cell_brand">
                            <?php if ($number_offer == 0) { ?>
                                <?php if ($list_details['original']['need_edit_brand'] == 0) { ?>
                                    <strong class="popover_inf_brand" data-content="<?= $list_details['original']['popover_content_brand'] ?>">
                                        <?= mb_strtoupper($list_details['original']['brand']) ?>
                                    </strong>
                                <?php } else { ?>
                                    <strong class="name_brand_<?= $list_details['original']['brand_id'] ?>">
                                        <?= mb_strtoupper($list_details['original']['brand']) ?>
                                    </strong>
                                <?php } ?>
                                <?php if ($list_details['original']['need_edit_brand'] != 0) { ?>
                                    <img data-id_brand="<?= $list_details['original']['brand_id'] ?>" class="open_wind_edit_brand edit_icon_brand_<?= $list_details['original']['brand_id'] ?>" src="<?= base_url('images/kservices.png'); ?>">
                                <?php } ?>
                                <img class="icon_recommended_brand icon_recommended_brand_<?= $list_details['original']['brand_id'] ?> <?php if ($list_details['original']['recommended'] != 1) { ?>hide_icon_recommended_brand<?php } ?>" src="<?= base_url('images/green_checkmark.png'); ?>" title="Рекомендуем" alt="Рекомендуем">
                                <img class="img_compatibility" src="<?= base_url('images/glassy-smiley-good.png'); ?>">
                            <?php } else { ?>
                                &nbsp;
                            <?php } ?>
                        </div>
                    </td>
                    <td class="cell_description">
                        <span class="<?php if ($number_offer != 0) { ?>hidden_offer_description<?php } ?>">
                            <?= $offer['offers_description'] ?>
                        </span>
                        <?php if ($number_offer == 0) { ?>
                            &nbsp;
                            <img class="show_more_names_detail" data-id_detail = "<?= $list_details['original']['id'] ?>" src="<?= base_url('images/more.png'); ?>" alt="Другие описания детали" title="Другие описания детали">
                        <?php } ?>
                    </td>
                    <td class="cell_opportunity_return">
                        <span class="popover_type_warehouse" data-content="<?= $offer['popover_content_type_warehouse'] ?>">
                            <?php if ($offer['opportunity_return'] == 1) { ?>
                                Да
                            <?php } else { ?>
                                Нет
                            <?php } ?>
                        </span>
                        <img class="img_warehouse_type" src="<?= base_url('images/' . $img_warehouse_type); ?>">
                        <?php if ($offer['type_provider'] == 1) { ?>
                            <img class="img_warehouse_type" src="<?= base_url('images/home.png'); ?>">
                        <?php } ?>
                    </td>
                    <td class="cell_provider">
                        <div class="div_cell_provider">
                            <?php if ($private_data == 'show') { ?>
                                <?= $offer['provider_with_link'] ?>
                            <?php } else { ?>
                                <span class="provider_name">
                                    <?= change_symbols_on_stars($offer['provider'], 1) ?>
                                </span>
                            <?php } ?>
                            <?php if (isset($offer['old_price_list'])) { ?>
                                <img class="img_alert_old_price" src="<?= base_url('images/alert-triangle-red.png'); ?>" title="Прайс устарел!">
                            <?php } else { ?>
                                &nbsp;
                            <?php } ?>
                        </div>
                    </td>
                    <td>
                        <span class="popover_with_description_for_type_warehouse" data-content="<?= $offer['popover_with_description_for_type_warehouse'] ?>">
                            <?= $offer['delivery_period'] ?>
                            <?php if (!empty($offer['delivery_period']) && ($offer['delivery_period'] != '-')) { ?>
                                дн.
                            <?php } ?>
                        </span>
                    </td>
                    <td>
                        <strong><?= $offer['retail_price'] ?></strong>
                    </td>
                    <td>
                        <?php if ($private_data == 'show') { ?>
                            <?= str_replace('.00', '', $offer['price']) ?>

                        <?php } else { ?>
                            *******
                        <?php } ?>
                    </td>
                    <td>
                        <?= $offer['quantity'] ?> <?php if ($offer['min_part'] > 1) { ?>(<?= $offer['min_part'] ?>)<?php } ?>
                    </td>
                    <td class="cell_btn_add_in_shopping_card">
                        <button type="button" class="btn btn_add_in_shopping_card" data-code_offer="<?= $offer['code'] ?>">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                            <span class="add_item_in_cart <?php if ($offer['in_shopping_cart'] <= 0) { ?>hide_count_items_in_cart<?php } ?>" data-count_items_cart="<?= $offer['in_shopping_cart'] ?>">(<?= $offer['in_shopping_cart'] ?>)</span>
                            <img class="loader_add_to_cart" src="<?= base_url('images/loader.gif'); ?>" title="Идет добавление" alt="Идет добавление">
                        </button>
                    </td>
                </tr>
                <?php if (isset($list_details['original']['nearest_offer_price_for_client']) && ($number_offer == (count($list_details['original']['offers']) - 1))) { ?>
                    <tr class="show_detail_offers element_<?= $list_details['original']['brand_id'] ?>" data-id_detail = "<?= $list_details['original']['id'] ?>">
                        <td colspan="10">
                            <span class="show_list_offers" id="show_list_offers_<?= $list_details['original']['id'] ?>">Показать список предложений (от <?= $list_details['original']['nearest_offer_price_for_client'] ?> руб.)</span>
                            <span class="hide_list_offers" id="hide_list_offers_<?= $list_details['original']['id'] ?>">Скрыть список предложений</span>
                        </td>
                    </tr>
                <?php } ?>
                <?php if (($number_offer <= 4) && ($number_offer == count($list_details['original']['offers']))) { ?>
                    <tr class="show_detail_offers element_<?= $list_details['original']['brand_id'] ?>" data-id_detail = "<?= $list_details['original']['id'] ?>">
                        <td colspan="10">&nbsp;</td>
                    </tr>
                <?php } ?>
                <?php $number_offer++; ?>
            <?php endforeach ?>
        <?php } else { ?>
            <tr class="empty_row_table">
                <td colspan="10">
                    Данных по оригинальной детали не найдено.
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td class="title" colspan="10">
                <h5 class="title_h5">Аналоги:</h5>
            </td>
        </tr>
        <?php if (isset($list_details['cross']) and ! empty($list_details['cross'])) { ?>
            <?php foreach ($list_details['cross'] as $detail): ?>
                <?php $number_offer = 0; ?>
                <?php foreach ($detail['offers'] as $key => $offer): ?>
                    <?php
                    if ($offer['warehouse_type'] == 1) {
                        $img_warehouse_type = "box_green.png";
                    } elseif ($offer['warehouse_type'] == 2) {
                        $img_warehouse_type = "box_orange.png";
                    } else {
                        $img_warehouse_type = "box_red.png";
                    }
                    ?>
                    <tr class="element_<?= $detail['brand_id'] ?> row_offer offer_for_detail_<?= $detail['id'] ?> <?php if ($number_offer >= 4) { ?>hide_offer hidden_offers_detail_<?= $detail['id'] ?><?php } ?>" data-id_offer="<?= $offer['id'] ?>">
                        <td class="cell_article">
                            <div class="div_cell_article">
                                <input type="checkbox" name="selected_offers[]" class="checkbox_offer" id="checkbox_for_row_offer_<?= $offer['id'] ?>" value="<?= $offer['id'] ?>">
                                <?php if ($number_offer == 0) { ?>
                                    <a href="http://www.exist.ru/price.aspx?pcode=<?= $detail['article'] ?>" class="a_exist" target="_blank">
                                        <img class="icon_exist" src="<?= base_url('images/exst.png'); ?>" title="exist.ru">
                                    </a>
                                    <a href="https://www.google.ru/search?q=<?= $detail['brand'] ?>%20<?= $detail['article'] ?>&newwindow=1&source=lnms&tbm=isch" class="a_google_search_img" target="_blank">
                                        <img class="icon_google_search_img" src="<?= base_url('images/perm_media.png'); ?>" title="Поиск изображения в google">
                                    </a>
                                    <?php if ($private_data == 'show') { ?>
                                        <strong>
                                            <?= anchor(base_url('/search_detail/search_detail_by_article_key_group/' . $detail['article'] . '/' . $detail['key_group']), mb_strtoupper($detail['article']), array('target' => '_blank')) ?>
                                        </strong>
                                    <?php } else { ?>
                                        *******
                                    <?php } ?>
                                <?php } else { ?>
                                    &nbsp;
                                <?php } ?>
                            </div>
                        </td>
                        <td class="cell_brand">
                            <div class="div_cell_brand">
                                <?php if ($number_offer == 0) { ?>
                                    <?php if ($detail['need_edit_brand'] == 0) { ?>
                                        <strong class="popover_inf_brand" data-content="<?= $detail['popover_content_brand'] ?>">
                                            <?= mb_strtoupper($detail['brand']) ?>
                                        </strong>
                                    <?php } else { ?>
                                        <strong>
                                            <?= mb_strtoupper($detail['brand']) ?>
                                        </strong>
                                    <?php } ?>
                                    <?php if ($detail['need_edit_brand'] == 0) { ?>
                                        <img data-id_brand="<?= $detail['brand_id'] ?>" class="open_wind_edit_brand edit_icon_brand_<?= $detail['brand_id'] ?>" src="<?= base_url('images/kservices.png'); ?>">
                                    <?php } ?>
                                    <img class="icon_recommended_brand icon_recommended_brand_<?= $detail['brand_id'] ?> <?php if ($detail['recommended'] != 1) { ?>hide_icon_recommended_brand<?php } ?>" src="<?= base_url('images/green_checkmark.png'); ?>" title="Рекомендуем" alt="Рекомендуем">
                                    <?php if (($detail['compatibility'] == 'original') or ( $detail['compatibility'] >= 7)) { ?>
                                        <img class="img_compatibility popover_list_provider_rating" data-content="<?= $detail['popover_list_provider_rating'] ?>" src="<?= base_url('images/glassy-smiley-good.png'); ?>">
                                    <?php } else { ?>
                                        <img class="img_compatibility popover_list_provider_rating" data-content="<?= $detail['popover_list_provider_rating'] ?>" src="<?= base_url('images/glassy-smiley-failure.png'); ?>">
                                    <?php } ?>
                                <?php } else { ?>
                                    &nbsp;
                                <?php } ?>
                            </div>
                        </td>
                        <td class="cell_description">
                            <span class="<?php if ($number_offer != 0) { ?>hidden_offer_description<?php } ?>">
                                <?= $offer['offers_description'] ?>
                            </span>
                            <?php if ($number_offer == 0) { ?>
                                &nbsp;
                                <img class="show_more_names_detail" data-id_detail = "<?= $detail['id'] ?>" src="<?= base_url('images/more.png'); ?>" alt="Другие описания детали" title="Другие описания детали">
                            <?php } ?>
                        </td>
                        <td class="cell_opportunity_return">
                            <span class="popover_type_warehouse" data-content="<?= $offer['popover_content_type_warehouse'] ?>">
                                <?php if ($offer['opportunity_return'] == 1) { ?>
                                    Да
                                <?php } else { ?>
                                    Нет
                                <?php } ?>
                            </span>
                            <img class="img_warehouse_type" src="<?= base_url('images/' . $img_warehouse_type); ?>">
                            <?php if ($offer['type_provider'] == 1) { ?>
                                <img class="img_warehouse_type" src="<?= base_url('images/home.png'); ?>">
                            <?php } ?>
                        </td>
                        <td class="cell_provider">
                            <div class="div_cell_provider">
                                <?php if ($private_data == 'show') { ?>
                                    <?= $offer['provider_with_link'] ?>
                                <?php } else { ?>
                                    <span class="provider_name">
                                        <?= change_symbols_on_stars($offer['provider'], 1) ?>
                                    </span>
                                <?php } ?>
                                <?php if (isset($offer['old_price_list'])) { ?>
                                    <img src="<?= base_url('images/alert-triangle-red.png'); ?>" title="Прайс устарел!">
                                <?php } else { ?>
                                    &nbsp;
                                <?php } ?>
                            </div>
                        </td>
                        <td>
                            <span class="popover_with_description_for_type_warehouse" data-content="<?= $offer['popover_with_description_for_type_warehouse'] ?>">
                                <?= $offer['delivery_period'] ?>
                                <?php if (!empty($offer['delivery_period']) && ($offer['delivery_period'] != '-')) { ?>
                                    дн.
                                <?php } ?>
                            </span>
                        </td>
                        <td>
                            <strong><?= $offer['retail_price'] ?></strong>
                        </td>
                        <td>
                            <?php if ($private_data == 'show') { ?>
                                <?= str_replace('.00', '', $offer['price']) ?>

                            <?php } else { ?>
                                *******
                            <?php } ?>
                        </td>
                        <td>
                            <?= $offer['quantity'] ?> <?php if ($offer['min_part'] > 1) { ?>(<?= $offer['min_part'] ?>)<?php } ?>
                        </td>
                        <td class="cell_btn_add_in_shopping_card">
                            <button type="button" class="btn btn_add_in_shopping_card" data-code_offer="<?= $offer['code'] ?>">
                                <span class="glyphicon glyphicon-shopping-cart"></span>
                                <span class="add_item_in_cart <?php if ($offer['in_shopping_cart'] <= 0) { ?>hide_count_items_in_cart<?php } ?>" data-count_items_cart="<?= $offer['in_shopping_cart'] ?>">(<?= $offer['in_shopping_cart'] ?>)</span>
                                <img class="loader_add_to_cart" src="<?= base_url('images/loader.gif'); ?>" title="Идет добавление" alt="Идет добавление">
                            </button>
                        </td>
                    </tr>
                    <?php if (isset($detail['nearest_offer_price_for_client']) && ($number_offer == (count($detail['offers']) - 1))) { ?>
                        <tr class="show_detail_offers element_<?= $detail['brand_id'] ?>" data-id_detail = "<?= $detail['id'] ?>">
                            <td colspan="10">
                                <span class="show_list_offers" id="show_list_offers_<?= $detail['id'] ?>">Показать список предложений (от <?= $detail['nearest_offer_price_for_client'] ?> руб.)</span>
                                <span class="hide_list_offers" id="hide_list_offers_<?= $detail['id'] ?>">Скрыть список предложений</span>
                            </td>
                        </tr>
                    <?php } elseif (($number_offer <= 4) && ($number_offer == count($detail['offers']) - 1)) { ?>
                        <tr class="show_detail_offers element_<?= $detail['brand_id'] ?>">
                            <td colspan="10">&nbsp;</td>
                        </tr>
                    <?php } ?>
                    <?php $number_offer++; ?>
                <?php endforeach ?>
            <?php endforeach ?>
        <?php } else { ?>
            <tr class="empty_row_table">
                <td colspan="10">
                    Аналогов для данной детали нет.
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>