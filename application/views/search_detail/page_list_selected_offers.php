<div class="white_background_page div_list_selected_offers">
    <?= $pop_up_edit_brand ?>

    <h5 class="title_h5"><?php echo $title; ?></h5>
    <?php if (!empty($list_selected_offers)) { ?>
        <?php $temporary_id_detail = '' ?>
    
        <?php echo form_open('', array('id' => 'form_selected_offers', 'autocomplete ' => 'off')); ?>
        <input type="hidden" name="article_search" value="<?= $article ?>">
        <input type="hidden" name="key_group_search" value="<?= $key_group ?>">
        <table class="table_details">
            <thead>
                <tr class="table_details_head">
                    <th>
                        Артикул
                    </th>
                    <th>
                        Бренд
                    </th>
                    <th>
                        Наименование
                    </th>
                    <th>
                        Цена закупки (&#8381;)
                    </th>
                    <th>
                        Поставщик
                    </th>
                    <th>
                        Цена розница (&#8381;)
                    </th>
                    <th>
                        Количество
                    </th>
                    <th colspan="2">
                        Срок доставки
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($list_selected_offers as $selected_offers): ?>
                    <?php
                    if ($selected_offers['id_detail'] != $temporary_id_detail) {
                        $temporary_id_detail = $selected_offers['id_detail'];

                        $is_first = 1;
                    } else {
                        $is_first = 0;
                    }
                    ?>

                    <tr class="row_offer">
                        <?php if ($is_first == 1) { ?>
                            <td class="td_article">
                                <strong><?= mb_strtoupper($selected_offers['article']) ?></strong>
                            </td>
                            <td class="td_brand">
                                <?php if ($selected_offers['need_edit_brand'] == 0) { ?>
                                    <img src="<?= base_url('images/green_checkmark.png'); ?>">
                                    <strong class="popover_inf_brand" data-content="<?= $selected_offers['popover_content_brand'] ?>">
                                        <?= mb_strtoupper($selected_offers['brand']) ?>
                                    </strong>
                                <?php } else { ?>
                                    <img class="edit_success_hide edit_success_icon_brand_<?= $selected_offers['brand_id'] ?>" src="<?= base_url('images/green_checkmark.png'); ?>">
                                    <img data-id_brand="<?= $selected_offers['brand_id'] ?>" class="open_wind_edit_brand edit_icon_brand_<?= $selected_offers['brand_id'] ?>" src="<?= base_url('images/kservices.png'); ?>">
                                    <strong class="name_brand_<?= $selected_offers['brand_id'] ?>">
                                        <?= mb_strtoupper($selected_offers['brand']) ?>
                                    </strong>
                                <?php } ?>
                            </td>
                        <?php } else { ?>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        <?php } ?>
                        <td><?= $selected_offers['description'] ?></td>
                        <td>
                            <?= $selected_offers['price'] ?>
                        </td>
                        <td>
                            <?= $selected_offers['provider'] ?>
                        </td>
                        <td>
                            <strong><?= $selected_offers['retail_price'] ?></strong>
                        </td>
                        <td class="td_quantity">
                            <img class="img_minus_blue" data-id_offer="<?= $selected_offers['id_offer'] ?>" src="<?= base_url('images/minus-blue.png'); ?>">
                            <span class="quantity_for_offer_<?= $selected_offers['id_offer'] ?>"><?= $selected_offers['quantity'] ?></span>
                            <img class="img_plus_blue" data-id_offer="<?= $selected_offers['id_offer'] ?>" src="<?= base_url('images/plus-blue.png'); ?>">
                        </td>
                        <td>
                            <?= $selected_offers['delivery_period'] ?>
                        </td>
                        <td>
                            <img class="delete_selected_offers" data-id_selected_offer="<?= $selected_offers['id_offer'] ?>" src="<?= base_url('images/delete.png'); ?>">
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        </form>
    <?php } else { ?>
        <table class="table_details">
            <tbody>
                <tr class="empty_row_table">
                    <td>
                        Записей не обнаружено.
                    </td>
                </tr>
            </tbody>
        </table>
    <?php } ?>
    <div class="link_back">
        <?= anchor(base_url('/search_detail/list_details/' . $article . '/' . $key_group . '/' . $private_data), 'Назад') ?>
    </div>
</div>