<div class="control_panel_list_brands">
    <div class="body_control_panel_buttons">
        <ul class="control_panel_buttons">
            <li>
                <a href="<?= base_url('search_detail/list_brands/' . $article . '?private_data=' . $private_data) ?>">
                    <img src="<?= base_url('images/merge.png'); ?>" title="Объединить бренды">
                </a>
            </li>
        </ul>
    </div>
    <div class="merge_brands_activated">
        Режим редактирования брендов ВКЛЮЧЕН!
    </div>
</div>
<div class="clear"></div>
<?= $search_detail_form ?>
<div>
    <div class="modal fade" id="pop_up_merge_brands" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Список объединяемых брендов</h4>
                </div>
                <div class="modal-body">
                    <div class="modal_merge_brands_body">
                        <p>Будут объединены следующие бренды:</p>
                        <div class="list_merge_brands"></div>
                        <?php echo form_open('', array('id' => 'form_name_group_in_brands_list', 'autocomplete' => 'off')); ?>
                        <div>
                            <div class="body_select_name_group_brand">
                                <div>
                                    <label for="name_group">Название группы</label>
                                </div>
                                <div class="select_name_group_brand"></div>
                            </div>
                            <div class="body_select_brand_for_description">
                                <div>
                                    <label for="name_group">Описание бренда</label>
                                </div>
                                <div class="select_brand_for_description">
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <br>
                        <div class="form_error"></div>
                        <div class="selected_description_brand"></div>
                        <div>
                            <button type="button" class="btn btn-success btn_name_group">Объединить</button>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <?php echo form_open('', array('id' => 'form_merge_brands_in_brands_list', 'autocomplete' => 'off')); ?>
    <input type="hidden" name="article_search" id="article_search" value="<?= $article ?>">
    <input type="hidden" name="private" id="private" value="<?= $private_data ?>">
    <table class="table_list_merge_brands">
        <?php if (!empty($list_brands)) { ?>
            <?php foreach ($list_brands as $key => $brand): ?>
                <tr class="brand_row" data-id_row_brand='<?= $key ?>'>
                    <td>
                        <input type="checkbox" name="selected_merge_brands[]" class="checkbox_brand" id="checkbox_for_row_<?= $key ?>" data-brand_name="<?= mb_strtoupper(htmlspecialchars($brand['name'])) ?> (<?= $brand['type'] ?>)" value="<?= mb_strtoupper(htmlspecialchars($brand['name'])) ?>">
                        <span class="brand_name"><?= mb_strtoupper($brand['name']) ?></span>
                    </td>
                </tr>
            <?php endforeach ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <button type="button" class="btn btn-success btn_merge_brands">Приравнять бренды</button>
                </td>
            </tr>
        <?php } else { ?>
            <tr>
                <td class="empty_row_table">
                    Брендов по данному артикулу не найдено.
                </td>
            </tr>
        <?php } ?>
    </table>
</form>
</div>