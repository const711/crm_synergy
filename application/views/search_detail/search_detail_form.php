<div>
    <?php echo form_open('search_detail/index', array('id' => 'search_detail_form')); ?>
        <div class="form_field_body">
            <div>
                <label for="article_detail">Поиск по коду</label>
            </div>
            <div class="input-group">
                <input type="input" name="article_detail" placeholder="Поиск кода" class="form-control test" id="article_detail">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" name="submit" id="search_detail"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </div>
        <div class="form_error">
            <?php if (isset($error_mass)) { ?>
                <?= $error_mass ?>
            <?php } else { ?>
                <?php echo validation_errors(); ?>
            <?php } ?>
        </div>
    </form>
</div>