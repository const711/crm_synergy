<div class="control_panel_list_brands">
    <ul class="control_panel_buttons">
        <li>
            <a href="<?= base_url('search_detail/list_merge_brands/' . $article . '?private_data=' . $private_data) ?>">
                <img src="<?= base_url('images/merge.png'); ?>" title="Объединить бренды">
            </a>
        </li>
        <li>
            <?php if ($private_data == 'show') { ?>
                <a href="<?= base_url('search_detail/list_brands/' . $article . '?private_data=hide') ?>">
                    <img src="<?= base_url('images/eye.png'); ?>" title="Обычный режим">
                </a>
            <?php } else { ?>
                <a href="<?= base_url('search_detail/list_brands/' . $article . '?private_data=show') ?>">
                    <img src="<?= base_url('images/eye_black.png'); ?>" title="Скрытый режим">
                </a>
            <?php } ?>
        </li>
        <li>
            <a href="javascript:void(0);" data-article_search="<?= $article ?>" class="need_update_list_brands">
                <img src="<?= base_url('images/update.png'); ?>" class="img_need_update" title="Обновить данные">
            </a>
            <img src="<?= base_url('images/loader.gif'); ?>" class="img_loader" title="Обновляется">
        </li>
    </ul>
</div>
<?= $search_detail_form ?>
<div>
    <table class="table_list_brands">
        <?php if (!empty($list_brands)) { ?>
            <?php foreach ($list_brands as $brand): ?>
                <tr>
                    <td>
                        <?= anchor(base_url('search_detail/list_details/' . $article . '/' . $brand['key_group'] . '/' . $private_data), mb_strtoupper($brand['name'])); ?>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php } else { ?>
            <tr>
                <td class="empty_row_table">
                    Брендов по данному артикулу не найдено.
                </td>
            </tr>
        <?php } ?>
    </table>
</div>