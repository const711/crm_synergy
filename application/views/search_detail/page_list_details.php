<div class="control_panel_list_details">
    <div class="body_control_panel_buttons">
        <ul class="control_panel_buttons">
            <li>
                <a href="<?= base_url('search_detail/list_merge_brands_for_list_details/' . $article . '/' . $key_group . '?private_data=' . $private_data) ?>">
                    <img src="<?= base_url('images/merge.png'); ?>" title="Объединить бренды">
                </a>
            </li>
            <li class="active">
                <?php if ($private_data == 'show') { ?>
                    <a href="<?= base_url('search_detail/list_details/' . $article . '/' . $key_group . '?private_data=hide&type_warehouse=' . $filter_type_warehouse . '&recom_brands=' . $recom_brands . '&type_sort=' . $type_sort . '&price_between_beg=' . $price_between_beg . '&price_between_end=' . $price_between_end) ?>">
                        <img src="<?= base_url('images/eye.png'); ?>" title="Обычный режим">
                    </a>
                <?php } else { ?>
                    <a href="<?= base_url('search_detail/list_details/' . $article . '/' . $key_group . '?private_data=show&type_warehouse=' . $filter_type_warehouse . '&recom_brands=' . $recom_brands . '&type_sort=' . $type_sort . '&price_between_beg=' . $price_between_beg . '&price_between_end=' . $price_between_end) ?>">
                        <img src="<?= base_url('images/eye_black.png'); ?>" title="Скрытый режим">
                    </a>
                <?php } ?>
            </li>
            <li class="active">
                <?php if ($recom_brands == 'all_brands') { ?>
                    <a href="<?= base_url('search_detail/list_details/' . $article . '/' . $key_group . '?private_data=' . $private_data . '&type_warehouse=' . $filter_type_warehouse . '&recom_brands=recom_brands') ?>">
                        <img src="<?= base_url('images/thumbs-up-blue.png'); ?>" title="Рекоменд. бренды">
                    </a>
                <?php } else { ?>
                    <a href="<?= base_url('search_detail/list_details/' . $article . '/' . $key_group . '?private_data=' . $private_data . '&type_warehouse=' . $filter_type_warehouse . '&recom_brands=all_brands') ?>">
                        <img src="<?= base_url('images/thumbs-up-red.png'); ?>" title="Все бренды">
                    </a>
                <?php } ?>
            </li>
            <li>
                <a href="<?= base_url('search_detail/update_list_details/' . $article . '/' . $key_group) ?>" class="need_update_list_details">
                    <img src="<?= base_url('images/update.png'); ?>" class="img_need_update" title="Обновить данные">
                </a>
                <img src="<?= base_url('images/loader.gif'); ?>" class="img_loader" title="Обновляется">
            </li>
            <li <?php if ($filter_type_warehouse == 'one') { ?>class="active"<?php } ?>>
                <a href="<?= base_url('search_detail/list_details/' . $article . '/' . $key_group . '?private_data=' . $private_data . '&type_warehouse=one&recom_brands=' . $recom_brands) ?>">
                    <img src="<?= base_url('images/green_house.png'); ?>" title="Показать детали первого типа склада">
                </a>
            </li>
            <li>
                <a href="<?= base_url('search_detail/list_details/' . $article . '/' . $key_group) ?>">
                    <img src="<?= base_url('images/reset.png'); ?>" title="Сброс">
                </a>
            </li>
        </ul>
    </div>
    <div class="list_provider_with_api">
        <?php if (isset($list_provider_with_api) and ! empty($list_provider_with_api)) { ?>
            <ul>
                <?php foreach ($list_provider_with_api as $provider) { ?>
                    <?php
                    if (!empty($provider['link'])) {
                        $link = 'http://' . str_replace('{article}', $article, $provider['link']);

                        $target = 'target="_blank"';
                    } else {
                        $link = 'javascript:void(0);';

                        $target = '';
                    }

                    if (isset($providers_in_list_details) && in_array($provider['id'], $providers_in_list_details)) {
                        $color_text_provider_button = 'exist_provider_in_list_details';
                    } else {
                        $color_text_provider_button = 'not_exist_provider_in_list_details';
                    }
                    ?>

                    <li>
                        <a href="<?= $link ?>" <?= $target ?> class="provider_button <?= $color_text_provider_button ?>">
                            <?= $provider['name'] ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        <?php } ?>
    </div>
</div>
<div class="clear"></div>
<?php if (isset($list_details) && !empty($list_details)) { ?>
    <?= $pop_up_edit_brand ?>
    <div class="div_filter_and_list_details">
        <div class="div_left_filter">
            <div class="white_background_page">
                <div class="filter_by_brand">
                    <div class="title_filter_by_brand">
                        Производитель
                    </div>
                    <div class="list_brands_for_filter">
                        <?php if (isset($unique_brands) && !empty($unique_brands)) { ?>
                            <div>
                                <?php $first_element = reset($unique_brands); ?>
                                <?php foreach ($unique_brands as $key => $value) { ?>                            
                                    <div class="row_brand">
                                        <div class="brand_name">
                                            <input type="checkbox" name="search_details_by_brand" value="<?= $key ?>" <?php if ($value['brand'] === $first_element['brand']) { ?> checked="checked" disabled="disabled"<?php } ?>>
                                            <strong><?= $value['brand'] ?></strong>
                                            <?php if ($value['recommended'] == 1) { ?>
                                                <img src="<?= base_url('images/green_checkmark.png'); ?>" title="Мы рекомендуем" alt="Мы рекомендуем">
                                            <?php } ?>
                                            <?php if ($value['exist_my_storage'] == 1) { ?>
                                                <img class="img_home" src="<?= base_url('images/home.png'); ?>" title="Есть домашний склад" alt="Есть домашний склад">
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="more_brands">
                        Все бренды
                    </div>
                </div>
            </div>
        </div>
        <div class="div_top_filter_list_details">
            <div class="white_background_page div_top_filter">
                <div class="filter_body">
                    <div class="old_url">
                        <?= base_url('search_detail/list_details/' . $article . '/' . $key_group) ?>
                    </div>
                    <div class="body_filter_by_price">
                        <div class="title"><strong>Фильтр по ценам:</strong></div>
                        <div class="selected_range_price">Выбраный промежуток цен: <strong class="price_between_beg"><?= $price_between_beg ?></strong> руб. - <strong class="price_between_end"><?= $price_between_end ?></strong> руб.</div>
                        <div class="filter_by_price"></div>
                        <div class="filter_price_range">
                            <div class="price_begin">0 руб.</div>
                            <div class="price_end">
                                <?php
                                if (isset($max_price_detail)) {
                                    echo $max_price_detail;
                                } else {
                                    echo 0;
                                }
                                ?>
                                руб.
                            </div>
                        </div>
                    </div>
                    <div class="body_filter_by_delivery_period">
                        <div class="title"><strong>Фильтр по сроку доставки:</strong></div>
                        <div class="row_filter_by_delivery_period">
                            <div>
                                <input id="all_deliv" type="checkbox" name="filter_by_delivery_period" value="all">
                                <label for="all_deliv">Все</label>
                            </div>
                            <div>
                                <input id="0_1_deliv" type="checkbox" name="filter_by_delivery_period" <?php if ($intervals_deliv_period['0_1'] == 0) { ?>disabled="disabled"<?php } ?> value="0-1">
                                <label for="0_1_deliv" <?php if ($intervals_deliv_period['0_1'] == 0) { ?>class="not_active"<?php } ?>>0-1 дн.</label>
                            </div>
                            <div>
                                <input id="2_deliv" type="checkbox" name="filter_by_delivery_period" <?php if ($intervals_deliv_period['2'] == 0) { ?>disabled="disabled"<?php } ?> value="2">
                                <label for="2_deliv" <?php if ($intervals_deliv_period['2'] == 0) { ?>class="not_active"<?php } ?>>2 дн.</label>
                            </div>
                        </div>
                        <div class="row_filter_by_delivery_period">
                            <div>
                                <input id="3_4_deliv" type="checkbox" name="filter_by_delivery_period" <?php if ($intervals_deliv_period['3_4'] == 0) { ?>disabled="disabled"<?php } ?> value="3-4">
                                <label for="3_4_deliv" <?php if ($intervals_deliv_period['3_4'] == 0) { ?>class="not_active"<?php } ?>>3-4 дн.</label>
                            </div>
                            <div>
                                <input id="5_7_deliv" type="checkbox" name="filter_by_delivery_period" <?php if ($intervals_deliv_period['5_7'] == 0) { ?>disabled="disabled"<?php } ?> value="5-7">
                                <label for="5_7_deliv" <?php if ($intervals_deliv_period['5_7'] == 0) { ?>class="not_active"<?php } ?>>5-7 дн.</label>
                            </div>
                        </div>
                    </div>
                    <div class="body_top_btn_sort_list_details">
                        <div class="title">
                            <strong>
                                Сортировка по:
                            </strong>
                        </div>
                        <a href="<?= base_url('search_detail/list_details/' . $article . '/' . $key_group . '?type_sort=2&price_between_beg=' . $price_between_beg . '&price_between_end=' . $price_between_end . '&private_data=' . $private_data . '&type_warehouse=' . $filter_type_warehouse . '&recom_brands=' . $recom_brands) ?>" class="btn_sort_by_retail_price">
                            По цене
                        </a>
                        <a href="<?= base_url('search_detail/list_details/' . $article . '/' . $key_group . '?type_sort=4&price_between_beg=' . $price_between_beg . '&price_between_end=' . $price_between_end . '&private_data=' . $private_data . '&type_warehouse=' . $filter_type_warehouse . '&recom_brands=' . $recom_brands) ?>" class="btn_sort_by_delivery_period">
                            По сроку дост.
                        </a>
                    </div>
                </div>
                <div class="show_filter_details">
                    Скрыть фильтр
                </div>
            </div>
            <div class="white_background_page">
                <?php echo form_open('search_detail/list_selected_offers/' . $private_data, array('id' => 'form_list_details')); ?>
                <input type="hidden" name="article_search" id="article_search" value="<?= $article ?>">
                <input type="hidden" name="key_group_search" value="<?= $key_group ?>">
                <div class="div_btn_select_details_background_search_detail">
                    <div class="div_btn_select_details">
                        <button name="select_details" class="btn btn-success">Выбрать</button>
                    </div>
                    <?php if ($is_complete == '0') { ?>
                        <div class="div_background_search_detail">
                            <span class="body_loader_background_search_detail">
                                Идет опрос оставшихся поставщиков
                                <img src="<?= base_url('images/loader.gif'); ?>" class="img_loader_background_search_detail" title="Идет поиск">
                            </span>
                            <button type="button" class="btn btn-warning btn_show_all_offers">Показать все предложения</button>
                        </div>
                    <?php } ?>
                    <div class="clear"></div>
                </div>
                <div class="list_details">
                    <?= $template_list_details ?>
                </div>
                <div class="div_bottom_btn_select_details">
                    <button name="select_details" class="btn btn-success">Выбрать</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <?php } else { ?>
    <div class="white_background_page">
    <?php if ($is_complete === '0') { ?>
            <div>
                <div class="div_background_search_detail">
                    <span class="body_loader_background_search_detail">
                        Идет опрос оставшихся поставщиков
                        <img src="<?= base_url('images/loader.gif'); ?>" class="img_loader_background_search_detail" title="Идет поиск">
                    </span>
                    <button type="button" class="btn btn-warning btn_show_all_offers">Показать все предложения</button>
                </div>
                <div class="clear"></div>
            </div>
    <?php } ?>
        <table class="table_details">
            <tbody>
                <tr class="empty_row_table">
                    <td>
                        <?php if ($is_complete == 'error_complete') { ?>
                            При поиске произошла ошибка. Попробуйте выполнить поиск позже.
                        <?php } else { ?>
                            Записей не обнаружено.
                        <?php } ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
<?php } ?>

<script type="text/javascript">

    var max_price_for_filter = <?= $max_price_detail ?>;
    var price_between_beg = <?= $price_between_beg ?>;
    var price_between_end = <?= $price_between_end ?>;
    var article = '<?= $article ?>';
    var key_group = '<?= $key_group ?>';
    var search_is_complete = '<?= $is_complete ?>';
    var filter_type_warehouse = '<?= $filter_type_warehouse ?>';
    var private_data = '<?= $private_data ?>';
    var recom_brands = '<?= $recom_brands ?>';
    var type_warehouse = '<?= $filter_type_warehouse ?>';
    var type_sort = '<?= $type_sort ?>';

    $(function () {
        if (search_is_complete == '0') {
            $.post(base_url + "index.php/search_detail/background_search_detail", {article: article, key_group: key_group}, function (event) {
                if (event.status == 'ok') {
                    $('.body_loader_background_search_detail').hide();
                    $('.btn_show_all_offers').show();
                } else {
                    alert(event.message);
                    window.location.replace(base_url);
                }
            }, 'json');
        }
    });

</script>