<div class="white_background_page div_list_details_in_order_widget">
    <h4 class="title_h4"><?php echo $title; ?></h4>

    <?php $i = 0; ?>

    <div>
        <?php echo form_open('orders_widget/search_detail/' . $id_amocrm . '/' . $current_page, array('method' => 'get', 'class' => 'form_search_detail_widget form-inline')); ?>
        <input type="text" class="form-control" name="search_detail" placeholder="Найти деталь" id="search_detail" value="<?= isset($search_detail) ? $search_detail : '' ?>">
        <button class="btn btn-primary">Выполнить</button>
        </form>
    </div>

    <?php if (isset($pagination) and ! empty($pagination)) { ?>
        <div class="pagination">
            <?= $pagination ?>
        </div>
    <?php } ?>
    <table class="table_details list_details_in_order_widget">
        <tr class="table_details_head">
            <th>
                Артикул
            </th>
            <th>
                Бренд
            </th>
            <th>
                Название
            </th>
            <th>
                Поставщик
            </th>
            <th>
                Опт.цена
            </th>
            <th>
                Розн.цена
            </th>
            <th>
                Количество
            </th>
            <th>
                &nbsp;
            </th>
        </tr>
        <?php if (!empty($list_details)) { ?>
            <?php foreach ($list_details as $value) { ?>
                <tr id="row_detail_<?= $value['id'] ?>" <?php if ($i % 2 == 0) { ?>class="background_row"<?php } ?>>
                    <td class="cell_vendor_code">
                        <span class="vendor_code_value" data-id_detail="<?= $value['id'] ?>"><?= $value['vendorCode'] ?></span>
                        <input type="text" class="vendor_code_input" name="vendor_code_input" placeholder="Артикул" value="<?= $value['vendorCode'] ?>">
                        <img class="img_vendor_code_change" data-id_detail="<?= $value['id'] ?>" src="<?= base_url('images/tick.png'); ?>" title="Изменить артикул" alt="Изменить артикул">
                        <img class="<?php if (isset($value['changes']['vendorCode']) && $value['changes']['vendorCode'] == 1) { ?>img_is_changed<?php } else { ?>img_is_changed hide_img_is_changed<?php } ?>" data-id_detail="<?= $value['id'] ?>" data-type="vendor_code" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">
                    </td>
                    <td class="cell_brand">
                        <span class="brand_value" data-id_detail="<?= $value['id'] ?>"><?= $value['brand'] ?></span>
                        <input type="text" class="brand_input" name="brand_input" placeholder="Бренд" value="<?= $value['brand'] ?>">
                        <img class="img_brand_change" data-id_detail="<?= $value['id'] ?>" src="<?= base_url('images/tick.png'); ?>" title="Изменить бренд" alt="Изменить бренд">
                        <img class="<?php if (isset($value['changes']['brand']) && $value['changes']['brand'] == 1) { ?>img_is_changed<?php } else { ?>img_is_changed hide_img_is_changed<?php } ?>" data-id_detail="<?= $value['id'] ?>" data-type="brand" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">
                    </td>
                    <td class="cell_name">
                        <span class="name_value" data-id_detail="<?= $value['id'] ?>"><?= $value['name'] ?></span>
                        <input type="text" class="name_input" name="name_input" placeholder="Название" value="<?= $value['name'] ?>">
                        <img class="img_name_change" data-id_detail="<?= $value['id'] ?>" src="<?= base_url('images/tick.png'); ?>" title="Изменить название" alt="Изменить название">
                        <img class="<?php if (isset($value['changes']['name']) && $value['changes']['name'] == 1) { ?>img_is_changed<?php } else { ?>img_is_changed hide_img_is_changed<?php } ?>" data-id_detail="<?= $value['id'] ?>" data-type="name" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">
                    </td>
                    <td class="cell_provider">
                        <span class="provider_value" data-id_detail="<?= $value['id'] ?>"><?= $value['provider'] ?></span>
                        <input type="text" class="provider_input" name="provider_input" placeholder="Поставщик" value="<?= $value['provider'] ?>">
                        <img class="img_provider_change" data-id_detail="<?= $value['id'] ?>" src="<?= base_url('images/tick.png'); ?>" title="Изменить поставщика" alt="Изменить поставщика">
                        <a href="<?= $value['full_provider_link'] ?>" target="_blank">
                            <img class="link_site_provider" src="<?= base_url('images/link.png'); ?>" title="Сайт поставщика" alt="Сайт поставщика">
                        </a>
                        <img class="<?php if (isset($value['changes']['provider']) && $value['changes']['provider'] == 1) { ?>img_is_changed<?php } else { ?>img_is_changed hide_img_is_changed<?php } ?>" data-id_detail="<?= $value['id'] ?>" data-type="provider" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">
                    </td>
                    <td class="cell_request_price">
                        <span class="request_price_value" data-id_detail="<?= $value['id'] ?>"><?= $value['requestPrice'] ?></span>
                        <input type="text" class="request_price_input" name="request_price_input" placeholder="Опт.цена" value="<?= $value['requestPrice'] ?>">
                        <img class="img_request_price_change" data-id_detail="<?= $value['id'] ?>" src="<?= base_url('images/tick.png'); ?>" title="Изменить опт.цену" alt="Изменить опт.цену">
                        <img class="<?php if (isset($value['changes']['requestPrice']) && $value['changes']['requestPrice'] == 1) { ?>img_is_changed<?php } else { ?>img_is_changed hide_img_is_changed<?php } ?>" data-id_detail="<?= $value['id'] ?>" data-type="request_price" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">
                    </td>
                    <td class="cell_retail_price">
                        <span class="retail_price_value" data-id_detail="<?= $value['id'] ?>"><?= $value['retailPrice'] ?></span>
                        <input type="text" class="retail_price_input" name="retail_price_input" placeholder="Опт.цена" value="<?= $value['retailPrice'] ?>">
                        <img class="img_retail_price_change" data-id_detail="<?= $value['id'] ?>" src="<?= base_url('images/tick.png'); ?>" title="Изменить розн.цену" alt="Изменить розн.цену">
                        <img class="<?php if (isset($value['changes']['retailPrice']) && $value['changes']['retailPrice'] == 1) { ?>img_is_changed<?php } else { ?>img_is_changed hide_img_is_changed<?php } ?>" data-id_detail="<?= $value['id'] ?>" data-type="retail_price" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">
                        <img src="<?= base_url('images/alert-triangle-red.png'); ?>" data-id_detail="<?= $value['id'] ?>" class="need_calculate_retail_price<?php if($value['need_calculate_retail_price'] == 0) { ?> hide_need_calculate_retail_price<?php } ?>" title="Пересчитать розн. цену?" alt="Пересчитать розн. цену?">
                    </td>
                    <td class="cell_quantity">
                        <span class="quantity_value" data-id_detail="<?= $value['id'] ?>"><?= $value['quantity'] ?></span>
                        <input type="text" class="quantity_input" name="quantity_input" placeholder="Количество" value="<?= $value['quantity'] ?>">
                        <img class="img_quantity_change" data-id_detail="<?= $value['id'] ?>" src="<?= base_url('images/tick.png'); ?>" title="Изменить количество" alt="Изменить количество">
                        <img class="<?php if (isset($value['changes']['quantity']) && $value['changes']['quantity'] == 1) { ?>img_is_changed<?php } else { ?>img_is_changed hide_img_is_changed<?php } ?>" data-id_detail="<?= $value['id'] ?>" data-type="quantity" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">
                    </td>
                    <td class="cell_control">
                        <?php if ($value['isOrdered'] == 1) { ?>
                            <a class="detail_is_ordered" data-id_detail="<?= $value['id'] ?>" data-is_ordered="1" href="javascript:void(0);">Заказано</a>
                        <?php } else { ?>
                            <a class="detail_is_ordered" data-id_detail="<?= $value['id'] ?>" data-is_ordered="0" href="javascript:void(0);">Заказать</a>
                        <?php } ?>
                        <a class="delete_detail" data-id_detail="<?= $value['id'] ?>" href="javascript:void(0);">
                            <img src="<?= base_url('images/delete.png'); ?>" title="Удалить">
                        </a>
                    </td>
                </tr>
                <?php $i++ ?>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td class="empty_row_table" colspan="8">
                    Список деталей пуст.
                </td>
            </tr>
        <?php } ?>
    </table>
    <?php if (isset($list_details) and ! empty($list_details)) { ?>
        <div class="pagination">
            <?= $pagination ?>
        </div>
    <?php } ?>
</div>