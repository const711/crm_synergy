<div class="white_background_page div_list_orders_widget">
    <h4 class="title_h4"><?php echo $title; ?></h4>

    <?php $i = 0; ?>

    <div>
        <?php echo form_open('orders_widget/search_order/' . $current_page, array('method' => 'get', 'class' => 'form_search_order form-inline')); ?>
        <input type="text" class="form-control" name="search_order" id="search_order" placeholder="Найти заказ" value="<?= isset($search_order) ? $search_order : '' ?>">
        <button class="btn btn-primary">Выполнить</button>
        </form>
    </div>

    <?php if (isset($pagination) and ! empty($pagination)) { ?>
        <div class="pagination">
            <?= $pagination ?>
        </div>
    <?php } ?>
    <table class="table_details list_orders_widget">
        <tr class="table_details_head">
            <th>
                ID сделки AMO
            </th>
            <th>
                Сумма заказа
            </th>
            <th>
                Скидка
            </th>
            <th>
                Наценка
            </th>            
            <th>
                Прибыль
            </th>
            <th>
                Доставка
            </th>
            <th>
                &nbsp;
            </th>
        </tr>
        <?php if (!empty($list_orders)) { ?>
            <?php foreach ($list_orders as $value) { ?>
                <tr id="row_order_<?= $value['id'] ?>" <?php if ($i % 2 == 0) { ?>class="background_row"<?php } ?>>
                    <td>
                        <a href="<?= base_url('orders_widget/list_details/' . $value['amoCrmEntityId']) ?>">
                            <?= $value['amoCrmEntityId'] ?>
                        </a>
                        <a href="https://<?= $this->config->item('domain_amocrm') ?>.amocrm.ru/leads/detail/<?= $value['amoCrmEntityId'] ?>">
                            <i>(AmoCRM)</i>
                        </a>
                    </td>
                    <td class="td_total_sum">
                        <?= $value['total_sum'] ?>
                    </td>
                    <td>
                        <span class="discount_value" data-id_order="<?= $value['id'] ?>"><?= $value['discountValue'] ?></span>
                        <img class="<?php if (isset($value['changes']['discountValue']) && $value['changes']['discountValue'] == 1) { ?>img_is_changed<?php } else { ?>img_is_changed hide_img_is_changed<?php } ?>" data-id_order="<?= $value['id'] ?>" data-type="discount" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">

                        <input type="text" class="discount_input" name="discount_input" placeholder="Скидка" value="<?= $value['discountValue'] ?>">
                        <img class="img_discount_change" data-id_order="<?= $value['id'] ?>" src="<?= base_url('images/tick.png'); ?>" title="Изменить скидку" alt="Изменить скидку">
                    </td>
                    <td>
                        <span class="percent_value" data-id_order="<?= $value['id'] ?>"><?= $value['percentValue'] ?></span>
                        <input type="text" class="percent_input" name="percent_input" placeholder="Наценка" value="<?= $value['percentValue'] ?>">
                        <img class="img_percent_change" data-id_order="<?= $value['id'] ?>" src="<?= base_url('images/tick.png'); ?>" title="Изменить наценку" alt="Изменить наценку">
                        <img class="<?php if (isset($value['changes']['percentValue']) && $value['changes']['percentValue'] == 1) { ?>img_is_changed<?php } else { ?>img_is_changed hide_img_is_changed<?php } ?>" data-id_order="<?= $value['id'] ?>" data-type="percent" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">
                    </td>
                    <td class="td_profit <?= ($value['profit'] < 0) ? 'profit_bad' : 'profit_good' ?>">
                        <?= $value['profit'] ?>
                    </td>
                    <td>
                        <span class="delivery_value" data-id_order="<?= $value['id'] ?>"><?= $value['deliveryDate'] ?></span>
                        <img class="<?php if (isset($value['changes']['deliveryDate']) && $value['changes']['deliveryDate'] == 1) { ?>img_is_changed<?php } else { ?>img_is_changed hide_img_is_changed<?php } ?>" data-id_order="<?= $value['id'] ?>" data-type="delivery" src="<?= base_url('images/pencil.png'); ?>" title="Данные изменены" alt="Данные изменены">

                        <input type="text" class="delivery_input" name="delivery_input" placeholder="Доставка" value="<?= $value['deliveryDate'] ?>">
                        <img class="img_delivery_change" data-id_order="<?= $value['id'] ?>" src="<?= base_url('images/tick.png'); ?>" title="Изменить доставку" alt="Изменить доставку">
                    </td>
                    <td>
                        <a href="<?= base_url('orders_widget/list_details/' . $value['amoCrmEntityId']) ?>">Детали</a>
                        <a class="delete_order" data-id_amo_crm="<?= $value['amoCrmEntityId'] ?>" href="javascript:void(0);">
                            <img src="<?= base_url('images/delete.png'); ?>" title="Удалить">
                        </a>
                    </td>
                </tr>
                <?php $i++ ?>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td class="empty_row_table" colspan="5">
                    Список заказов пуст.
                </td>
            </tr>
        <?php } ?>
    </table>
    <?php if (isset($list_orders) and ! empty($list_orders)) { ?>
        <div class="pagination">
            <?= $pagination ?>
        </div>
    <?php } ?>
</div>