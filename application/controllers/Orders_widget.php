<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_widget extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('authorization_model');
        $this->load->model('orders_widget_model');
        $this->load->model('shopping_cart_model');
        $this->load->library('session');
        $this->load->helper('work_helper');
    }

    public function list_orders($page = '') {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        $data['title'] = 'Список сделок';

        $data['other_js'] = array('js/orders_widget.js');

        $this->load->library('pagination');

        $config['base_url'] = base_url('orders_widget/list_orders/');
        $config['total_rows'] = count($this->orders_widget_model->get_list_orders());
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['use_page_numbers'] = TRUE;

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $num = $config['per_page'];

        if ($page == '') {
            $offset = 0;
        } else {
            $offset = ((int) $page - 1) * $config['per_page'];
        }

        $list_orders = $this->orders_widget_model->get_list_orders($num, $offset);

        if (!empty($list_orders)) {
            foreach ($list_orders as $key => $order) {
                $all_changes_order = $this->orders_widget_model->get_all_changes_order($order['id']);

                if (!empty($all_changes_order)) {
                    foreach ($all_changes_order as $change) {
                        $list_orders[$key]['changes'][$change['field']] = $change['is_changed'];
                    }
                }

                $total_sum_and_profit_order = $this->orders_widget_model->calculate_total_sum_and_profit_order($order);

                $list_orders[$key]['total_sum'] = $total_sum_and_profit_order['total_sum'];
                $list_orders[$key]['profit'] = $total_sum_and_profit_order['profit'];
            }
        }

        $data['list_orders'] = $list_orders;
        $data['current_page'] = ($page == '') ? 1 : $page;

        $id_manager = $this->session->userdata['manager_data']['id_user'];
        $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

        $this->load->view('templates/header', $data);
        $this->load->view('orders_widget/list_orders', $data);
        $this->load->view('templates/footer');
    }

    public function change_data_order() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $type = $this->input->post('type');

        switch ($type) {
            case 'discount':
                $new_data = $this->input->post('new_discount');
                $new_data = standard_price($new_data);
                $field = 'discountValue';

                if ($new_data == '') {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Укажите правильную скидку'
                    );

                    echo json_encode($result);
                    exit;
                }

                break;
            case 'percent':
                $new_data = $this->input->post('new_percent');
                $new_data = preg_replace('/[^0-9,\.\-]/iu', '', $new_data);
                $new_data = preg_replace('/,/iu', '.', $new_data);
                $field = 'percentValue';

                if ($new_data == '') {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Укажите правильную наценку'
                    );

                    echo json_encode($result);
                    exit;
                }

                break;
            case 'delivery':
                $new_data = $this->input->post('new_delivery');
                $field = 'deliveryDate';

                if ($new_data == '') {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Укажите правильную доставку'
                    );

                    echo json_encode($result);
                    exit;
                }

                break;
            default:
                $result = array(
                    'status' => 'error',
                    'message' => 'Укажите правильные данные'
                );

                echo json_encode($result);
                exit;
        }

        $id_order = $this->input->post('id_order');

        $data_order = $this->orders_widget_model->get_order_by_id($id_order);

        if (!empty($data_order)) {
            if ($this->orders_widget_model->update_order($id_order, $field, $new_data)) {
                $data_order = $this->orders_widget_model->get_order_by_id($id_order);

                $total_sum_and_profit_order = $this->orders_widget_model->calculate_total_sum_and_profit_order($data_order);

                $result = array(
                    'status' => 'ok',
                    'message' => 'Данные успешно обновлены',
                    'total_sum' => $total_sum_and_profit_order['total_sum'],
                    'profit' => $total_sum_and_profit_order['profit'],
                );
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    public function reset_change_order() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $id_order = $this->input->post('id_order');
        $type = $this->input->post('type');

        $data_order = $this->orders_widget_model->get_order_by_id($id_order);

        if (!empty($data_order)) {
            if ($this->orders_widget_model->reset_change_order($id_order, $type)) {
                $result = array(
                    'status' => 'ok',
                    'message' => 'Данные успешно обновлены'
                );
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    /**
     * Функция удаляет заказ
     * @return json $result
     */
    public function delete_order() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $id_amocrm = $this->input->post('id_amo_crm');

        $data_order = $this->orders_widget_model->get_order_by_id_amocrm($id_amocrm);

        if (empty($data_order)) {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );

            echo json_encode($result);
            exit;
        }

        if ($this->orders_widget_model->delete_order_by_id_amo($id_amocrm)) {
            $result = array(
                'status' => 'ok',
                'id_order' => $data_order['id'],
            );
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    /* Функция производит поиск по списку заказов */

    public function search_order($page = '') {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        if (isset($_GET['search_order']) && ($_GET['search_order'] != '')) {
            $search_order = $this->input->get('search_order');
            $data['title'] = 'Список заказов';
            $data['other_js'] = array('js/orders_widget.js');

            $this->load->library('pagination');

            $config['base_url'] = base_url('orders_widget/search_order/');
            $config['total_rows'] = count($this->orders_widget_model->search_order($search_order));
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['first_link'] = 'Первая';
            $config['last_link'] = 'Последняя';
            $config['use_page_numbers'] = TRUE;
            if (count($_GET) > 0) {
                $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            }

            $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);

            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();

            $num = $config['per_page'];

            if ($page == '') {
                $offset = 0;
            } else {
                $offset = ((int) $page - 1) * $config['per_page'];
            }

            $list_orders = $this->orders_widget_model->search_order($search_order, $num, $offset);

            if (!empty($list_orders)) {
                foreach ($list_orders as $key => $order) {
                    $all_changes_order = $this->orders_widget_model->get_all_changes_order($order['id']);

                    if (!empty($all_changes_order)) {
                        foreach ($all_changes_order as $change) {
                            $list_orders[$key]['changes'][$change['field']] = $change['is_changed'];
                        }
                    }

                    $total_sum_and_profit_order = $this->orders_widget_model->calculate_total_sum_and_profit_order($order);

                    $list_orders[$key]['total_sum'] = $total_sum_and_profit_order['total_sum'];
                    $list_orders[$key]['profit'] = $total_sum_and_profit_order['profit'];
                }
            }

            $data['list_orders'] = $list_orders;
            $data['search_order'] = $search_order;
            $data['current_page'] = ($page == '') ? 1 : $page;

            $id_manager = $this->session->userdata['manager_data']['id_user'];
            $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

            $this->load->view('templates/header', $data);
            $this->load->view('orders_widget/list_orders', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('/orders_widget/list_orders');
        }
    }

    /**
     * Функция удаляет деталь из заказа
     * @return json $result
     */
    public function delete_detail() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $id_detail = $this->input->post('id_detail');

        $data_detail = $this->orders_widget_model->get_detail_by_id($id_detail);

        if (empty($data_detail)) {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );

            echo json_encode($result);
            exit;
        }

        if ($this->orders_widget_model->delete_detail_by_id($id_detail)) {
            $list_details = $this->orders_widget_model->get_list_details_in_order($data_detail['amoCrmEntityId']);

            if (empty($list_details)) {
                $this->orders_widget_model->delete_order_by_id_amo($data_detail['amoCrmEntityId']);

                $result = array(
                    'status' => 'del_order_ok',
                    'message' => 'Данные успешно удалены'
                );
            } else {
                $result = array(
                    'status' => 'del_detail_ok',
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    /**
     * Функция переводит деталь в статус "Заказано"
     * @return json $result
     */
    public function change_status_detail() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $id_detail = $this->input->post('id_detail');
        $is_ordered = $this->input->post('is_ordered');

        $data_detail = $this->orders_widget_model->get_detail_by_id($id_detail);

        if (empty($data_detail)) {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );

            echo json_encode($result);
            exit;
        }

        if ($is_ordered == 0) {
            $data = 1;
        } else {
            $data = 0;
        }

        if ($this->orders_widget_model->update_detail($id_detail, 'isOrdered', $data)) {
            $result = array(
                'status' => 'ok',
            );
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    public function list_details($id_amocrm, $page = '') {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        $data['title'] = 'Список деталей';

        $data['other_js'] = array('js/orders_widget.js');

        $this->load->library('pagination');

        $config['base_url'] = base_url('orders_widget/list_orders/');
        $config['total_rows'] = count($this->orders_widget_model->get_list_details_in_order($id_amocrm));
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['use_page_numbers'] = TRUE;

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $num = $config['per_page'];

        if ($page == '') {
            $offset = 0;
        } else {
            $offset = ((int) $page - 1) * $config['per_page'];
        }

        $list_details = $this->orders_widget_model->get_list_details_in_order($id_amocrm);

        if (!empty($list_details)) {
            foreach ($list_details as $key => $detail) {
                $list_details[$key]['full_provider_link'] = $this->orders_widget_model->create_link_for_offer($detail['vendorCode'], $detail['providerLink']);

                $all_changes_detail = $this->orders_widget_model->get_all_changes_detail($detail['id']);

                if (!empty($all_changes_detail)) {
                    foreach ($all_changes_detail as $change) {
                        $list_details[$key]['changes'][$change['field']] = $change['is_changed'];
                    }
                }

                if ($detail['requestPrice'] != $detail['baseRequestPrice']) {
                    $list_details[$key]['need_calculate_retail_price'] = 1;
                } else {
                    $list_details[$key]['need_calculate_retail_price'] = 0;
                }
            }
        }

        $data['list_details'] = $list_details;
        $data['current_page'] = ($page == '') ? 1 : $page;
        $data['id_amocrm'] = $id_amocrm;

        $id_manager = $this->session->userdata['manager_data']['id_user'];
        $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

        $this->load->view('templates/header', $data);
        $this->load->view('orders_widget/list_details', $data);
        $this->load->view('templates/footer');
    }

    /* Функция производит поиск по списку деталей в заказе */

    public function search_detail($id_amocrm = 0, $page = '') {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        if (isset($_GET['search_detail']) && ($_GET['search_detail'] != '')) {
            $search_detail = $this->input->get('search_detail');
            $data['title'] = 'Список деталей';
            $data['other_js'] = array('js/orders_widget.js');

            $this->load->library('pagination');

            $config['base_url'] = base_url('orders_widget/search_detail/' . $id_amocrm);
            $config['total_rows'] = count($this->orders_widget_model->search_detail($id_amocrm, $search_detail));
            $config['per_page'] = 10;
            $config['uri_segment'] = 5;
            $config['first_link'] = 'Первая';
            $config['last_link'] = 'Последняя';
            $config['use_page_numbers'] = TRUE;
            if (count($_GET) > 0) {
                $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            }

            $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);

            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();

            $num = $config['per_page'];

            if ($page == '') {
                $offset = 0;
            } else {
                $offset = ((int) $page - 1) * $config['per_page'];
            }

            $list_details = $this->orders_widget_model->search_detail($id_amocrm, $search_detail, $num, $offset);

            if (!empty($list_details)) {
                foreach ($list_details as $key => $detail) {
                    $list_details[$key]['full_provider_link'] = $this->orders_widget_model->create_link_for_offer($detail['vendorCode'], $detail['providerLink']);

                    $all_changes_detail = $this->orders_widget_model->get_all_changes_detail($detail['id']);

                    if (!empty($all_changes_detail)) {
                        foreach ($all_changes_detail as $change) {
                            $list_details[$key]['changes'][$change['field']] = $change['is_changed'];
                        }
                    }
                }
            }

            $data['list_details'] = $list_details;
            $data['search_detail'] = $search_detail;
            $data['current_page'] = ($page == '') ? 1 : $page;
            $data['id_amocrm'] = $id_amocrm;

            $id_manager = $this->session->userdata['manager_data']['id_user'];
            $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

            $this->load->view('templates/header', $data);
            $this->load->view('orders_widget/list_details', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('/orders_widget/list_details/' . $id_amocrm);
        }
    }

    public function reset_change_detail() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $id_detail = $this->input->post('id_detail');
        $type = $this->input->post('type');

        $data_detail = $this->orders_widget_model->get_detail_by_id($id_detail);

        if (!empty($data_detail)) {
            if ($this->orders_widget_model->reset_change_detail($id_detail, $type)) {
                $result = array(
                    'status' => 'ok',
                    'message' => 'Данные успешно обновлены'
                );
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    public function change_data_detail() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $type = $this->input->post('type');

        switch ($type) {
            case 'vendor_code':
                $new_data = $this->input->post('new_vendor_code');
                $new_data = leave_eng_letters_numbers($new_data);
                $field = 'vendorCode';

                if ($new_data == '') {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Укажите правильный артикул'
                    );

                    echo json_encode($result);
                    exit;
                }

                break;
            case 'brand':
                $new_data = $this->input->post('new_brand');
                $field = 'brand';

                if ($new_data == '') {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Укажите правильный бренд'
                    );

                    echo json_encode($result);
                    exit;
                }

                break;
            case 'name':
                $new_data = $this->input->post('new_name');
                $field = 'name';

                if ($new_data == '') {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Укажите правильное название'
                    );

                    echo json_encode($result);
                    exit;
                }

                break;
            case 'provider':
                $new_data = $this->input->post('new_provider');
                $field = 'provider';

                if ($new_data == '') {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Укажите правильное название поставщика'
                    );

                    echo json_encode($result);
                    exit;
                }

                break;
            case 'retail_price':
                $new_data = $this->input->post('new_retail_price');
                $field = 'retailPrice';

                if ($new_data == '') {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Укажите правильную розн.цену'
                    );

                    echo json_encode($result);
                    exit;
                }

                break;
            case 'request_price':
                $new_data = $this->input->post('new_request_price');
                $field = 'requestPrice';

                if ($new_data == '') {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Укажите правильную опт.цену'
                    );

                    echo json_encode($result);
                    exit;
                }

                break;
            case 'quantity':
                $new_data = $this->input->post('new_quantity');
                $field = 'quantity';

                if ($new_data == '') {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Укажите правильное количество'
                    );

                    echo json_encode($result);
                    exit;
                }

                break;
            default:
                $result = array(
                    'status' => 'error',
                    'message' => 'Укажите правильные данные'
                );

                echo json_encode($result);
                exit;
        }

        $id_detail = $this->input->post('id_detail');

        $data_detail = $this->orders_widget_model->get_detail_by_id($id_detail);

        if (!empty($data_detail)) {
            if ($this->orders_widget_model->update_detail($id_detail, $field, $new_data)) {
                $result = array(
                    'status' => 'ok',
                    'message' => 'Данные успешно обновлены'
                );
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    public function calculate_retail_price() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $id_detail = $this->input->post('id_detail');

        $data_detail = $this->orders_widget_model->get_detail_by_id($id_detail);

        if (empty($data_detail)) {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );

            echo json_encode($result);
            exit;
        }

        if ($new_retail_price = $this->orders_widget_model->calculate_retail_price($data_detail)) {
            $result = array(
                'status' => 'ok',
                'message' => 'Данные успешно обновлены',
                'new_retail_price' => $new_retail_price,
            );
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

}
