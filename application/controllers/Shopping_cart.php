<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping_cart extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('shopping_cart_model');
        $this->load->model('authorization_model');
        $this->load->model('search_detail_model');
        $this->load->library('session');
        $this->load->helper('work_helper');
    }

    /* Функция добавляет деталь в корзину */
    public function add_to_cart() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $code_offer = $this->input->post('code_offer');
        $is_custom = $this->input->post('is_custom');
        $new_count_items = (int) $this->input->post('new_count_items');

        if (empty($code_offer)) {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );

            echo json_encode($result);
            exit;
        }

        if (!$new_count_items || empty($new_count_items)) {
            $result = array(
                'status' => 'error',
                'message' => 'Укажите правильное количество деталей'
            );

            echo json_encode($result);
            exit;
        }

        $id_manager = $this->session->userdata['manager_data']['id_user'];

        $result = $this->shopping_cart_model->add_to_cart($code_offer, $new_count_items, $id_manager, $is_custom);

        if ($result['status'] == 'ok') {
            $result['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

            $shopping_cart = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);

            $total_sum_and_profit = $this->shopping_cart_model->calculate_total_sum_and_profit($shopping_cart);

            $result['total_sum'] = $total_sum_and_profit['total_sum'];
            $result['profit'] = $total_sum_and_profit['profit'];
        }

        echo json_encode($result);
        exit;
    }

    /* Функция выводит список деталей корзины */
    public function list_details_in_cart() {
        if (!$this->authorization_model->check_auth()) {
            redirect('/my_error_404/index');
        }

        $id_manager = $this->session->userdata['manager_data']['id_user'];

        if ($_POST) {
            $selected_details = $this->input->post('selected_details');

            if (isset($selected_details) && !empty($selected_details)) {
                foreach ($selected_details as $detail) {
                    $this->shopping_cart_model->del_detail_from_cart_by_id_detail_id_manager($detail, $id_manager);
                }

                $order_cart = $this->shopping_cart_model->get_order_cart_by_id_manager($id_manager);

                if (!empty($order_cart)) {
                    $id_order = $order_cart['id'];

                    $details_in_order = $this->shopping_cart_model->get_details_cart_by_id_order($id_order, $id_manager);

                    if (empty($details_in_order)) {
                        $this->shopping_cart_model->del_order_from_cart_by_id_order_id_manager($id_order, $id_manager);
                    }
                }
            }
        }

        $data['other_js'] = array(
            'js/jquery-ui.js',
            'js/datepicker.js',
            'js/shopping_cart.js',
        );

        $data['other_css'] = array(
            'css/jquery-ui.css',
        );

        $shopping_cart = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);
        $data['shopping_cart'] = $this->shopping_cart_model->check_orders_in_shopping_cart($shopping_cart);
        $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);
        $data['title'] = 'Корзина';
        $data['list_providers'] = $this->shopping_cart_model->get_list_providers();
        $data['pop_up_add_new_detail'] = $this->load->view('shopping_cart/pop_up_add_new_detail', $data, TRUE);

        $this->load->view('templates/header', $data);
        $this->load->view('shopping_cart/list_details_in_cart');
        $this->load->view('templates/footer');
    }

    /* Функция удаляет выбранную деталь из корзины */
    public function remove_from_cart() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $code_offer = $this->input->post('code_offer');

        if (empty($code_offer)) {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );

            echo json_encode($result);
            exit;
        }

        $id_manager = $this->session->userdata['manager_data']['id_user'];

        $res_remove = $this->shopping_cart_model->remove_from_cart($code_offer, $id_manager);

        if ($res_remove['status'] == 'ok') {
            $res_remove['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

            $shopping_cart = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);

            $total_sum_and_profit = $this->shopping_cart_model->calculate_total_sum_and_profit($shopping_cart);

            $res_remove['total_sum'] = $total_sum_and_profit['total_sum'];
            $res_remove['profit'] = $total_sum_and_profit['profit'];
        }

        echo json_encode($res_remove);
        exit;
    }

    /* Функция добавляет скидку к заказу */
    public function add_discount() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $id_manager = $this->session->userdata['manager_data']['id_user'];
        $need_add_discount = $this->input->post('need_add_discount');

        if (!empty($need_add_discount)) {
            $id_manager = $this->session->userdata['manager_data']['id_user'];

            $order_cart = $this->shopping_cart_model->get_order_cart_by_id_manager($id_manager);

            if (!empty($order_cart)) {
                $id_order = $order_cart['id'];

                if ($this->shopping_cart_model->update_order_shopping_cart(array('discount' => 300), $id_order, $id_manager)) {
                    $shopping_cart = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);

                    $total_sum_and_profit = $this->shopping_cart_model->calculate_total_sum_and_profit($shopping_cart);

                    $result = array(
                        'status' => 'ok',
                        'total_sum' => $total_sum_and_profit['total_sum'],
                        'profit' => $total_sum_and_profit['profit'],
                    );
                } else {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Произошла неизвестная ошибка'
                    );
                }
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    /* Функция добавляет наценку для заказа */
    public function add_price_percent() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $id_manager = $this->session->userdata['manager_data']['id_user'];
        $price_percent = $this->input->post('price_percent');
        $price_percent = preg_replace('/[^0-9,\.\-]/iu', '', $price_percent);
        $price_percent = preg_replace('/,/iu', '.', $price_percent);

        if ($price_percent != '') {
            $id_manager = $this->session->userdata['manager_data']['id_user'];

            $order_cart = $this->shopping_cart_model->get_order_cart_by_id_manager($id_manager);

            if (!empty($order_cart)) {
                $id_order = $order_cart['id'];

                if ($this->shopping_cart_model->update_order_shopping_cart(array('percent' => $price_percent), $id_order, $id_manager)) {
                    $shopping_cart = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);

                    $total_sum_and_profit = $this->shopping_cart_model->calculate_total_sum_and_profit($shopping_cart);

                    $result = array(
                        'status' => 'ok',
                        'message' => 'Наценка успешно добавлена',
                        'total_sum' => $total_sum_and_profit['total_sum'],
                        'profit' => $total_sum_and_profit['profit'],
                    );
                } else {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Произошла неизвестная ошибка'
                    );
                }
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Введены некорректные данные!'
            );
        }

        echo json_encode($result);
        exit;
    }

    /* Функция сбрасывает флаг, который показывает изменение в данных детали */
    public function reset_detail_changed() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $id_detail = $this->input->post('id_detail');

        if (empty($id_detail)) {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );

            echo json_encode($result);
            exit;
        }

        $id_manager = $this->session->userdata['manager_data']['id_user'];

        $data_detail = $this->shopping_cart_model->get_detail_cart_by_id_detail_id_manager($id_detail, $id_manager);

        if (!empty($data_detail)) {
            if ($this->shopping_cart_model->update_detail_shopping_cart(array('is_changed' => 0), array('id' => $data_detail['id']), $id_manager)) {
                $result = array(
                    'status' => 'ok'
                );
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    /* Функция изменяет розничную цену детали в корзине */
    public function price_change() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $new_price = $this->input->post('new_price');

        $new_price = standard_price($new_price);

        if (empty($new_price)) {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );

            echo json_encode($result);
            exit;
        }

        $id_detail = $this->input->post('id_detail');
        $id_manager = $this->session->userdata['manager_data']['id_user'];

        $data_detail = $this->shopping_cart_model->get_detail_cart_by_id_detail_id_manager($id_detail, $id_manager);

        if (!empty($data_detail)) {
            $data = array(
                'price' => $new_price,
                'is_changed' => 1,
            );

            if ($this->shopping_cart_model->update_detail_shopping_cart($data, array('id' => $data_detail['id']), $id_manager)) {
                $shopping_cart = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);

                $total_sum_and_profit = $this->shopping_cart_model->calculate_total_sum_and_profit($shopping_cart);

                $result = array(
                    'status' => 'ok',
                    'total_sum' => $total_sum_and_profit['total_sum'],
                    'profit' => $total_sum_and_profit['profit'],
                );
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    /* Функция удаляет скидку из заказа */
    public function del_discount() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $need_del_discount = $this->input->post('need_del_discount');
        $id_manager = $this->session->userdata['manager_data']['id_user'];

        if (!empty($need_del_discount)) {
            $id_manager = $this->session->userdata['manager_data']['id_user'];

            $order_cart = $this->shopping_cart_model->get_order_cart_by_id_manager($id_manager);

            if (!empty($order_cart)) {
                $id_order = $order_cart['id'];

                $data = array(
                    'discount' => 0,
                    'discount_changed' => 0,
                );

                if ($this->shopping_cart_model->update_order_shopping_cart($data, $id_order, $id_manager)) {
                    $shopping_cart = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);

                    $total_sum_and_profit = $this->shopping_cart_model->calculate_total_sum_and_profit($shopping_cart);

                    $result = array(
                        'status' => 'ok',
                        'total_sum' => $total_sum_and_profit['total_sum'],
                        'profit' => $total_sum_and_profit['profit'],
                    );
                } else {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Произошла неизвестная ошибка'
                    );
                }
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    /* Функция изменяет скидку в заказе */
    public function discount_change() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $new_discount = $this->input->post('new_discount');
        $new_discount = standard_price($new_discount);

        if (!empty($new_discount)) {
            $id_manager = $this->session->userdata['manager_data']['id_user'];

            $order_cart = $this->shopping_cart_model->get_order_cart_by_id_manager($id_manager);

            if (!empty($order_cart)) {
                $id_order = $order_cart['id'];

                $data = array(
                    'discount' => $new_discount,
                    'discount_changed' => 1,
                );

                if ($this->shopping_cart_model->update_order_shopping_cart($data, $id_order, $id_manager)) {
                    $shopping_cart = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);

                    $total_sum_and_profit = $this->shopping_cart_model->calculate_total_sum_and_profit($shopping_cart);

                    $result = array(
                        'status' => 'ok',
                        'total_sum' => $total_sum_and_profit['total_sum'],
                        'profit' => $total_sum_and_profit['profit'],
                    );
                } else {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Произошла неизвестная ошибка'
                    );
                }
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    public function reset_discount_changed() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $need_reset_discount_changed = $this->input->post('need_reset_discount_changed');
        $id_manager = $this->session->userdata['manager_data']['id_user'];

        if (!empty($need_reset_discount_changed)) {
            $id_manager = $this->session->userdata['manager_data']['id_user'];

            $order_cart = $this->shopping_cart_model->get_order_cart_by_id_manager($id_manager);

            if (!empty($order_cart)) {
                $id_order = $order_cart['id'];

                $data = array(
                    'discount_changed' => 0,
                );

                if ($this->shopping_cart_model->update_order_shopping_cart($data, $id_order, $id_manager)) {
                    $result = array(
                        'status' => 'ok',
                    );
                } else {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Произошла неизвестная ошибка'
                    );
                }
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        echo json_encode($result);
        exit;
    }

    public function add_new_detail() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $post = $this->input->post();
        $errors_array = array();

        if (isset($post['article'])) {
            $post['article'] = leave_eng_letters_numbers($post['article']);
        }

        if (!isset($post['article']) or empty($post['article'])) {
            $errors_array['article'] = '<p>Заполните поле "Артикул"</p>';
        }

        if (!isset($post['brand']) or empty($post['brand'])) {
            $errors_array['brand'] = '<p>Заполните поле "Бренд"</p>';
        }

        if (!isset($post['name']) or empty($post['name'])) {
            $errors_array['name'] = '<p>Заполните поле "Название"</p>';
        }

        if (!isset($post['type_warehouse']) or empty($post['type_warehouse'])) {
            $errors_array['type_warehouse'] = '<p>Заполните поле "Тип склада"</p>';
        }

        if (empty($this->shopping_cart_model->get_type_warehouse_by_id($post['type_warehouse']))) {
            $errors_array['type_warehouse'] = '<p>Такой тип склада не существует!</p>';
        }

        if (!isset($post['price_from_provider']) or empty($post['price_from_provider'])) {
            $errors_array['price_from_provider'] = '<p>Заполните поле "Цена от поставщика"</p>';
        }

        if (!isset($post['count']) or empty($post['count'])) {
            $errors_array['count'] = '<p>Заполните поле "Количество"</p>';
        }

        if (empty($errors_array)) {
            $id_manager = $this->session->userdata['manager_data']['id_user'];

            if (!empty($this->shopping_cart_model->get_detail_cart_by_article_brand_name($post['article'], $post['brand'], $post['name'], $id_manager))) {
                $result = array(
                    'status' => 'error',
                    'message' => '<p>Такая деталь уже существует</p>',
                );

                echo json_encode($result);
                exit;
            }

            $post['price_from_provider'] = standard_price($post['price_from_provider']);

            if (empty($post['price'])) {
                $post['price'] = $this->search_detail_model->calculation_retail_price($post['price_from_provider']);
                $post['base_price'] = $post['price'];
            } else {
                $post['price'] = standard_price($post['price']);
                $post['base_price'] = $this->search_detail_model->calculation_retail_price($post['price_from_provider']);
            }

            if ($data_new_detail = $this->shopping_cart_model->save_custom_detail_to_cart($post, $id_manager)) {
                $shopping_cart = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);

                $total_sum_and_profit = $this->shopping_cart_model->calculate_total_sum_and_profit($shopping_cart);

                $result = array(
                    'status' => 'ok',
                    'row_new_detail' => $this->shopping_cart_model->create_row_new_detail($data_new_detail, $shopping_cart),
                    'total_sum' => $total_sum_and_profit['total_sum'],
                    'profit' => $total_sum_and_profit['profit'],
                );

                echo json_encode($result);
                exit;
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => '<p>Произошла неизвестная ошибка</p>',
                );

                echo json_encode($result);
                exit;
            }
        } else {
            $result = array(
                'status' => 'error_empty_input',
                'errors_array' => $errors_array
            );

            echo json_encode($result);
            exit;
        }
    }

    /**
     * Функция получает список складов для поставщика
     * @return json data
     */
    public function get_warehouse_for_provider() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $id_provider = $this->input->post('id_provider');

        if (empty($this->shopping_cart_model->get_provider_by_id($id_provider))) {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );

            echo json_encode($result);
            exit;
        }

        $list_warehouses = $this->shopping_cart_model->get_list_warehouses_for_provider($id_provider);

        $result = array(
            'status' => 'ok',
            'list_warehouses' => $list_warehouses
        );

        echo json_encode($result);
        exit;
    }

    /**
     * Функция создает заказ
     * @return json $result
     */
    public function create_order() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Пожалуйста авторизуйтесь'
            );

            echo json_encode($result);
            exit;
        }

        $this->load->model('amocrm_model');
        $id_deal_amo = $this->input->post('id_deal_amo');
        $delivery_date = $this->input->post('delivery_date');

        if (!empty($this->shopping_cart_model->get_order_widget_by_id_deal_amo($id_deal_amo))) {
            $result = array(
                'status' => 'error',
                'message' => 'Такой заказ уже существует'
            );

            echo json_encode($result);
            exit;
        }

        if (!empty($id_deal_amo)) {
            if ($this->amocrm_model->auth()) {
                $domain_amocrm = $this->config->item('domain_amocrm');

                $link = 'https://' . $domain_amocrm . '.amocrm.ru/api/v2/leads?query=' . $id_deal_amo;

                $list_leads = $this->amocrm_model->list_leads($link);

                if (!empty($list_leads)) {
                    $id_manager = $this->session->userdata['manager_data']['id_user'];
                    $data_order = $this->shopping_cart_model->get_order_cart_by_id_manager($id_manager);

                    if (empty($data_order)) {
                        $result = array(
                            'status' => 'error',
                            'message' => 'Такой заказ в корзине больше не существует'
                        );

                        echo json_encode($result);
                        exit;
                    }

                    $list_details = $this->shopping_cart_model->get_details_cart_by_id_order($data_order['id'], $id_manager);

                    if (empty($list_details)) {
                        $result = array(
                            'status' => 'error',
                            'message' => 'Такой заказ в корзине больше не существует'
                        );

                        echo json_encode($result);
                        exit;
                    }

                    if ($this->shopping_cart_model->save_order_in_widget($id_deal_amo, $id_manager, $data_order, $list_details, $delivery_date)) {
                        $lead = $this->shopping_cart_model->create_data_lead_for_update($id_deal_amo, $data_order, $list_details, $delivery_date);
                        $this->amocrm_model->set_leads($lead, 'update');

                        $note = $this->shopping_cart_model->create_data_note_amocrm_for_add($id_deal_amo, $data_order, $list_details, $delivery_date);
                        $this->amocrm_model->set_notes($note, 'add');

                        $result = array(
                            'status' => 'ok',
                            'message' => 'Заказ успешно создан'
                        );
                    } else {
                        $result = array(
                            'status' => 'error',
                            'message' => 'Произошла неизвестная ошибка'
                        );
                    }
                } else {
                    $result = array(
                        'status' => 'error',
                        'message' => 'Укажите правильный id сделки'
                    );
                }
            }
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Укажите правильный id сделки'
            );
        }

        echo json_encode($result);
        exit;
    }

}
