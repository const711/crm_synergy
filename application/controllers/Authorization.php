<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Authorization extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('authorization_model');
        $this->load->library('session');
        $this->load->model('shopping_cart_model');
    }

    /**
     * функция авторизации в программу для поиска деталей
     */
    public function login() {
        if ($this->authorization_model->check_auth()) {
            redirect('search_detail/index');
        }

        $data['title'] = 'Авторизация в программу';

        $data['other_js'] = array('js/script.js');

        if ($_POST) {
            $error_mass = '';

            $email_login = trim($this->input->post('email_login'));
            $password_login = trim($this->input->post('password_login'));

            if ($email_login == '') {
                $error_mass .= '<p>Заполните поле "Email"</p>';
            }

            if ($password_login == '') {
                $error_mass .= '<p>Заполните поле "Пароль"</p>';
            }

            if (mb_strlen($password_login) < 6 && $password_login != '') {
                $error_mass .= '<p>Длина поля "Пароль" не меньше 6 символов</p>';
            }

            if ($error_mass === '') {
                $user_data = $this->authorization_model->get_manager_by_email_pass($email_login, $password_login);

                if (empty($user_data)) {
                    $error_mass .= '<p>Введен неправильный логин или пароль!</p>';
                }
            }

            if ($error_mass === '') {
                $data_session = array(
                    'id_user' => $user_data['id'],
                    'name' => $user_data['name'],
                    'last_name' => $user_data['last_name'],
                    'email' => $user_data['email'],
                    'pass' => $user_data['pass'],
                    'type_user' => 'manager',
                );

                $this->session->set_userdata('manager_data', $data_session);

                $id_manager = $this->session->userdata['manager_data']['id_user'];
                $shopping_cart = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);
                $this->shopping_cart_model->check_orders_in_shopping_cart($shopping_cart);

                redirect('search_detail/index');
            } else {
                $data['error_mass'] = $error_mass;
                $data['email_login'] = $email_login;
                $data['password_login'] = $password_login;

                $this->load->view('templates/header_auth', $data);
                $this->load->view('authorization/login', $data);
                $this->load->view('templates/footer');
            }
        } else {
            $this->load->view('templates/header_auth', $data);
            $this->load->view('authorization/login', $data);
            $this->load->view('templates/footer');
        }
    }
    
    /**
     * функция выхода из программы для поиска деталей
     */
    public function logout() {
        $this->session->unset_userdata('manager_data');
        redirect('authorization/login');
    }
}
