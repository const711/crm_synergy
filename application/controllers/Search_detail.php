<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search_detail extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('work_helper');
        $this->load->model('search_detail_model');
        $this->load->model('authorization_model');
        $this->load->model('shopping_cart_model');
        $this->load->library('session');
    }

    /* Функция выводит страницу с формой для поиска деталей */

    public function index() {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        $data['title'] = 'Поиск по коду';

        $data['other_js'] = array('js/script.js');

        $id_manager = $this->session->userdata['manager_data']['id_user'];

        $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

        if ($_POST) {
            $this->load->helper('form');

            $article = mb_strtolower($this->input->post('article_detail'));

            $this->form_validation->set_rules('article_detail', 'Артикл', 'required');
            $this->form_validation->set_message('required', 'Заполните поле "{field}"');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('templates/header', $data);
                $this->load->view('search_detail/index');
                $this->load->view('templates/footer');
            } else {
                $article = leave_eng_letters_numbers($article);

                redirect('/search_detail/list_brands/' . $article);
            }
        } else {
            $this->load->view('templates/header', $data);
            $this->load->view('search_detail/search_detail_form');
            $this->load->view('templates/footer');
        }
    }

    /* Функция используется для поиска детали по артикулу из верхней формы */

    public function search_detail_top_form() {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        if ($_POST) {
            $article = mb_strtolower($this->input->post('article_detail'));

            $article = leave_eng_letters_numbers($article);

            redirect('/search_detail/list_brands/' . $article);
        } else {
            redirect('/search_detail/index');
        }
    }

    /**
     * Функция выводит страницу со списком найденных брендов
     * @param string $article
     * @param string $private_data
     */
    public function list_brands($article = '') {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        $data['title'] = 'Список найденных брендов';

        $data['other_js'] = array('js/script.js');

        $get_data = $this->input->get();
        $need_update = 0;
        $private_data = isset($get_data['private_data']) && !empty($get_data['private_data']) ? $get_data['private_data'] : 'show';

        $article = leave_eng_letters_numbers(mb_strtolower($article));

        $date_update_list_brands = $this->search_detail_model->get_date_update_list_brands_searchs($article);

        if (empty($date_update_list_brands)) {
            $need_update = 1;
        } else {
            $date_end_update_list_brands = strtotime("+1 day", strtotime($date_update_list_brands));
            $date_now = strtotime(date('Y-m-d'));

            if ($date_end_update_list_brands <= $date_now) {
                $need_update = 1;
            }
        }

        if ($need_update) {
            $this->search_detail_model->update_data_list_brands_searchs($article);
        }

        $data['article'] = $article;

        $data['private_data'] = $private_data;

        $data['list_brands'] = $this->search_detail_model->get_list_merge_brands($article);

        $data['search_detail_form'] = $this->load->view('search_detail/search_detail_form', $data, TRUE);

        $id_manager = $this->session->userdata['manager_data']['id_user'];

        $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

        $this->load->view('templates/header', $data);
        $this->load->view('search_detail/page_list_brands', $data);
        $this->load->view('templates/footer');
    }

    /* Функция обновления списка брендов, если нужно получить свежие бренды от поставщиков */

    public function update_list_brands() {
        if (!$this->authorization_model->check_auth()) {
            show_404();
        }

        $need_update = $this->input->post('need_update');
        $article_search = $this->input->post('article_search');
        $article_search = leave_eng_letters_numbers(mb_strtolower($article_search));

        if ($need_update) {
            if ($this->search_detail_model->update_data_list_brands_searchs($article_search)) {
                echo json_encode('ok');
                exit;
            } else {
                echo json_encode('error');
                exit;
            }
        }
    }

    /**
     * Функция выводит страницу со списком брендов для объединения (для страницы со списком брендов)
     * @param string $article
     * @param string $private_data
     */
    public function list_merge_brands($article = '', $private_data = 'show') {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        $data['title'] = 'Объединение брендов';

        $data['other_js'] = array('js/script.js');

        $data['article'] = leave_eng_letters_numbers(mb_strtolower($article));

        $data['private_data'] = $private_data;

        $data['list_brands'] = $this->search_detail_model->get_list_merge_brands($article);

        $data['search_detail_form'] = $this->load->view('search_detail/search_detail_form', $data, TRUE);

        $id_manager = $this->session->userdata['manager_data']['id_user'];

        $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

        $this->load->view('templates/header', $data);
        $this->load->view('search_detail/page_merge_brands_in_brands_list', $data);
        $this->load->view('templates/footer');
    }

    /* Функция производит объединение выбранных брендов (для страницы со списком брендов) */

    public function merge_brands_in_brand_list() {
        if (!$this->authorization_model->check_auth()) {
            show_404();
        }

        $name_group = mb_strtolower(strip_tags($this->input->post('name_group')));
        $merge_brands_array = $this->input->post('selected_merge_brands');
        $brand_for_description = strip_tags($this->input->post('brand_for_description'));
        $final_result = array();
        $description_brand = array();

        if (!isset($name_group) or $name_group == '' or $name_group == 'undefined') {
            $result = array(
                'status' => 'error',
                'message' => 'Выберите название для группы',
            );

            echo json_encode($result);
            exit;
        }

        if (count($merge_brands_array) < 2) {
            $result = array(
                'status' => 'error',
                'message' => 'Выберите минимум пару брендов!',
            );

            echo json_encode($result);
            exit;
        }

        foreach ($merge_brands_array as $key => $value) {
            $merge_brands_array[$key] = mb_strtolower(strip_tags($value));
        }

        $key_group = $this->search_detail_model->get_key_group_brands_by_group_name($name_group);

        $final_result = $this->search_detail_model->create_final_data_merge_brands($merge_brands_array);

        if (!empty($final_result)) {
            if (isset($brand_for_description) && $brand_for_description != '' && $brand_for_description != 'undefined') {
                $description_brand = $this->search_detail_model->get_description_brand_by_name_brand($brand_for_description);
            }

            if ($this->search_detail_model->save_merge_brands($merge_brands_array, $final_result, $name_group, $key_group, $description_brand)) {
                $result = array(
                    'status' => 'ok',
                    'message' => 'Бренды успешно объединены',
                );
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка',
                );
            }

            echo json_encode($result);
            exit;
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка',
            );

            echo json_encode($result);
            exit;
        }
    }

    /**
     * Функция для поиска детали по определенному артикулу и бренду (используется в случае щелчка мышью по артикулу на странице уже найденных деталей)
     * @param string $article
     * @param string $key_group
     */
    public function search_detail_by_article_key_group($article, $key_group) {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        $article = leave_eng_letters_numbers(mb_strtolower($article));

        if (!$brand = $this->search_detail_model->get_group_name_brands_by_key_group($key_group)) {
            redirect('/search_detail/index');
        }

        if (!$data_search = $this->search_detail_model->check_exist_article_search_brand_search($article, $brand)) {
            if ($this->search_detail_model->save_data_for_search_by_article_brand($article, $brand)) {
                redirect('/search_detail/list_details/' . $article . '/' . $key_group);
            } else {
                redirect('/search_detail/index');
            }
        } else {
            $key_group = $this->search_detail_model->get_key_group_brands_by_group_name($brand);

            redirect('/search_detail/list_details/' . $article . '/' . $key_group);
        }
    }

    /**
     * Функция выводит список найденных деталей
     * @param string $article
     * @param string $key_group
     * @param string $private_data
     * @param string $search_type
     * @param string $recom_brands
     * @param string $field_sort
     * @param string $type_sort
     */
    public function list_details($article = '', $key_group = '') {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        $article = leave_eng_letters_numbers(mb_strtolower($article));
        $key_group = strip_tags($key_group);

        $data['title'] = 'Список найденных деталей';

        $data['other_js'] = array(
            'js/jquery-ui.js',
            'js/script.js',
            'js/shopping_cart.js'
        );

        $data['other_css'] = array(
            'css/jquery-ui.css',
        );

        $data['article'] = $article;
        $data['key_group'] = $key_group;
        $get_data = $this->input->get();
        $need_update = 0;

        $private_data = isset($get_data['private_data']) && !empty($get_data['private_data']) ? $get_data['private_data'] : 'show';
        $type_sort = isset($get_data['type_sort']) && !empty($get_data['type_sort']) ? $get_data['type_sort'] : 1;
        $recom_brands = isset($get_data['recom_brands']) && !empty($get_data['recom_brands']) ? $get_data['recom_brands'] : 'all_brands';
        $filter_type_warehouse = isset($get_data['type_warehouse']) && !empty($get_data['type_warehouse']) ? $get_data['type_warehouse'] : 'three';

        $date_update_detail = $this->search_detail_model->get_date_update_detail($article, $key_group);

        $group_name = $this->search_detail_model->get_group_name_by_key_group($key_group);

        $data['group_name'] = $group_name;

        if (empty($date_update_detail)) {
            $need_update = 1;
        } else {
            $date_end_update_detail = strtotime("+1 day", strtotime($date_update_detail));
            $date_now = strtotime(date('Y-m-d'));

            if ($date_end_update_detail <= $date_now) {
                $need_update = 1;
            }
        }

        if ($need_update && !empty($group_name)) {
            $this->search_detail_model->update_data_search_detail($article, $group_name);
        }

        $data['pop_up_edit_brand'] = $this->load->view('search_detail/pop_up_edit_brand', array(), TRUE);

        $id_manager = $this->session->userdata['manager_data']['id_user'];

        $data['shopping_cart'] = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);

        $data_list_details = $this->search_detail_model->get_search_list_details($article, $group_name, $data['shopping_cart'], $type_sort, $filter_type_warehouse, $recom_brands, $get_data);

        $data['unique_brands'] = $this->search_detail_model->get_unique_brands_from_list_details($data_list_details['list_details']);
        $data['min_price_cross'] = $data_list_details['min_price_cross'];
        $data['max_price_detail'] = $data_list_details['max_price_detail'];
        $data['is_complete'] = $data_list_details['is_complete'];
        $data['list_details'] = $data_list_details['list_details'];
        $data['price_between_beg'] = isset($data_list_details['price_between_beg']) ? $data_list_details['price_between_beg'] : 0;
        $data['price_between_end'] = isset($data_list_details['price_between_end']) ? $data_list_details['price_between_end'] : 0;
        $data['providers_in_list_details'] = $data_list_details['providers_in_list_details'];
        $data['list_provider_with_api'] = $this->search_detail_model->get_list_provider_with_api();
        $data['private_data'] = $private_data;
        $data['recom_brands'] = $recom_brands;
        $data['filter_type_warehouse'] = $filter_type_warehouse;
        $data['type_sort'] = $type_sort;
        $data['intervals_deliv_period'] = $this->search_detail_model->get_intervals_deliv_period($data_list_details['list_details']);
        $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);
        $data['template_list_details'] = $this->load->view('search_detail/template_list_details', $data, TRUE);
        $data['template_list_details'] = $this->load->view('search_detail/template_list_details', $data, TRUE);

        $this->load->view('templates/header', $data);
        $this->load->view('search_detail/page_list_details', $data);
        $this->load->view('templates/footer');
    }

    /* Функция обновления списка деталей, если нужно получить свежие предложения по указанной детали */

    public function update_list_details($article = '', $key_group = '') {
        if (!$this->authorization_model->check_auth()) {
            show_404();
        }

        $article = leave_eng_letters_numbers(mb_strtolower($article));
        $key_group = strip_tags($key_group);

        $this->search_detail_model->del_inf_brands_search_by_article($article);

        $group_name = $this->search_detail_model->get_group_name_by_key_group($key_group);

        $id = $this->search_detail_model->get_id_by_article_and_brand($article, $group_name);

        if (!empty($id)) {
            $this->search_detail_model->del_cross_for_detail($id);
            $this->search_detail_model->del_detail_considering_cross($id);
        }

        redirect('/search_detail/list_brands/' . $article);
    }

    /**
     * Функция выводит страницу со списком брендов для объединения (для страницы найденных деталей)
     * @param string $article
     * @param string $key_group
     * @param string $private_data
     * @param string $field_sort
     * @param string $type_sort
     */
    public function list_merge_brands_for_list_details($article = '', $key_group = '') {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        $data['title'] = 'Объединение брендов';

        $data['other_js'] = array('js/script.js');
        $data['article'] = $article;
        $data['key_group'] = $key_group;
        $get_data = $this->input->get();
        $fields_sort_array = array(
            'article',
            'brand',
            'name',
        );

        $field_sort = (isset($get_data['field_sort']) && (in_array($get_data['field_sort'], $fields_sort_array))) ? $get_data['field_sort'] : '';
        $type_sort = (isset($get_data['type_sort']) && !empty($get_data['type_sort'])) ? $get_data['type_sort'] : 'asc';
        $private_data = (isset($get_data['private_data']) && !empty($get_data['private_data'])) ? $get_data['private_data'] : 'show';

        $data['list_brands'] = $this->search_detail_model->get_list_merge_brands_for_list_details($article, $key_group, $field_sort, $type_sort);

        if (!empty($data['list_brands'])) {
            $data['sort_links'] = $this->search_detail_model->create_sort_links($fields_sort_array, $field_sort, $type_sort);
        }

        $data['private_data'] = $private_data;

        $id_manager = $this->session->userdata['manager_data']['id_user'];

        $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

        $this->load->view('templates/header', $data);
        $this->load->view('search_detail/page_merge_brands_for_list_details', $data);
        $this->load->view('templates/footer');
    }

    /* Функция производит объединение выбранных брендов (для страницы найденных деталей) */

    public function merge_brands_for_list_details() {
        if (!$this->authorization_model->check_auth()) {
            show_404();
        }

        $name_group = mb_strtolower($this->input->post('name_group'));
        $selected_merge_brands = $this->input->post('selected_merge_brands');
        $key_group_search = strip_tags($this->input->post('key_group_search'));
        $brand_for_description = strip_tags($this->input->post('brand_for_description'));
        $final_result = array();
        $merge_brands_array = array();
        $description_brand = array();

        if (!isset($name_group) or $name_group == '' or $name_group == 'undefined') {
            $result = array(
                'status' => 'error',
                'message' => 'Выберите название для группы',
            );

            echo json_encode($result);
            exit;
        }

        if (count($selected_merge_brands) < 2) {
            $result = array(
                'status' => 'error',
                'message' => 'Выберите минимум пару брендов!',
            );

            echo json_encode($result);
            exit;
        }

        foreach ($selected_merge_brands as $key => $value) {
            $selected_merge_brands[$key] = mb_strtolower(strip_tags($value));
        }

        if (!$this->search_detail_model->check_all_articles_equal($selected_merge_brands)) {
            $result = array(
                'status' => 'error',
                'message' => 'В выбранном списке есть бренды с разными артикулами',
            );

            echo json_encode($result);
            exit;
        }

        foreach ($selected_merge_brands as $data) {
            $data = preg_replace('/\*\*(.+)\*\*/iu', '', $data);

            $merge_brands_array[] = trim($data);
        }

        $key_group_array = $this->search_detail_model->get_key_group_for_merge_brands($merge_brands_array);

        if (in_array($key_group_search, $key_group_array)) {
            $key_group = $key_group_search;
        } else {
            $key_group = $this->search_detail_model->get_key_group_brands_by_group_name($name_group);
        }

        $final_result = $this->search_detail_model->create_final_data_merge_brands($merge_brands_array);

        if (!empty($final_result)) {
            if (isset($brand_for_description) && $brand_for_description != '' && $brand_for_description != 'undefined') {
                $description_brand = $this->search_detail_model->get_description_brand_by_name_brand($brand_for_description);
            }

            if ($this->search_detail_model->save_merge_brands_for_list_detail($merge_brands_array, $final_result, $name_group, $key_group, $description_brand)) {
                $result = array(
                    'status' => 'ok',
                    'message' => 'Бренды успешно объединены',
                );
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка',
                );
            }

            echo json_encode($result);
            exit;
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка',
            );

            echo json_encode($result);
            exit;
        }
    }

    /**
     * Функция выводит страницу со списком выбранных деталей из списка найденных деталей
     * @param string $private_data
     */
    public function list_selected_offers($private_data = 'show') {
        if (!$this->authorization_model->check_auth()) {
            redirect('/authorization/login');
        }

        if ($_POST) {
            $data['title'] = 'Выбранные детали';

            $data['other_js'] = array('js/script.js');

            $this->load->library('user_agent');

            $selected_offers = $this->input->post('selected_offers');

            if (!empty($selected_offers)) {
                $data['list_selected_offers'] = $this->search_detail_model->get_details_by_offers($selected_offers);
            } else {
                $data['list_selected_offers'] = '';
            }

            $data['article'] = $this->input->post('article_search');
            $data['key_group'] = $this->input->post('key_group_search');
            $data['private_data'] = $private_data;

            $data['pop_up_edit_brand'] = $this->load->view('search_detail/pop_up_edit_brand', array(), TRUE);

            $id_manager = $this->session->userdata['manager_data']['id_user'];

            $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

            $this->load->view('templates/header', $data);
            $this->load->view('search_detail/page_list_selected_offers', $data);
            $this->load->view('templates/footer');
        } else {
            redirect('/search_detail/index');
        }
    }

    /* Функция позволяет редактировать данные бренда */

    public function edit_brand() {
        if (!$this->authorization_model->check_auth()) {
            show_404();
        }

        $id_brand = $this->input->post('id_form_edit_brand');
        $description_brand = $this->input->post('description_form_edit_brand');
        $link_brand = $this->input->post('link_form_edit_brand');
        $country_brand = $this->input->post('country_form_edit_brand');

        if (empty($description_brand) and empty($link_brand) and empty($country_brand)) {
            $data['status_ajax'] = 'error_empty_form';

            echo json_encode($data);
            exit;
        }

        if (!$this->search_detail_model->check_exist_brand_by_id($id_brand)) {
            $data['status_ajax'] = 'error';

            echo json_encode($data);
            exit;
        }

        $data_for_update = array(
            'description' => strip_tags($this->input->post('description_form_edit_brand')),
            'link' => strip_tags($this->input->post('link_form_edit_brand')),
            'country' => strip_tags($this->input->post('country_form_edit_brand')),
            'recommended' => (int) $this->input->post('recommended_form_edit_brand'),
            'rating' => (int) $this->input->post('rating_form_edit_brand'),
        );

        if ($this->search_detail_model->update_brand($id_brand, $data_for_update)) {
            $data['status_ajax'] = 'ok';

            echo json_encode($data);
            exit;
        } else {
            $data['status_ajax'] = 'error';

            echo json_encode($data);
            exit;
        }
    }

    /* Функция позволяет получить описание бренда (используется в окне объединения брендов) */

    public function get_description_brand() {
        if (!$this->authorization_model->check_auth()) {
            show_404();
        }

        $brand_for_description = $this->input->post('brand_for_description');

        if (!empty($data_brand = $this->search_detail_model->get_description_brand_by_name_brand($brand_for_description))) {
            if (empty($data_brand['description'])
                    and empty($data_brand['link']) and empty($data_brand['country'])) {
                $data['status_ajax'] = 'empty';
            } else {
                $data['status_ajax'] = 'ok';
                $data['data_brand'] = $data_brand;
            }
        } else {
            $data['status_ajax'] = 'empty';
        }

        echo json_encode($data);
        exit;
    }

    /**
     * Функция отвечает за сортировку предложений для детали в списке найденных деталей
     */
    public function sort_offers() {
        if (!$this->authorization_model->check_auth()) {
            show_404();
        }

        $id_detail = (int) $this->input->post('id_detail');
        $name_sort = $this->input->post('name_sort');
        $type_sort = $this->input->post('type_sort');
        $filter_warehouse = $this->input->post('filter_warehouse');
        $private_data = $this->input->post('private_data');
        $article_search = $this->input->post('article_search');
        $selected_offers = $this->input->post('selected_offers');
        $is_complete = $this->input->post('is_complete');

        if (empty($id_detail) or empty($name_sort) or empty($article_search)) {
            $data['status_ajax'] = 'error';

            echo json_encode($data);
            exit;
        }

        $list_offers = $this->search_detail_model->get_all_data_offers_for_detail($id_detail);

        if (!empty($list_offers)) {
            $list_offers = $this->search_detail_model->filter_and_sort_list_offers($list_offers, $filter_warehouse, $name_sort, $type_sort, $is_complete);

            $data['status_ajax'] = 'ok';

            $id_manager = $this->session->userdata['manager_data']['id_user'];

            $shopping_cart = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);

            $data['list_offers'] = $this->search_detail_model->create_final_list_offers_after_sort($list_offers, $selected_offers, $id_detail, $article_search, $private_data, $shopping_cart);
            echo json_encode($data);
            exit;
        } else {
            $data['status_ajax'] = 'error';

            echo json_encode($data);
            exit;
        }
    }

    public function background_search_detail() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        $article = $this->input->post('article');
        $key_group = $this->input->post('key_group');

        $article = leave_eng_letters_numbers(mb_strtolower($article));
        $key_group = strip_tags($key_group);

        $brand = $this->search_detail_model->get_group_name_by_key_group($key_group);

        $id_detail = $this->search_detail_model->get_id_by_article_and_brand($article, $brand);

        $search_is_complete = $this->search_detail_model->check_search_detail_is_complete($id_detail);

        if (!empty($search_is_complete) && ($search_is_complete['is_complete'] == '0')) {
            if ($this->search_detail_model->background_update_data_search_detail($article, $brand)) {
                $result = array(
                    'status' => 'ok',
                    'message' => '',
                );
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Во время поиска произошла ошибка',
                );
            }
        } else {
            $result = array(
                'status' => 'ok',
                'message' => '',
            );
        }

        echo json_encode($result);
        exit;
    }

    public function filter_for_list_details() {
        if (!$this->input->is_ajax_request()) {
            redirect('/search_detail/index');
        }

        if (!$this->authorization_model->check_auth()) {
            $result = array(
                'status' => 'error',
                'message' => 'Авторизуйтесь пожалуйста'
            );

            echo json_encode($result);
            exit;
        }

        $post_data = $this->input->post();

        $article = isset($post_data['article']) ? leave_eng_letters_numbers(mb_strtolower($post_data['article'])) : '';
        $key_group = isset($post_data['key_group']) ? strip_tags($post_data['key_group']) : '';
        $private_data = isset($post_data['private_data']) && !empty($post_data['private_data']) ? $post_data['private_data'] : 'show';
        $type_sort = isset($post_data['type_sort']) && !empty($post_data['type_sort']) ? $post_data['type_sort'] : 1;
        $recom_brands = isset($post_data['recom_brands']) && !empty($post_data['recom_brands']) ? $post_data['recom_brands'] : 'all_brands';
        $filter_type_warehouse = isset($post_data['type_warehouse']) && !empty($post_data['type_warehouse']) ? $post_data['type_warehouse'] : 'default';

        $group_name = $this->search_detail_model->get_group_name_by_key_group($key_group);

        $data['group_name'] = $group_name;

        $id_manager = $this->session->userdata['manager_data']['id_user'];
        $data['shopping_cart'] = $this->shopping_cart_model->get_shopping_cart_by_id_manager($id_manager);
        $data_list_details = $this->search_detail_model->get_search_list_details($article, $group_name, $data['shopping_cart'], $type_sort, $filter_type_warehouse, $recom_brands, $post_data);

        $data['private_data'] = $private_data;
        $data['type_sort'] = $type_sort;
        $data['article'] = $article;
        $data['key_group'] = $key_group;
        $data['list_details'] = $data_list_details['list_details'];
        $data['unique_brands'] = $this->search_detail_model->get_unique_brands_from_list_details($data_list_details['list_details']);
        $data['count_rows_details_shopping_cart'] = $this->shopping_cart_model->count_rows_details_shopping_cart_for_manager($id_manager);

        $result = array(
            'status' => 'ok',
            'template_list_details' => $this->load->view('search_detail/template_list_details', $data, TRUE),
            'unique_brands' => '',
        );

        echo json_encode($result);
        exit;
    }

}
