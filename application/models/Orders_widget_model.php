<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Orders_widget_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('search_detail_model');
        $this->db2 = $this->load->database('db_amo_widget', TRUE);
    }

    /**
     * Функция получает список заказов
     * @return array
     */
    public function get_list_orders($num = 0, $offset = 0) {
        $this->db2->select('*');
        $this->db2->from('amoCrmProductsWidgetLeadsDiscount');
        $this->db2->order_by('id', 'desc');

        if ($num != 0) {
            $this->db2->limit($num, $offset);
        }

        $query = $this->db2->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    public function get_order_by_id($id_order) {
        $this->db2->select('*');
        $this->db2->from('amoCrmProductsWidgetLeadsDiscount');
        $this->db2->where('id', $id_order);

        $query = $this->db2->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    public function get_order_by_id_amocrm($id_amocrm) {
        $this->db2->select('*');
        $this->db2->from('amoCrmProductsWidgetLeadsDiscount');
        $this->db2->where('amoCrmEntityId', $id_amocrm);

        $query = $this->db2->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    public function update_order($id_order, $field, $data) {
        $this->db2->trans_start();

        $data_array = array(
            $field => $data
        );

        $this->db2->where('id', $id_order);
        $query = $this->db2->update('amoCrmProductsWidgetLeadsDiscount', $data_array);

        if ($this->del_change_order_by_field($id_order, $field)) {
            $data_array = array(
                'field' => $field,
                'is_changed' => 1,
                'id_order' => $id_order,
            );

            $this->save_new_change_order($data_array);
        }

        if ($this->db2->trans_status() === FALSE) {
            $this->db2->trans_rollback();

            return FALSE;
        } else {
            $this->db2->trans_commit();

            return TRUE;
        }
    }

    public function get_all_changes_order($id_order) {
        $this->db2->select('*');
        $this->db2->from('changes_order');
        $this->db2->where('id_order', $id_order);

        $query = $this->db2->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    public function get_all_changes_detail($id_detail) {
        $this->db2->select('*');
        $this->db2->from('changes_detail');
        $this->db2->where('id_detail', $id_detail);

        $query = $this->db2->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    public function save_new_change_order($data) {
        $this->db2->insert('changes_order', $data);

        if ($this->db2->insert_id()) {
            return $this->db2->insert_id();
        } else {
            return FALSE;
        }
    }

    public function del_change_order_by_field($id_order, $field) {
        $this->db2->where('id_order', $id_order);
        $this->db2->where('field', $field);
        if ($this->db2->delete('changes_order')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function del_change_detail_by_field($id_detail, $field) {
        $this->db2->where('id_detail', $id_detail);
        $this->db2->where('field', $field);
        if ($this->db2->delete('changes_detail')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function reset_change_order($id_order, $type) {
        if ($type == 'discount') {
            $field = 'discountValue';
        } elseif ($type == 'percent') {
            $field = 'percentValue';
        } elseif ($type == 'delivery') {
            $field = 'deliveryDate';
        } else {
            return FALSE;
        }

        $this->db2->where('id_order', $id_order);
        $this->db2->where('field', $field);
        $query = $this->db2->update('changes_order', array('is_changed' => 0));

        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function reset_change_detail($id_detail, $type) {
        if ($type == 'vendor_code') {
            $field = 'vendorCode';
        } elseif ($type == 'brand') {
            $field = 'brand';
        } elseif ($type == 'name') {
            $field = 'name';
        } elseif ($type == 'provider') {
            $field = 'provider';
        } elseif ($type == 'request_price') {
            $field = 'requestPrice';
        } elseif ($type == 'retail_price') {
            $field = 'retailPrice';
        } elseif ($type == 'quantity') {
            $field = 'quantity';
        } else {
            return FALSE;
        }

        $this->db2->where('id_detail', $id_detail);
        $this->db2->where('field', $field);
        $query = $this->db2->update('changes_detail', array('is_changed' => 0));

        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function delete_order_by_id_amo($id_amocrm) {
        $this->db2->trans_start();

        $this->db2->where('amoCrmEntityId', $id_amocrm);
        if ($this->db2->delete('amoCrmProductsWidgetLeadsDiscount')) {
            $this->delete_detail_by_id_amocrm($id_amocrm);
        }

        if ($this->db2->trans_status() === FALSE) {
            $this->db2->trans_rollback();

            return FALSE;
        } else {
            $this->db2->trans_commit();

            return TRUE;
        }
    }

    public function delete_detail_by_id_amocrm($id_amocrm) {
        $this->db2->where('amoCrmEntityId', $id_amocrm);
        if ($this->db2->delete('amoCrmProductsWidgetRows')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Функция производит поиск по заказам
     * @param string $search_client
     * @return array
     */
    public function search_order($search_order, $num = 0, $offset = 0) {
        $this->db2->select('*');
        $this->db2->from('amoCrmProductsWidgetLeadsDiscount');
        $this->db2->where('amoCrmEntityId', $search_order);
        /* $this->db2->or_where('discountValue', $search_order);
          $this->db2->or_where('deliveryDate', $search_order); */
        $this->db2->order_by('id', 'desc');

        if ($num != 0) {
            $this->db2->limit($num, $offset);
        }

        $query = $this->db2->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    public function get_list_details_in_order($id_amocrm, $num = 0, $offset = 0) {
        $this->db2->select('*');
        $this->db2->from('amoCrmProductsWidgetRows');
        $this->db2->where('amoCrmEntityId', $id_amocrm);
        $this->db2->order_by('id', 'desc');

        if ($num != 0) {
            $this->db2->limit($num, $offset);
        }

        $query = $this->db2->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    public function get_detail_by_id($id_detail) {
        $this->db2->select('*');
        $this->db2->from('amoCrmProductsWidgetRows');
        $this->db2->where('id', $id_detail);

        $query = $this->db2->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    public function delete_detail_by_id($id_detail) {
        $this->db2->where('id', $id_detail);
        if ($this->db2->delete('amoCrmProductsWidgetRows')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function update_detail($id_detail, $field, $data) {
        $this->db2->trans_start();

        if ($field == 'requestPrice') {
            $data_array = array(
                $field => $data,
                'baseRetailPrice' => $this->search_detail_model->calculation_retail_price($data),
            );
        } else {
            $data_array = array(
                $field => $data
            );
        }

        $this->db2->where('id', $id_detail);
        $query = $this->db2->update('amoCrmProductsWidgetRows', $data_array);

        if ($this->del_change_detail_by_field($id_detail, $field)) {
            $data_array = array(
                'field' => $field,
                'is_changed' => 1,
                'id_detail' => $id_detail,
            );

            $this->save_new_change_detail($data_array);
        }

        if ($this->db2->trans_status() === FALSE) {
            $this->db2->trans_rollback();

            return FALSE;
        } else {
            $this->db2->trans_commit();

            return TRUE;
        }
    }

    public function save_new_change_detail($data) {
        $this->db2->insert('changes_detail', $data);

        if ($this->db2->insert_id()) {
            return $this->db2->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Функция производит поиск детали в заказе
     * @param string $search_client
     * @return array
     */
    public function search_detail($id_amocrm, $search_detail, $num = 0, $offset = 0) {
        $this->db2->select('*');
        $this->db2->from('amoCrmProductsWidgetRows');
        $this->db2->where('vendorCode', $search_detail);
        $this->db2->where('amoCrmEntityId', $id_amocrm);
        $this->db2->order_by('id', 'desc');

        if ($num != 0) {
            $this->db2->limit($num, $offset);
        }

        $query = $this->db2->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция создает ссылку поставщика для конкретной детали в заказе
     * @param string $article
     * @param string $link
     * @return string $result
     */
    public function create_link_for_offer($article, $link) {
        if (!empty($link)) {

            $link = str_replace(' ', '', $link);

            $link = str_replace('{article}', $article, $link);

            $result = 'http://' . $link;
        } else {
            $result = '';
        }

        return $result;
    }

    public function calculate_total_sum_and_profit_order($data_order) {
        $id_amocrm = $data_order['amoCrmEntityId'];
        $details_in_order = $this->get_list_details_in_order($id_amocrm);

        if (!empty($details_in_order)) {
            $total_sum = 0;
            $base_total_sum = 0;
            $profit = 0;
            $discount = $data_order['discountValue'];

            foreach ($details_in_order as $detail) {
                $total_sum = $total_sum + ($detail['retailPrice'] * $detail['quantity']);
                $base_total_sum = $base_total_sum + ($detail['baseRetailPrice'] * $detail['quantity']);
            }

            if ($data_order['percentValue'] > 0) {
                $total_sum = $total_sum + ($total_sum * $data_order['percentValue'] / 100);
            } elseif ($data_order['percentValue'] < 0) {
                $discount = $discount + ($total_sum * ($data_order['percentValue'] * -1) / 100);
            }

            $total_sum = $total_sum - $discount;

            if ($base_total_sum > 3000) {
                if ($discount > 300) {
                    $profit = ($total_sum + 300) - $base_total_sum;
                } else {
                    $profit = 0;
                }
            } else {
                $profit = $total_sum - $base_total_sum;
            }
        } else {
            $total_sum = 0;
            $profit = 0;
        }

        return array(
            'total_sum' => number_format($total_sum, 2, '.', ''),
            'profit' => number_format($profit, 2, '.', ''),
        );
    }

    public function calculate_retail_price($data_detail) {
        $id_detail = $data_detail['id'];
        $new_retail_price = $this->search_detail_model->calculation_retail_price($data_detail['requestPrice']);

        $this->db2->trans_start();

        $data_array = array(
            'retailPrice' => $new_retail_price,
            'baseRequestPrice' => $data_detail['requestPrice'],
            'baseRetailPrice' => $new_retail_price,
        );

        $this->db2->where('id', $id_detail);
        $query = $this->db2->update('amoCrmProductsWidgetRows', $data_array);

        if ($this->del_change_detail_by_field($id_detail, 'retailPrice')) {
            $data_array = array(
                'field' => 'retailPrice',
                'is_changed' => 1,
                'id_detail' => $id_detail,
            );

            $this->save_new_change_detail($data_array);
        }

        if ($this->db2->trans_status() === FALSE) {
            $this->db2->trans_rollback();

            return FALSE;
        } else {
            $this->db2->trans_commit();

            return $new_retail_price;
        }
    }

}
