<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_detail_model extends CI_Model {

    /**
     * Функция делает выборку деталей по артикулу и бренду из всех данных, которые пришли от micado по указанному артикулу
     * @param array $data_api_micado
     * @param string $article_get
     * @param string $brand_get
     * @return array $result
     */
    public function get_slice_brand_from_data_api_micado($data_api_micado, $article_get, $brand_get) {
        $result = array();
        $data_provider = $this->get_provider_by_name('Микадо');
        $rating_provider = isset($data_provider['rating']) ? $data_provider['rating'] : 0;

        $ids_types_array = $this->get_ids_for_warehouses_types_provider('Микадо');

        if (array_key_exists(1, $ids_types_array)) {
            $id_for_type_1 = $ids_types_array[1];
        } else {
            $id_for_type_1 = NULL;
        }

        if (array_key_exists(3, $ids_types_array)) {
            $id_for_type_3 = $ids_types_array[3];
        } else {
            $id_for_type_3 = NULL;
        }

        foreach ($data_api_micado as $micado_value) {
            if (isset($micado_value['Source']['SourceProducer']) && !empty($micado_value['Source']['SourceProducer'])) {
                if (is_array($micado_value['ProducerCode']) or is_array($micado_value['ProducerBrand'])) {
                    continue;
                }

                $source_code = leave_eng_letters_numbers(mb_strtolower($micado_value['Source']['SourceCode']));
                $source_producer = mb_strtolower($micado_value['Source']['SourceProducer']);
                $price = standard_price($micado_value['PriceRUR']);
                $key_exist_detail = '';

                if (($article_get == $source_code) and ( $brand_get == $source_producer)) {
                    $data = array(
                        'article' => leave_eng_letters_numbers(mb_strtolower($micado_value['ProducerCode'])),
                        'name' => (isset($micado_value['Name']) and ( !empty($micado_value['Name'])) and ! is_array($micado_value['Name'])) ? $micado_value['Name'] : '',
                        'offers' => array(),
                    );

                    if (($data['article'] == $article_get)
                            and ( mb_strtolower($micado_value['ProducerBrand']) == $brand_get)) {
                        $data['analog'] = 0;
                        $data['brand'] = mb_strtolower($micado_value['Source']['SourceProducer']);
                        $data['rating'] = 'original';
                    } else {
                        $data['analog'] = 1;
                        $data['brand'] = mb_strtolower($micado_value['ProducerBrand']);
                        $data['rating'] = array('micado' => $rating_provider);
                    }

                    if (empty($micado_value['OnStocks'])) {
                        if (isset($micado_value['Srock'])) {
                            $delivery_period_old = (int) preg_replace("/[^0-9]/iu", "", $micado_value['Srock']);

                            if ($delivery_period_old != 0) {
                                $delivery_period_new = $delivery_period_old + 2;
                                $delivery_period_new .= ' дн.';
                            } else {
                                $delivery_period_new = '-';
                            }
                        } else {
                            $delivery_period_new = '-';
                        }

                        $offer = array(
                            'price' => $price,
                            'description' => (isset($micado_value['Name']) and ( !empty($micado_value['Name'])) and ! is_array($micado_value['Name'])) ? $micado_value['Name'] : '',
                            'warehouse' => '',
                            'warehouse_type' => 3,
                            'quantity' => 0,
                            'min_part' => 1,
                            'delivery_period' => $delivery_period_new,
                            'id_warehouse_type' => $id_for_type_3,
                        );

                        $data['offers'][] = $offer;
                    } else {
                        $diff = count($micado_value['OnStocks']['StockLine']) - count($micado_value['OnStocks']['StockLine'], COUNT_RECURSIVE);

                        if ($diff == 0) {
                            $stock_line = $micado_value['OnStocks']['StockLine'];
                            $micado_value['OnStocks']['StockLine'] = array();
                            $micado_value['OnStocks']['StockLine'][] = $stock_line;
                        }

                        foreach ($micado_value['OnStocks']['StockLine'] as $value) {
                            $warehouse_type = 1;
                            $id_warehouse_type = $id_for_type_1;

                            if (($value['StokName'] == 'Симферополь') or ( $value['StokName'] == 'Краснодар')) {
                                $delivery_period = '1 дн.';
                            } else {
                                $delivery_period = '2 дн.';
                            }

                            $offer = array(
                                'price' => $price,
                                'description' => (isset($micado_value['Name']) and ( !empty($micado_value['Name'])) and ! is_array($micado_value['Name'])) ? $micado_value['Name'] : '',
                                'warehouse' => $value['StokName'],
                                'warehouse_type' => $warehouse_type,
                                'quantity' => $value['StockQTY'],
                                'min_part' => 1,
                                'delivery_period' => $delivery_period,
                                'id_warehouse_type' => $id_warehouse_type,
                            );

                            $data['offers'][] = $offer;
                        }
                    }

                    if (!empty($result)) {
                        foreach ($result as $res_key => $res_value) {
                            if (($res_value['article'] == $data['article'])
                                    and ( $res_value['brand'] == $data['brand'])) {
                                $key_exist_detail = $res_key;
                            }
                        }
                    }

                    if ($key_exist_detail === '') {
                        array_push($result, $data);
                    } else {
                        if (isset($data['name']) && mb_strlen($result[$key_exist_detail]['name']) < mb_strlen($data['name'])) {
                            $result[$key_exist_detail]['name'] = $data['name'];
                        }

                        foreach ($data['offers'] as $offer) {
                            if (!in_array($offer, $result[$key_exist_detail]['offers'])) {
                                $result[$key_exist_detail]['offers'][] = $offer;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция получает все бренды от api по указанному артикулу
     * @param string $article
     * @return array $result
     */
    public function get_api_brands_by_article($article) {
        $result = array();

        $brands_from_curl = $this->get_api_brands_from_curl($article);

        if (!empty($brands_from_curl)) {
            foreach ($brands_from_curl as $key => $brands) {
                foreach ($brands as $brand) {
                    $result[] = array(
                        'brand' => $brand,
                        'provider' => $key,
                    );
                }
            }
        }

        $brands_avtoto = $this->get_brands_avtoto($article);

        if (!empty($brands_avtoto)) {
            foreach ($brands_avtoto as $brand) {
                $result[] = array(
                    'brand' => $brand,
                    'provider' => 'avtoto',
                );
            }
        }

        return $result;
    }

    /**
     * Функция получает все бренды от тех api, которые работают по curl
     * @param string $article
     * @return array $result
     */
    public function get_api_brands_from_curl($article) {
        $mh = curl_multi_init();
        $result = array();

        /* настройки Berg */
        $url_berg = 'https://api.berg.ru/ordering/get_stock.json?items[0][resource_article]=' . $article . '&key=' . $this->config->item('api_key_berg');

        /* настройки Tiss */
        $fields = array("JSONparameter" => "{'Article': '" . $article . "'}");
        $headers = array(
            'Authorization: Bearer ' . $this->config->item('api_key_tiss')
        );

        /* настройки Micado */
        $login_micado = $this->config->item('login_micado');
        $pass_micado = $this->config->item('pas_micado');
        $url_micado = 'http://www.mikado-parts.ru/ws/service.asmx/Code_Search?Search_Code=' . $article . '&ClientID=' . $login_micado . '&Password=' . $pass_micado;

        /* настройки Adeo Pro */
        $login_adeo_pro = $this->config->item('login_adeo_pro');
        $pass_adeo_pro = $this->config->item('pas_adeo_pro');
        $url_adeo_pro = "http://adeo.pro/pricedetals2.php";

        $xml = '<?xml version="1.0" encoding="UTF-8" ?>
         <message>
           <param>
             <action>price</action>
             <login>' . $login_adeo_pro . '</login>
             <password>' . $pass_adeo_pro . '</password>
             <code>' . $article . '</code>
          </param>
        </message>';

        $data = array('xml' => $xml);

        /* настройки ABS */
        $key_abs = $this->get_key_abs();
        $user_context_abs = $this->get_user_context_abs($key_abs);

        if (isset($user_context_abs['user_agreements'][0]['ua_id'])) {
            $agreement_id = $user_context_abs['user_agreements'][0]['ua_id'];
        } else {
            $agreement_id = '';
        }

        $url_abs = 'http://api.abs-auto.ru/api-search?auth=' . $key_abs . '&article=' . $article . '&with_cross=0&agreement_id=' . $agreement_id . '&format=json';

        /* настройки Армтек */
        $login_armtek = $this->config->item('login_armtek');
        $pas_armtek = $this->config->item('pas_armtek');

        $user_vkorg_list = $this->get_user_vkorg_list_armtek($login_armtek, $pas_armtek);

        if (!empty($user_vkorg_list)) {
            $chs_armtek = array();

            foreach ($user_vkorg_list as $vkorg) {
                $data_armtek = array('VKORG' => $vkorg['VKORG'],
                    'PIN' => $article,
                );

                $ch_armtek = curl_init();

                $chs_armtek[] = $ch_armtek;

                curl_setopt($ch_armtek, CURLOPT_URL, "http://ws.armtek.ru/api/ws_search/assortment_search?format=json");
                curl_setopt($ch_armtek, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_armtek, CURLOPT_POST, true);
                curl_setopt($ch_armtek, CURLOPT_POSTFIELDS, http_build_query($data_armtek));
                curl_setopt($ch_armtek, CURLOPT_USERPWD, $login_armtek . ':' . $pas_armtek);
                curl_setopt($ch_armtek, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch_armtek, CURLOPT_TIMEOUT, 25);
                curl_multi_add_handle($mh, $ch_armtek);
            }
        }

        $ch_berg = curl_init();
        $ch_tiss = curl_init();
        $ch_micado = curl_init();
        $ch_adeo_pro = curl_init();
        $ch_abs = curl_init();

        curl_setopt($ch_berg, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch_berg, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_berg, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch_berg, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch_berg, CURLOPT_URL, $url_berg);

        curl_setopt($ch_tiss, CURLOPT_URL, "api.tmparts.ru/api/ArticleBrandList?" . http_build_query($fields));
        curl_setopt($ch_tiss, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_tiss, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch_tiss, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch_tiss, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch_micado, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch_micado, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_micado, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch_micado, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch_micado, CURLOPT_URL, $url_micado);

        curl_setopt($ch_adeo_pro, CURLOPT_HEADER, 0);
        curl_setopt($ch_adeo_pro, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_adeo_pro, CURLOPT_POST, 1);
        curl_setopt($ch_adeo_pro, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch_adeo_pro, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch_adeo_pro, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch_adeo_pro, CURLOPT_URL, $url_adeo_pro);

        curl_setopt($ch_abs, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch_abs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_abs, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch_abs, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch_abs, CURLOPT_URL, $url_abs);

        curl_multi_add_handle($mh, $ch_berg);
        curl_multi_add_handle($mh, $ch_tiss);
        curl_multi_add_handle($mh, $ch_micado);
        curl_multi_add_handle($mh, $ch_adeo_pro);
        curl_multi_add_handle($mh, $ch_abs);

        $running = null;

        do {
            curl_multi_exec($mh, $running);
        } while ($running > 0);

        $data_berg = curl_multi_getcontent($ch_berg);
        $brands_berg = ($data_berg !== false) ? $this->standard_view_brands_berg($data_berg) : array();

        $data_tiss = curl_multi_getcontent($ch_tiss);
        $brands_tiss = ($data_tiss !== false) ? $this->standard_view_brands_tiss($data_tiss) : array();

        $data_micado = curl_multi_getcontent($ch_micado);
        $brands_micado = ($data_micado !== false) ? $this->standard_view_brands_micado($data_micado) : array();

        $data_adeo_pro = curl_multi_getcontent($ch_adeo_pro);
        $brands_adeo_pro = ($data_adeo_pro !== false) ? $this->standard_view_brands_adeo_pro($data_adeo_pro) : array();

        $data_abs = curl_multi_getcontent($ch_abs);
        $brands_abs = ($data_abs !== false) ? $this->standard_view_brands_abs($data_abs) : array();

        curl_multi_remove_handle($mh, $ch_berg);
        curl_close($ch_berg);
        curl_multi_remove_handle($mh, $ch_tiss);
        curl_close($ch_tiss);
        curl_multi_remove_handle($mh, $ch_micado);
        curl_close($ch_micado);
        curl_multi_remove_handle($mh, $ch_adeo_pro);
        curl_close($ch_adeo_pro);
        curl_multi_remove_handle($mh, $ch_abs);
        curl_close($ch_abs);

        if (isset($chs_armtek) && !empty($chs_armtek)) {
            $brands_armtek = array();

            foreach ($chs_armtek as $ch_armtek) {
                $data_armtek = curl_multi_getcontent($ch_armtek);

                if ($data_armtek !== false) {
                    $brands = $this->standard_view_brands_armtek($data_armtek);

                    if (!empty($brands)) {
                        foreach ($brands as $value) {
                            if (!in_array($value, $brands_armtek)) {
                                $brands_armtek[] = $value;
                            }
                        }
                    }
                }

                curl_multi_remove_handle($mh, $ch_armtek);
                curl_close($ch_armtek);
            }
        }

        curl_multi_close($mh);

        if (!empty($brands_berg)) {
            $result['berg'] = $brands_berg;
        }

        if (!empty($brands_tiss)) {
            $result['tiss'] = $brands_tiss;
        }

        if (!empty($brands_micado)) {
            $result['micado'] = $brands_micado;
        }

        if (!empty($brands_adeo_pro)) {
            $result['adeo_pro'] = $brands_adeo_pro;
        }

        if (!empty($brands_abs)) {
            $result['abs'] = $brands_abs;
        }

        if (!empty($brands_armtek)) {
            $result['armtek'] = $brands_armtek;
        }

        return $result;
    }

    /**
     * Функция получает список сбытовых организаций пользователя от армтек
     * @param string $login_armtek
     * @param string $pas_armtek
     * @return array
     */
    public function get_user_vkorg_list_armtek($login_armtek, $pas_armtek) {
        $address = "http://ws.armtek.ru/api/ws_user/getUserVkorgList?format=json";

        $curl = curl_init($address);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERPWD, $login_armtek . ':' . $pas_armtek);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_TIMEOUT, 25);

        $data = curl_exec($curl);

        curl_close($curl);

        $data = json_decode($data, true);

        if (!empty($data) and isset($data['RESP']) and ! empty($data['RESP'])
                and ! isset($data['RESP']['MSG']) and ! isset($data['RESP']['ERROR'])) {
            $resp = $data['RESP'];

            $diff = count($resp) - count($resp, COUNT_RECURSIVE);

            if ($diff == 0) {
                $data = $resp;
                $resp = array();
                $resp[] = $data;
            }

            foreach ($resp as $key => $value) {
                if (!isset($value['VKORG'])) {
                    unset($resp[$key]);
                }
            }

            return $resp;
        } else {
            return array();
        }
    }

    public function get_user_info_armtek($login_armtek, $pas_armtek, $vkorg_armtek) {
        $address = "http://ws.armtek.ru/api/ws_user/getUserInfo?format=json";

        $data = array('VKORG' => $vkorg_armtek);

        $data = http_build_query($data);

        $curl = curl_init($address);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_USERPWD, $login_armtek . ':' . $pas_armtek);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_TIMEOUT, 25);

        $data = curl_exec($curl);

        curl_close($curl);

        $data = json_decode($data, true);

        if (!empty($data) and isset($data['RESP']) and ! empty($data['RESP'])
                and ! isset($data['RESP']['MSG']) and ! isset($data['RESP']['ERROR'])) {
            return $data['RESP'];
        } else {
            return array();
        }
    }

    /**
     * Функция преобразует данные с брандами от berg в стандартный вид для данной программы
     * @param array $data_berg
     * @return array $result
     */
    public function standard_view_brands_berg($data_berg) {
        $result = array();
        $data_berg = json_decode($data_berg, true);

        if (isset($data_berg['resources']) && !empty($data_berg['resources'])) {
            foreach ($data_berg['resources'] as $data) {
                if (isset($data['brand']['name']) and ! empty($data['brand']['name'])) {
                    $brand = mb_strtolower($data['brand']['name']);

                    if (!in_array($brand, $result)) {
                        $result[] = $brand;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция преобразует данные с брандами от tiss в стандартный вид для данной программы
     * @param array $data_tiss
     * @return array $result
     */
    public function standard_view_brands_tiss($data_tiss) {
        $result = array();
        $data_tiss = json_decode($data_tiss, true);

        if (empty($data_tiss)) {
            return $result;
        }

        if (!array_key_exists('Message', $data_tiss) && (!empty($data_tiss['BrandList']))) {
            foreach ($data_tiss['BrandList'] as $data) {
                if (isset($data['BrandName']) and ! empty($data['BrandName'])) {
                    $brand = mb_strtolower($data['BrandName']);

                    if (!in_array($brand, $result)) {
                        $result[] = $brand;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция преобразует данные с брандами от micado в стандартный вид для данной программы
     * @param array $data_micado
     * @return array $result
     */
    public function standard_view_brands_micado($data_micado) {
        $result = array();
        libxml_use_internal_errors(true);
        $micado_xml = simplexml_load_string($data_micado);

        if (!$micado_xml) {
            libxml_clear_errors();

            return $result;
        }

        $micado_json = json_encode($micado_xml);
        $brands_micado = json_decode($micado_json, TRUE);

        if (isset($brands_micado['List']['Code_List_Row']['Source'])) {
            $data = $brands_micado['List']['Code_List_Row'];
            $brands_micado['List']['Code_List_Row'] = array();
            $brands_micado['List']['Code_List_Row'][] = $data;
        }

        if (!empty($brands_micado) and isset($brands_micado['List']['Code_List_Row'])
                and ! empty($brands_micado['List']['Code_List_Row'])) {
            foreach ($brands_micado['List']['Code_List_Row'] as $brand) {
                if (isset($brand['Source']['SourceProducer']) and ! empty($brand['Source']['SourceProducer'])) {
                    $brand = mb_strtolower($brand['Source']['SourceProducer']);

                    if (!in_array($brand, $result)) {
                        $result[] = $brand;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция преобразует данные с брандами от adeo_pro в стандартный вид для данной программы
     * @param array $data_adeo_pro
     * @return array $result
     */
    public function standard_view_brands_adeo_pro($data_adeo_pro) {
        $result = array();
        libxml_use_internal_errors(true);
        $adeo_pro_xml = simplexml_load_string($data_adeo_pro);

        if (!$adeo_pro_xml) {
            libxml_clear_errors();

            return $result;
        }

        $adeo_pro_json = json_encode($adeo_pro_xml);
        $brands_adeo_pro = json_decode($adeo_pro_json, TRUE);

        if (!empty($brands_adeo_pro) and isset($brands_adeo_pro['detail'])
                and ( $brands_adeo_pro['detail'] != 'Клиент заблокирован')) {
            $diff = count($brands_adeo_pro['detail']) - count($brands_adeo_pro['detail'], COUNT_RECURSIVE);

            if ($diff == 0) {
                $detail = $brands_adeo_pro['detail'];
                $brands_adeo_pro['detail'] = array();
                $brands_adeo_pro['detail'][] = $detail;
            }

            foreach ($brands_adeo_pro['detail'] as $value) {
                if (isset($value['producer']) and ! empty($value['producer'])) {
                    $brand = mb_strtolower($value['producer']);

                    if (!in_array($brand, $result)) {
                        $result[] = $brand;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция преобразует данные с брандами от abs в стандартный вид для данной программы
     * @param array $data_abs
     * @return array $result
     */
    public function standard_view_brands_abs($data_abs) {
        $result = array();
        $data_abs = json_decode($data_abs, true);

        if (isset($data_abs['data']) and ! empty($data_abs['data'])) {
            foreach ($data_abs['data'] as $data) {
                if (isset($data['brand']) and ! empty($data['brand'])) {
                    $brand = mb_strtolower($data['brand']);

                    if (!in_array($brand, $result)) {
                        $result[] = $brand;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция преобразует данные с брандами от armtek в стандартный вид для данной программы
     * @param array $data_armtek
     * @return array $result
     */
    public function standard_view_brands_armtek($data_armtek) {
        $result = array();

        $data_armtek = json_decode($data_armtek, TRUE);

        if (!empty($data_armtek) and isset($data_armtek['RESP']) and ! empty($data_armtek['RESP'])
                and ! isset($data_armtek['RESP']['MSG']) and ! isset($data_armtek['RESP']['ERROR'])) {
            $resp = $data_armtek['RESP'];

            $diff = count($resp) - count($resp, COUNT_RECURSIVE);

            if ($diff == 0) {
                $data = $resp;
                $resp = array();
                $resp[] = $data;
            }

            foreach ($resp as $data) {
                if (isset($data['BRAND']) and ! empty($data['BRAND'])) {
                    $brand = mb_strtolower($data['BRAND']);

                    if (!in_array($brand, $result)) {
                        $result[] = $brand;
                    }
                }
            }
        }

        return $result;
    }

    /* Функция формирует ключ для curl поставщика abs */

    public function get_key_abs() {
        return mb_strtolower(md5($this->config->item('login_abs') . mb_strtolower(md5($this->config->item('pas_abs')))));
    }

    /**
     * Функция получает служебные данные о пользователе от abs
     * @param string $key_abs
     * @return array
     */
    public function get_user_context_abs($key_abs) {
        $url = 'http://api.abs-auto.ru/api-get_user_context?auth=' . $key_abs . '&format=json';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);

        $data = curl_exec($ch);

        curl_close($ch);

        return json_decode($data, true);
    }

    /**
     * Функция получает список брендов по артикулу от поставщика avtoto
     * @param string $article
     * @return array $result
     */
    public function get_brands_avtoto($article) {
		ini_set('default_socket_timeout', '300');

        $login_params = array(
            'user_id' => $this->config->item('api_id_avtoto'),
            'user_login' => $this->config->item('api_login_avtoto'),
            'user_password' => $this->config->item('api_pass_avtoto'),
        );

        $search_analogs = 'off';
        $limit = 1000;
        $result = array();

        $this->load->library('avtoto_parts', $login_params);

        $data_avtoto = $this->avtoto_parts->get_parts($article, $search_analogs, $limit);
        $errors = $this->avtoto_parts->get_errors();

        if (empty($errors) && !empty($data_avtoto['Parts'])) {
            foreach ($data_avtoto['Parts'] as $avtoto_value) {
                if (isset($avtoto_value['Manuf']) and ! empty($avtoto_value['Manuf'])) {
                    $brand = mb_strtolower($avtoto_value['Manuf']);

                    if (!in_array($brand, $result)) {
                        $result[] = $brand;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция сохраняет все результаты поиска в БД
     * @param array $data
     * @return boolean
     */
    public function save_data_search($data) {
        foreach ($data as $value) {
            if ($value['analog'] === 0) {
                $id_detail = $this->save_detail($value);
                break;
            }
        }

        if ($id_detail) {
            $result = TRUE;

            $this->del_cross_for_detail($id_detail);

            foreach ($data as $value) {
                if ($value['analog'] != 0) {
                    if (!$this->save_cross_detail($value, $id_detail)) {
                        $result = FALSE;
                        break;
                    }
                }
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * Функция сохраняет в БД список брендов и названия поставщиков, которые эти бренды выдали (для того, чтобы сократить число обращений по curl)
     * @param string $article
     * @param array $list_brands
     * @return int $id_article_search или boolean
     */
    public function save_data_list_brands_searchs($article, $list_brands) {
        $this->db->trans_start();

        $id_article_search = $this->check_exist_article_search($article);

        if (!$id_article_search) {
            $data = array(
                'article' => $article,
                'date' => date('Y-m-d'),
            );

            $this->db->insert('articles_search', $data);

            $id_article_search = $this->db->insert_id();
        } else {
            $this->db->where('id', $id_article_search);
            $this->db->update('articles_search', array('date' => date('Y-m-d')));

            $this->db->delete('brands_search', array('id_article' => $id_article_search));
        }

        foreach ($list_brands as $brand) {
            $brand['brand'] = strip_tags($brand['brand']);

            $data = array(
                'brand' => $brand['brand'],
                'id_article' => $id_article_search,
                'provider' => $brand['provider'],
            );

            $this->db->insert('brands_search', $data);

            if (!$this->check_exist_brand_in_table_groups_brands($brand['brand'])) {
                do {
                    $key_group = create_random_code(10);
                } while (!empty($this->check_exist_key_equal_brands($key_group)));

                $data = array(
                    'brand' => $brand['brand'],
                    'group_name' => $brand['brand'],
                    'key_group' => $key_group,
                );

                $this->db->insert('groups_brands', $data);
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        } else {
            $this->db->trans_commit();

            return $id_article_search;
        }
    }

    /**
     * Функция проверяет есть ли артикул в таблице articles_search
     * @param string $article
     * @return int $result['id'] или boolean
     */
    public function check_exist_article_search($article) {
        $this->db->select('id');
        $this->db->from('articles_search');
        $this->db->where('article', $article);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        if (!empty($result)) {
            return $result['id'];
        } else {
            return FALSE;
        }
    }

    /**
     * Функция проверяет есть ли бренд в таблице groups_brands
     * @param string $brand
     * @return int $result['id'] или boolean
     */
    public function check_exist_brand_in_table_groups_brands($brand) {
        $this->db->select('id');
        $this->db->from('groups_brands');
        $this->db->where('brand', $brand);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        if (!empty($result)) {
            return $result['id'];
        } else {
            return FALSE;
        }
    }

    /**
     * Функция удаления кроссов для детали
     * @param int $id_detail
     */
    public function del_cross_for_detail($id_detail) {
        $id_cross_array = $this->get_id_cross_by_id_detail($id_detail);

        if (!empty($id_cross_array)) {
            $this->db->trans_start();

            foreach ($id_cross_array as $value) {
                $id_array[] = $value['id_cross'];
            }

            $this->db->delete('detail_cross', array('id_detail' => $id_detail));

            foreach ($id_array as $id) {
                $id_cross_array = $this->get_id_cross_by_id_detail($id);

                $this->db->select('id_detail');
                $this->db->from('detail_cross');
                $this->db->where('id_cross', $id);
                $this->db->where('id_detail !=', $id_detail);

                $query = $this->db->get();

                if (!$query) {
                    return false;
                }

                $id_details_array = $query->result_array();

                if (empty($id_cross_array) and empty($id_details_array)) {
                    $this->db->delete('articles', array('id' => $id));
                }
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();

                return FALSE;
            } else {
                $this->db->trans_commit();

                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * Удаление детали, если она не является кроссом другой детали
     * @param int $id_detail
     */
    public function del_detail_considering_cross($id_detail) {
        $this->db->select('id_detail');
        $this->db->from('detail_cross');
        $this->db->where('id_cross', $id_detail);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $id_details_array = $query->result_array();

        if (empty($id_details_array)) {
            if ($this->db->delete('articles', array('id' => $id_detail))) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            $this->db->trans_start();

            $this->db->where('id', $id_detail);
            $this->db->update('articles', array('is_complete' => 1, 'date' => NULL));

            $this->db->select('id');
            $this->db->from('auto_parts');
            $this->db->where('id_article', $id_detail);

            $query = $this->db->get();

            if (!$query) {
                return false;
            }

            $data = $query->row_array();

            $this->db->delete('offers', array('id_auto_part' => $data['id']));

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();

                return FALSE;
            } else {
                $this->db->trans_commit();

                return TRUE;
            }
        }
    }

    /**
     * Функция удаления бренда
     * @param int $id_brand
     * @return boolean
     */
    public function del_brand($id_brand) {
        $articles_array = $this->get_all_articles_with_this_brand($id_brand);

        if (empty($articles_array)) {
            if ($this->db->delete('brands', array('id' => $id_brand))) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    /**
     * Функция получает все артикулы по указанному бренду
     * @param int $id_brand
     * @return array
     */
    public function get_all_articles_with_this_brand($id_brand) {
        $this->db->select('*');
        $this->db->from('articles');
        $this->db->where('id_brand', $id_brand);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция сохраняет данные оригинальной детали в БД
     * @param array $data
     * @return int $id_row_article or boolean
     */
    public function save_detail($data) {
        $data_articles_table = array(
            'article' => strip_tags($data['article']),
            'is_complete' => 0,
            'date' => date('Y-m-d'),
        );

        if (!isset($data['name'])) {
            $data['name'] = '';
        }

        $data_auto_parts_table = array(
            'name' => strip_tags($data['name']),
        );

        $id_brand = $this->check_exist_brand($data['brand']);

        $this->db->trans_start();
        if (!$id_brand) {
            $this->db->insert('brands', array('name' => strip_tags($data['brand'])));

            $id_brand = $this->db->insert_id();
        }

        $data_articles_table['id_brand'] = $id_brand;

        if (!$id_row_article = $this->get_id_by_article_and_brand($data['article'], $data['brand'])) {
            $this->db->insert('articles', $data_articles_table);

            $id_row_article = $this->db->insert_id();

            $data_auto_parts_table['id_article'] = $id_row_article;
        } else {
            $this->db->where('id', $id_row_article);
            $this->db->update('articles', $data_articles_table);

            $data_auto_parts_table['id_article'] = $id_row_article;
        }

        if (empty($data_detail = $this->get_detail_by_article_brand($data['article'], $data['brand']))) {
            $this->db->insert('auto_parts', $data_auto_parts_table);

            $id_auto_part = $this->db->insert_id();
        } else {
            $id_old_auto_part = $data_detail['auto_part_id'];

            $this->db->where('id', $id_old_auto_part);
            $this->db->update('auto_parts', $data_auto_parts_table);

            $id_auto_part = $id_old_auto_part;

            $old_offers = $this->get_offers_by_id_auto_part($id_auto_part);

            $this->db->delete('offers', array('id_auto_part' => $id_old_auto_part, 'type_search' => 'main'));
        }

        if (isset($data['offers']) and ! empty($data['offers'])) {
            foreach ($data['offers'] as $offer) {
                $data_offers_table[] = array(
                    'price' => round(strip_tags($offer['price']), 2),
                    'quantity' => preg_replace("/(\s*)шт(\.*)/iu", "", strip_tags($offer['quantity'])),
                    'min_part' => preg_replace("/(\s*)шт(\.*)/iu", "", strip_tags($offer['min_part'])),
                    'id_warehouse_type' => $offer['id_warehouse_type'],
                    'description' => strip_tags($offer['description']),
                    'warehouse' => strip_tags($offer['warehouse']),
                    'delivery_period' => preg_replace("/(\s*)дней|день|дня|дн(\.*)/iu", "", strip_tags($offer['delivery_period'])),
                    'id_auto_part' => $id_auto_part,
                    'type_search' => 'main',
                );
            }

            $codes_array = array();

            foreach ($data_offers_table as $n_key => $n_offer) {
                $exists_offer = FALSE;

                if (!empty($old_offers)) {
                    foreach ($old_offers as $o_key => $o_offer) {
                        if (($n_offer['price'] == $o_offer['price']) && ($n_offer['quantity'] == $o_offer['quantity']) && ($n_offer['min_part'] == $o_offer['min_part']) && ($n_offer['id_warehouse_type'] == $o_offer['id_warehouse_type']) && ($n_offer['description'] == $o_offer['description']) && ($n_offer['warehouse'] == $o_offer['warehouse']) && ($n_offer['delivery_period'] == $o_offer['delivery_period'])) {
                            $exists_offer = $o_key;
                            break;
                        }
                    }
                }

                if (!$exists_offer) {
                    do {
                        $code = create_random_code(5) . md5(date('Y-m-d H:i:s'));
                    } while (in_array($code, $codes_array));
                } else {
                    $code = $old_offers[$exists_offer]['code'];
                }

                $data_offers_table[$n_key]['code'] = $code;
                $codes_array[] = $code;
            }

            $this->db->insert_batch('offers', $data_offers_table);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        } else {
            $this->db->trans_commit();

            return $id_row_article;
        }
    }

    public function get_offers_by_id_auto_part($id_auto_part) {
        $this->db->select('*');
        $this->db->from('offers');
        $this->db->where('id_auto_part', $id_auto_part);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция сохраняет аналоги для детали в БД
     * @param array $data
     * @param int $id_detail
     * @return int $id_row_article or boolean
     */
    public function save_cross_detail($data, $id_detail) {
        $compatibility = 0;

        $data_articles_table = array(
            'article' => strip_tags($data['article']),
        );

        if (!isset($data['name'])) {
            $data['name'] = '';
        }

        $data_auto_parts_table = array(
            'name' => strip_tags($data['name']),
        );

        $id_brand = $this->check_exist_brand($data['brand']);

        $this->db->trans_start();
        if (!$id_brand) {
            $this->db->insert('brands', array('name' => strip_tags($data['brand'])));

            $id_brand = $this->db->insert_id();
        }

        $data_articles_table['id_brand'] = $id_brand;

        if (!$id_row_article = $this->get_id_by_article_and_brand($data['article'], $data['brand'])) {
            $this->db->insert('articles', $data_articles_table);

            $id_row_article = $this->db->insert_id();

            $data_auto_parts_table['id_article'] = $id_row_article;
        } else {
            $this->db->where('id', $id_row_article);
            $this->db->update('articles', $data_articles_table);

            $data_auto_parts_table['id_article'] = $id_row_article;
        }

        if (empty($data_detail = $this->get_detail_by_article_brand($data['article'], $data['brand']))) {
            $this->db->insert('auto_parts', $data_auto_parts_table);

            $id_auto_part = $this->db->insert_id();
        } else {
            $id_old_auto_part = $data_detail['auto_part_id'];

            $this->db->where('id', $id_old_auto_part);
            $this->db->update('auto_parts', $data_auto_parts_table);

            $id_auto_part = $id_old_auto_part;

            $old_offers = $this->get_offers_by_id_auto_part($id_auto_part);

            $this->db->delete('offers', array('id_auto_part' => $id_old_auto_part, 'type_search' => 'main'));
        }

        if (isset($data['offers']) and ! empty($data['offers'])) {
            foreach ($data['offers'] as $offer) {
                $data_offers_table[] = array(
                    'id_auto_part' => $id_auto_part,
                    'price' => round(strip_tags($offer['price']), 2),
                    'quantity' => preg_replace("/(\s*)шт(\.*)/iu", "", strip_tags($offer['quantity'])),
                    'min_part' => preg_replace("/(\s*)шт(\.*)/iu", "", strip_tags($offer['min_part'])),
                    'id_warehouse_type' => $offer['id_warehouse_type'],
                    'warehouse' => strip_tags($offer['warehouse']),
                    'description' => strip_tags($offer['description']),
                    'delivery_period' => preg_replace("/(\s*)дней|день|дня|дн(\.*)/iu", "", strip_tags($offer['delivery_period'])),
                    'type_search' => 'main',
                );
            }

            $codes_array = array();

            foreach ($data_offers_table as $n_key => $n_offer) {
                $exists_offer = FALSE;

                if (!empty($old_offers)) {
                    foreach ($old_offers as $o_key => $o_offer) {
                        if (($n_offer['price'] == $o_offer['price']) && ($n_offer['quantity'] == $o_offer['quantity']) && ($n_offer['min_part'] == $o_offer['min_part']) && ($n_offer['id_warehouse_type'] == $o_offer['id_warehouse_type']) && ($n_offer['description'] == $o_offer['description']) && ($n_offer['warehouse'] == $o_offer['warehouse']) && ($n_offer['delivery_period'] == $o_offer['delivery_period'])) {
                            $exists_offer = $o_key;
                            break;
                        }
                    }
                }

                if (!$exists_offer) {
                    do {
                        $code = create_random_code(5) . md5(date('Y-m-d H:i:s'));
                    } while (in_array($code, $codes_array));
                } else {
                    $code = $old_offers[$exists_offer]['code'];
                }

                $data_offers_table[$n_key]['code'] = $code;
                $codes_array[] = $code;
            }

            $this->db->insert_batch('offers', $data_offers_table);
        }

        foreach ($data['rating'] as $rating) {
            $compatibility = $compatibility + $rating;
        }

        $data_detail_cross_table = array(
            'id_detail' => $id_detail,
            'id_cross' => $id_row_article,
            'compatibility' => $compatibility,
        );

        $this->db->insert('detail_cross', $data_detail_cross_table);

        if (!$this->check_exist_brand_in_table_groups_brands($data['brand'])) {
            do {
                $key_group = create_random_code(10);
            } while (!empty($this->check_exist_key_equal_brands($key_group)));

            $data = array(
                'brand' => strip_tags($data['brand']),
                'group_name' => strip_tags($data['brand']),
                'key_group' => $key_group,
            );

            $this->db->insert('groups_brands', $data);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        } else {
            $this->db->trans_commit();

            return $id_row_article;
        }
    }

    /**
     * Функция проверяет есть ли указанный бренд в БД
     * @param string $brand
     * @return int $result['id'] or boolean
     */
    public function check_exist_brand($brand) {
        $this->db->select('id');
        $this->db->from('brands');
        $this->db->where('name', $brand);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        if (!empty($result)) {
            return $result['id'];
        } else {
            return FALSE;
        }
    }

    /**
     * Функция получает id всех аналогов по id детали
     * @param int $id
     * @return array
     */
    public function get_id_cross_by_id_detail($id) {
        $this->db->select('id_cross');
        $this->db->from('detail_cross');
        $this->db->where('id_detail', $id);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция проверяет наличие артикула в БД
     * @param string $article
     * @return array
     */
    public function check_exist_article($article) {
        $this->db->select('id');
        $this->db->from('articles');
        $this->db->where('article', $article);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция получает данные детали по артикулу и бренду
     * @param string $article
     * @param string $brand
     * @return array
     */
    public function get_detail_by_article_brand($article, $brand) {
        $this->db->select('auto_parts.id as auto_part_id,
                           auto_parts.name as auto_part_name');
        $this->db->from('auto_parts');
        $this->db->join('articles', 'articles.id = auto_parts.id_article');
        $this->db->join('brands', 'brands.id = articles.id_brand');
        $this->db->where('articles.article', $article);
        $this->db->where('brands.name', $brand);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        return $result;
    }

    /**
     * Функция выбирает из БД весь список деталей, найденных по артикулу и группе
     * @param string $article
     * @param string $group_name
     * @param string $field_sort
     * @param string $type_sort
     * @param string $search_type
     * @return array $result
     */
    public function get_search_list_details($article, $group_name, $shopping_cart, $type_sort, $filter_type_warehouse, $recom_brands, $other_data) {
        $result = array();
        $details_array = array();
        $providers_in_list_details = array();
        $brand = $group_name;
        $max_price_detail = 0;
        $min_price_cross = 0;
        $is_complete = 1;

        $id = $this->get_id_by_article_and_brand($article, $brand);

        if (!empty($brand) && !empty($id)) {
            $data_detail = $this->get_data_detail($id, $recom_brands, $shopping_cart);

            if (isset($other_data['is_complete']) && in_array($other_data['is_complete'], array(1, 0))) {
                $is_complete = $other_data['is_complete'];
            } elseif (isset($data_detail['is_complete'])) {
                $is_complete = $data_detail['is_complete'];
            }

            if (!empty($data_detail) && isset($data_detail['offers']) && !empty($data_detail['offers'])) {
                $details_array['original'] = $data_detail;
            }

            $cross_detail = $this->get_cross_detail($id, $recom_brands, $shopping_cart);

            if (!empty($cross_detail)) {
                foreach ($cross_detail as $value) {
                    if (isset($value['offers']) && !empty($value['offers'])) {
                        $details_array['cross'][] = $value;
                    }
                }
            }

            $providers_in_list_details = $this->get_providers_in_list_details($id, $is_complete);

            if (!empty($details_array)) {
                $details_array = $this->filter_type_warehouse_and_is_complete($details_array, $filter_type_warehouse, $is_complete);
            }

            if (!empty($details_array)) {
                $details_array = $this->filter_list_details_by_delivery_period($details_array, $other_data);
            }

            if (!empty($details_array)) {
                if (isset($details_array['original'])) {
                    $offers = $details_array['original']['offers'];

                    foreach ($offers as $offer) {
                        if ($max_price_detail < $offer['retail_price']) {
                            $max_price_detail = $offer['retail_price'];
                        }
                    }
                }

                if (isset($details_array['cross'])) {
                    foreach ($details_array['cross'] as $key => $detail) {
                        $offers = $detail['offers'];

                        foreach ($offers as $offer) {
                            if ($max_price_detail < $offer['retail_price']) {
                                $max_price_detail = $offer['retail_price'];
                            }
                        }
                    }
                }
            }

            if (!isset($other_data['price_between_beg'])) {
                $price_between_beg = 0;
            } else {
                $price_between_beg = (int) $other_data['price_between_beg'];
            }

            if (!isset($other_data['price_between_end'])) {
                $price_between_end = $max_price_detail;
            } else {
                $price_between_end = (int) $other_data['price_between_end'];
            }

            $details_array = $this->filter_list_details_by_price($details_array, $price_between_beg, $price_between_end);

            $result['price_between_beg'] = $price_between_beg;
            $result['price_between_end'] = $price_between_end;
            $result['max_price_detail'] = $max_price_detail;

            if (!empty($details_array)) {
                $details_array = $this->sort_search_list_details($details_array, $type_sort);
            }

            if (isset($details_array['original'])) {
                $offers = $details_array['original']['offers'];

                $i = 0;

                foreach ($offers as $key_offer => $offer) {
                    if ($i == 4) {
                        $details_array['original']['nearest_offer_price_for_client'] = $offer['retail_price'];
                    }

                    $i++;
                }
            }

            if (isset($details_array['cross'])) {
                foreach ($details_array['cross'] as $key_cross => $cross) {
                    $offers = $cross['offers'];

                    $i = 0;

                    foreach ($offers as $key_offer => $offer) {
                        if ($i == 4) {
                            $details_array['cross'][$key_cross]['nearest_offer_price_for_client'] = $offer['retail_price'];
                        }

                        $i++;
                    }
                }

                $offers = $details_array['cross'][$key_cross]['offers'];

                foreach ($offers as $offer) {
                    if (($min_price_cross == 0) || ($offer['retail_price'] < $min_price_cross)) {
                        $min_price_cross = $offer['retail_price'];
                    }
                }
            }
        }

        $result['list_details'] = $details_array;
        $result['providers_in_list_details'] = $providers_in_list_details;
        $result['is_complete'] = $is_complete;
        $result['min_price_cross'] = $min_price_cross;

        if(!isset($result['max_price_detail'])) {
            $result['max_price_detail'] = $max_price_detail;
        }

        return $result;
    }

    /**
     * Функция производит сортировку списка найденных деталей
     * @param array $details_array
     * @param string $field_sort
     * @param string $type_sort
     * @return array $details_array
     */
    public function sort_search_list_details($details_array, $type_sort) {
        if ($type_sort == 1) {
            if (isset($details_array['cross'])) {
                $compatibility_array = array();
                $type_provider_array = array();
                $warehouse_type_array = array();
                $price_array = array();

                foreach ($details_array['cross'] as $key_cross => $cross) {
                    $first_offer = array_shift($cross['offers']);

                    array_push($compatibility_array, $cross['compatibility']);
                    array_push($type_provider_array, $first_offer['type_provider']);
                    array_push($warehouse_type_array, $first_offer['warehouse_type']);
                    array_push($price_array, $first_offer['retail_price']);
                }

                array_multisort($type_provider_array, SORT_DESC, $warehouse_type_array, SORT_REGULAR, $compatibility_array, SORT_DESC, $price_array, SORT_REGULAR, $details_array['cross']);
            }

            return $details_array;
        }

        if (($type_sort == 2) or ( $type_sort == 4)) {
            if ($type_sort == 2) {
                $field_sort = 'retail_price';
            } else {
                $field_sort = 'del_period_for_filter';
            }

            if (isset($details_array['original'])) {
                $sort_array = array();
                $price_array = array();
                $type_array = array();
                $type_prov_array = array();
                $compatibility_array = array();

                foreach ($details_array['original']['offers'] as $key => $offer) {
                    $min_type = 3;
                    $max_type_provider = 0;

                    $sort_array[] = $offer[$field_sort];

                    array_push($price_array, $offer['retail_price']);

                    if ($offer['warehouse_type'] < $min_type) {
                        $min_type = $offer['warehouse_type'];
                    }

                    if ($offer['type_provider'] > $max_type_provider) {
                        $max_type_provider = $min_type = $offer['type_provider'];
                    }

                    array_push($type_array, $min_type);
                    array_push($type_prov_array, $max_type_provider);

                    $details_array['original']['type_provider'] = $max_type_provider;
                    $details_array['original']['warehouse_type'] = $min_type;
                }

                if ($type_sort == 2) {
                    array_multisort($sort_array, SORT_ASC, $type_prov_array, SORT_DESC, $type_array, SORT_REGULAR, $details_array['original']['offers']);
                } else {
                    array_multisort($sort_array, SORT_ASC, $type_prov_array, SORT_DESC, $type_array, SORT_REGULAR, $price_array, SORT_REGULAR, $details_array['original']['offers']);
                }

                $details_array['original'][$field_sort] = $details_array['original']['offers'][0][$field_sort];
            }

            if (isset($details_array['cross'])) {

                foreach ($details_array['cross'] as $key_cross => $cross) {
                    $sort_array = array();
                    $price_array = array();
                    $type_array = array();
                    $type_prov_array = array();
                    $compatibility_array = array();

                    foreach ($cross['offers'] as $key_offer => $offer) {
                        $min_type = 3;
                        $max_type_provider = 0;

                        $sort_array[] = $offer[$field_sort];

                        array_push($price_array, $offer['retail_price']);

                        if ($offer['warehouse_type'] < $min_type) {
                            $min_type = $offer['warehouse_type'];
                        }

                        if ($offer['type_provider'] > $max_type_provider) {
                            $max_type_provider = $min_type = $offer['type_provider'];
                        }

                        array_push($type_array, $min_type);
                        array_push($type_prov_array, $max_type_provider);

                        $details_array['cross'][$key_cross]['type_provider'] = $max_type_provider;
                        $details_array['cross'][$key_cross]['warehouse_type'] = $min_type;
                    }

                    if ($type_sort == 2) {
                        array_multisort($sort_array, SORT_ASC, $type_prov_array, SORT_DESC, $type_array, SORT_REGULAR, $details_array['cross'][$key_cross]['offers']);
                    } else {
                        array_multisort($sort_array, SORT_ASC, $type_prov_array, SORT_DESC, $type_array, SORT_REGULAR, $price_array, SORT_REGULAR, $details_array['cross'][$key_cross]['offers']);
                    }

                    $details_array['cross'][$key_cross][$field_sort] = $details_array['cross'][$key_cross]['offers'][0][$field_sort];
                }

                $sort_array = array();
                $compatibility_array = array();
                $type_provider_array = array();
                $warehouse_type_array = array();

                foreach ($details_array['cross'] as $key_cross => $cross) {
                    array_push($sort_array, $cross[$field_sort]);
                    array_push($compatibility_array, $cross['compatibility']);
                    array_push($type_provider_array, $cross['type_provider']);
                    array_push($warehouse_type_array, $cross['warehouse_type']);
                }

                array_multisort($sort_array, SORT_ASC, $type_provider_array, SORT_DESC, $warehouse_type_array, SORT_REGULAR, $compatibility_array, SORT_DESC, $details_array['cross']);
            }

            return $details_array;
        }

        if (isset($details_array['cross'])) {
            $sort_array = array();
            $compatibility_array = array();
            $type_provider_array = array();
            $warehouse_type_array = array();
            $price_array = array();

            if ($type_sort == 3) {
                $field_sort = 'brand';
            } elseif ($type_sort == 5) {
                $field_sort = 'rating';
            } else {
                return $details_array;
            }

            foreach ($details_array['cross'] as $key_cross => $cross) {
                array_push($sort_array, $cross[$field_sort]);

                $first_offer = array_shift($cross['offers']);

                array_push($compatibility_array, $cross['compatibility']);
                array_push($type_provider_array, $first_offer['type_provider']);
                array_push($warehouse_type_array, $first_offer['warehouse_type']);
                array_push($price_array, $first_offer['retail_price']);
            }

            if ($field_sort == 'rating') {
                array_multisort($sort_array, SORT_DESC, SORT_NUMERIC, $type_provider_array, SORT_DESC, $warehouse_type_array, SORT_REGULAR, $compatibility_array, SORT_DESC, $price_array, SORT_REGULAR, $details_array['cross']);
            } else {
                array_multisort($sort_array, SORT_ASC, $type_provider_array, SORT_DESC, $warehouse_type_array, SORT_REGULAR, $compatibility_array, SORT_DESC, $price_array, SORT_REGULAR, $details_array['cross']);
            }
        }

        return $details_array;
    }

    /**
     * Функция производит фильтр списка найденных деталей по типу склада
     * @param array $details_array
     * @param string $filter_type_warehouse
     * @return array $result
     */
    public function filter_type_warehouse_and_is_complete($details_array, $filter_type_warehouse, $is_complete) {
        $result = array();
        $temp_details_array = array();
        $temp_offers_array = array();
        $new_temp_offers_array = array();

        if (isset($details_array['original'])) {
            $details_array['original']['type'] = 'original';

            $temp_details_array[] = $details_array['original'];
        }

        if (isset($details_array['cross'])) {
            foreach ($details_array['cross'] as $key => $value) {
                $value['type'] = 'cross';

                $temp_details_array[] = $value;
            }
        }

        foreach ($temp_details_array as $key_detail => $detail) {
            foreach ($detail['offers'] as $value) {
                $value['key_detail'] = $key_detail;

                if ($is_complete == '1') {
                    $temp_offers_array[] = $value;
                } else {
                    if ($value['type_search'] == 'main') {
                        $temp_offers_array[] = $value;
                    }
                }
            }

            $temp_details_array[$key_detail]['offers'] = array();
        }

        if ($filter_type_warehouse == 'one') {
            foreach ($temp_offers_array as $value) {
                if ($value['warehouse_type'] == 1) {
                    $new_temp_offers_array[] = $value;
                }
            }
        } elseif ($filter_type_warehouse == 'three') {
            $new_temp_offers_array = $temp_offers_array;
        } elseif ($filter_type_warehouse == 'one_two') {
            foreach ($temp_offers_array as $value) {
                if (($value['warehouse_type'] == 1) or ( $value['warehouse_type'] == 2)) {
                    $new_temp_offers_array[] = $value;
                }
            }
        } else {
            foreach ($temp_offers_array as $value) {
                if (($value['warehouse_type'] == 1) or ( $value['warehouse_type'] == 2)) {
                    $new_temp_offers_array[] = $value;
                }
            }

            if (empty($new_temp_offers_array)) {
                $new_temp_offers_array = $temp_offers_array;
            }
        }

        if (!empty($new_temp_offers_array)) {
            foreach ($new_temp_offers_array as $offer) {
                $key_detail = $offer['key_detail'];

                unset($offer['key_detail']);

                $temp_details_array[$key_detail]['offers'][] = $offer;
            }

            foreach ($temp_details_array as $detail) {
                if (!empty($detail['offers'])) {
                    if ($detail['type'] == 'original') {
                        unset($detail['type']);

                        $result['original'] = $detail;
                    } else {
                        unset($detail['type']);

                        $result['cross'][] = $detail;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция получает список опрошенных при поиске поставщиков.
     * @param int $id
     * @return array $result
     */
    public function get_providers_in_list_details($id, $is_complete) {
        $result = array();
        $id_articles_array = array();

        $this->db->select('detail_cross.id_cross');
        $this->db->from('detail_cross');
        $this->db->where('detail_cross.id_detail', $id);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        array_push($id_articles_array, $id);

        if (!empty($query->result_array())) {
            foreach ($query->result_array() as $value) {
                $id_articles_array[] = $value['id_cross'];
            }
        }

        $this->db->select('type_warehouse.id_provider');
        $this->db->distinct();
        $this->db->from('type_warehouse');
        $this->db->join('offers', 'offers.id_warehouse_type = type_warehouse.id');
        $this->db->join('auto_parts', 'auto_parts.id = offers.id_auto_part');
        $this->db->where_in('auto_parts.id_article', $id_articles_array);

        if ($is_complete != 1) {
            $this->db->where('offers.type_search', 'main');
        }

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        if (!empty($query->result_array())) {
            foreach ($query->result_array() as $value) {
                $result[] = $value['id_provider'];
            }
        }

        return $result;
    }

    /**
     * Функция получает название группы эквивалентных брендов по ключу группы
     * @param string $key_group
     * @return string $result['group_name']
     */
    public function get_group_name_by_key_group($key_group) {
        $this->db->select('group_name');
        $this->db->distinct();
        $this->db->from('groups_brands');
        $this->db->where('key_group', $key_group);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        if (!empty($result)) {
            return $result['group_name'];
        } else {
            return '';
        }
    }

    /**
     * Функция получает id из таблицы articles по артикулу и бренду
     * @param string $article
     * @param string $brand
     * @return int $result['id']
     */
    public function get_id_by_article_and_brand($article, $brand) {
        $this->db->select('articles.id');
        $this->db->from('articles');
        $this->db->join('brands', 'brands.id = articles.id_brand');
        $this->db->where('articles.article', $article);
        $this->db->where('brands.name', $brand);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        if (!empty($result)) {
            return $result['id'];
        } else {
            return '';
        }
    }

    /**
     * Функция получает все данные по оригинальной детали
     * @param int $id
     * @return array $result
     */
    public function get_data_detail($id, $recom_brands, $shopping_cart) {
        $this->db->select('auto_parts.id,
                           auto_parts.name,
                           brands.id as brand_id,
                           brands.name as brand,
                           brands.description as brand_description,
                           brands.country as brand_country,
                           brands.link as brand_link,
                           brands.recommended,
                           brands.rating,
                           articles.article,
                           articles.is_complete,
                           groups_brands.key_group');
        $this->db->from('auto_parts');
        $this->db->join('articles', 'articles.id = auto_parts.id_article');
        $this->db->join('brands', 'brands.id = articles.id_brand');
        $this->db->join('groups_brands', 'groups_brands.group_name = brands.name');
        $this->db->where('articles.id', $id);

        if ($recom_brands == 'recom_brands') {
            $this->db->where('brands.recommended', 1);
        }

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        if (!empty($result)) {
            $offers = $this->get_all_data_offers_for_detail($result['id']);

            if (!empty($offers)) {
                $list_providers_rating = $this->get_list_providers_for_detail($result['id']);

                foreach ($offers as $id_offer => $offer) {
                    $in_shopping_cart = 0;
                    $offers[$id_offer]['provider_with_link'] = $this->create_link_for_offer($result['article'], $offer['provider'], $offer['link_provider']);
                    $offers[$id_offer]['del_period_for_filter'] = $this->get_del_period_for_filter($offer['delivery_period']);

                    $list_fields_type_warehouse = array(
                        'opportunity_return',
                        'hold_percentage',
                        'time_return',
                    );

                    if (isset($shopping_cart['details']) && !empty($shopping_cart['details'])) {
                        foreach ($shopping_cart['details'] as $detail) {
                            if ($detail['code_offer'] == $offer['code']) {
                                $in_shopping_cart = $detail['count'];

                                break;
                            }
                        }
                    }

                    $offers[$id_offer]['popover_content_type_warehouse'] = $this->create_popover_with_data_type_warehouse($offer, $list_fields_type_warehouse);

                    $offers[$id_offer]['popover_with_description_for_type_warehouse'] = $this->create_popover_with_data_type_warehouse($offer, array('comment'));

                    if (!empty($offer['price_lists_date'])) {
                        $date_end = strtotime("+5 day", strtotime($offer['price_lists_date']));
                        $date_now = strtotime(date('Y-m-d'));

                        if ($date_end <= $date_now) {
                            $offers[$id_offer]['old_price_list'] = 1;
                        }
                    }

                    $offers[$id_offer]['in_shopping_cart'] = $in_shopping_cart;

                    $offers[$id_offer]['retail_price'] = $this->calculation_retail_price($offer['price']);
                }

                $result['popover_list_provider_rating'] = $this->create_popover_list_provider_rating($list_providers_rating);
            }

            $result['offers'] = $offers;

            $result['analog'] = 0;
            $result['compatibility'] = 'original';

            if (empty($result['brand_description'])
                    and empty($result['brand_country']) and empty($result['brand_link'])) {
                $result['need_edit_brand'] = 1;
            } else {
                $result['need_edit_brand'] = 0;

                $result['popover_content_brand'] = $this->create_popover_brand($result);
            }
        }

        return $result;
    }

    /**
     * Функция получает все данные по аналогам для оригинальной детали
     * @param int $id
     * @return array $result
     */
    public function get_cross_detail($id, $recom_brands, $shopping_cart) {
        $this->db->select('auto_parts.id,
                           auto_parts.name,
                           brands.id as brand_id,
                           brands.name as brand,
                           brands.description as brand_description,
                           brands.country as brand_country,
                           brands.link as brand_link,
                           brands.recommended,
                           brands.rating,
                           articles.article,
                           detail_cross.compatibility,
                           groups_brands.key_group');
        $this->db->distinct();
        $this->db->from('detail_cross');
        $this->db->join('articles', 'articles.id = detail_cross.id_cross');
        $this->db->join('brands', 'brands.id = articles.id_brand');
        $this->db->join('auto_parts', 'articles.id = auto_parts.id_article');
        $this->db->join('groups_brands', 'groups_brands.group_name = brands.name');
        $this->db->where('detail_cross.id_detail', $id);

        if ($recom_brands == 'recom_brands') {
            $this->db->where('brands.recommended', 1);
        }

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->result_array();

        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $offers = $this->get_all_data_offers_for_detail($value['id']);

                if (!empty($offers)) {
                    $list_providers_rating = $this->get_list_providers_for_detail($value['id']);

                    foreach ($offers as $id_offer => $offer) {
                        $in_shopping_cart = 0;
                        $offers[$id_offer]['provider_with_link'] = $this->create_link_for_offer($value['article'], $offer['provider'], $offer['link_provider']);
                        $offers[$id_offer]['del_period_for_filter'] = $this->get_del_period_for_filter($offer['delivery_period']);

                        $list_fields_type_warehouse = array(
                            'opportunity_return',
                            'hold_percentage',
                            'time_return',
                        );

                        if (isset($shopping_cart['details']) && !empty($shopping_cart['details'])) {
                            foreach ($shopping_cart['details'] as $detail) {
                                if ($detail['code_offer'] == $offer['code']) {
                                    $in_shopping_cart = $detail['count'];

                                    break;
                                }
                            }
                        }

                        $offers[$id_offer]['popover_content_type_warehouse'] = $this->create_popover_with_data_type_warehouse($offer, $list_fields_type_warehouse);

                        $offers[$id_offer]['popover_with_description_for_type_warehouse'] = $this->create_popover_with_data_type_warehouse($offer, array('comment'));

                        if (!empty($offer['price_lists_date'])) {
                            $date_end = strtotime("+5 day", strtotime($offer['price_lists_date']));
                            $date_now = strtotime(date('Y-m-d'));

                            if ($date_end <= $date_now) {
                                $offers[$id_offer]['old_price_list'] = 1;
                            }
                        }

                        $offers[$id_offer]['in_shopping_cart'] = $in_shopping_cart;

                        $offers[$id_offer]['retail_price'] = $this->calculation_retail_price($offer['price']);
                    }

                    $result[$key]['popover_list_provider_rating'] = $this->create_popover_list_provider_rating($list_providers_rating);
                }

                $result[$key]['offers'] = $offers;

                $result[$key]['analog'] = 1;

                if (empty($value['brand_description'])
                        and empty($value['brand_country']) and empty($value['brand_link'])) {
                    $result[$key]['need_edit_brand'] = 1;
                } else {
                    $result[$key]['need_edit_brand'] = 0;

                    $result[$key]['popover_content_brand'] = $this->create_popover_brand($value);
                }
            }
        }

        return $result;
    }

    /**
     * Функция получает список поставщиков для конкретной детали
     * @param int $id_auto_part
     * @return array
     */
    public function get_list_providers_for_detail($id_auto_part) {
        $this->db->select('provider.id as id_provider,
                           provider.name as provider,
                           provider.source_data');
        $this->db->distinct();
        $this->db->from('offers');
        $this->db->join('type_warehouse', 'type_warehouse.id = offers.id_warehouse_type');
        $this->db->join('provider', 'provider.id = type_warehouse.id_provider');
        $this->db->where('offers.id_auto_part', $id_auto_part);
        $this->db->order_by('provider.name', 'asc');

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция создает окошко с поставщиками для конкретной детали
     * @param array $data
     * @return string
     */
    public function create_popover_list_provider_rating($data) {
        $str = '';

        foreach ($data as $value) {
            if ($value['source_data'] == 'api') {
                $str .= '<span class="provider_rating_api">' . $value['provider'] . '</span><br>';
            } else {
                $str .= '<span class="provider_rating_price">' . $value['provider'] . '</span><br>';
            }
        }

        $str = htmlspecialchars($str);

        return "<div>$str</div>";
    }

    /**
     * Функция создает окошко с данными о бренде
     * @param array $data
     * @return string
     */
    public function create_popover_brand($data) {
        $str = '';

        if (!empty($data['brand_description'])) {
            $str .= '<strong>Описание</strong>:<br>' . $data['brand_description'] . '<br>';
        }

        if (!empty($data['brand_country'])) {
            $str .= '<strong>Страна</strong>:<br>' . $data['brand_country'] . '<br>';
        }

        if (!empty($data['brand_link'])) {
            $str .= '<strong>Ссылка</strong>:<br><a href=\'http://' . $data['brand_link'] . '\' target=\'_blank\'>' . $data['brand_link'] . '</a><br>';
        }

        $str = htmlspecialchars($str);

        return "<div>$str</div>";
    }

    /**
     * Функция создает окошко с данными о складе
     * @param array $list_fields
     * @return string $popover_content
     */
    public function create_popover_with_data_type_warehouse($data, $list_fields = array()) {
        $popover_content = '<div>';

        if (in_array('opportunity_return', $list_fields)) {
            if ($data['opportunity_return'] == 1) {
                $popover_content .= '<strong>Возможность возврата</strong>: Да<br>';
            } else {
                $popover_content .= '<strong>Возможность возврата</strong>: Нет<br>';
            }
        }

        if (in_array('hold_percentage', $list_fields) && ($data['opportunity_return'] == 1)) {
            $popover_content .= '<strong>% удержания при возврате</strong>: ' . $data['hold_percentage'] . '<br>';
        }

        if (in_array('comment', $list_fields)) {
            $popover_content .= '<strong>Комментарий</strong>: ' . $data['comment'] . '<br>';
        }

        if (in_array('time_return', $list_fields) && ($data['opportunity_return'] == 1)) {
            $popover_content .= '<strong>Срок возврата</strong>: ' . $data['time_return'] . '<br>';
        }

        $popover_content .= '</div>';

        $popover_content = htmlspecialchars($popover_content);

        return $popover_content;
    }

    /**
     * Функция создает ссылку поставщика для конкретного предложения
     * @param string $article
     * @param string $link
     * @param string $provider
     * @return string $result
     */
    public function create_link_for_offer($article, $provider, $link) {
        $provider = '<span class="provider_name">' . $provider . '</span>';

        if (!empty($link)) {

            $link = str_replace(' ', '', $link);

            $link = str_replace('{article}', $article, $link);

            $result = anchor('http://' . $link, $provider, array('target' => '_blank'));
        } else {
            $result = $provider;
        }

        return $result;
    }

    /**
     * Функция получает список предложений для детали
     * @param int $id_auto_part
     * @return array
     */
    public function get_all_data_offers_for_detail($id_auto_part) {
        $result = array();

        $this->db->select('offers.id,
                           type_warehouse.type as warehouse_type,
                           type_warehouse.opportunity_return,
                           type_warehouse.hold_percentage,
                           type_warehouse.comment,
                           type_warehouse.time_return,
                           offers.price,
                           offers.quantity,
                           offers.min_part,
                           offers.code,
                           offers.description as offers_description,
                           offers.warehouse,
                           offers.delivery_period,
                           offers.type_search,
                           provider.id as provider_id,
                           provider.name as provider,
                           provider.link as link_provider,
                           provider.icon as provider_icon,
                           provider.type_provider as type_provider,
                           price_lists.date as price_lists_date');
        $this->db->from('offers');
        $this->db->join('type_warehouse', 'type_warehouse.id = offers.id_warehouse_type');
        $this->db->join('provider', 'provider.id = type_warehouse.id_provider');
        $this->db->join('price_lists', 'price_lists.id_type_warehouse = type_warehouse.id', 'left');
        $this->db->where('offers.id_auto_part', (int) $id_auto_part);
        $this->db->where('provider.show_in_search', 1);
        $this->db->order_by('provider.type_provider', 'desc');
        $this->db->order_by('type_warehouse.type', 'asc');
        $this->db->order_by('offers.price', 'asc');

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция получает список деталей по списку предложений
     * @param array $offers
     * @return array $result
     */
    public function get_details_by_offers($offers) {
        $this->db->select('articles.article,
                           brands.id as brand_id,
                           brands.name as brand,
                           brands.description as brand_description,
                           brands.country as brand_country,
                           brands.link as brand_link,
                           articles.id as id_detail,
                           offers.id as id_offer,
                           offers.price,
                           offers.description,
                           offers.delivery_period,
                           offers.quantity,
                           provider.name as provider');
        $this->db->from('articles');
        $this->db->join('brands', 'brands.id = articles.id_brand');
        $this->db->join('auto_parts', 'articles.id = auto_parts.id_article');
        $this->db->join('offers', 'offers.id_auto_part = auto_parts.id');
        $this->db->join('type_warehouse', 'type_warehouse.id = offers.id_warehouse_type');
        $this->db->join('provider', 'provider.id = type_warehouse.id_provider');
        $this->db->where_in('offers.id', $offers);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->result_array();

        if (!empty($result)) {
            foreach ($result as $key => $value) {
                if (empty($value['brand_description'])
                        and empty($value['brand_country']) and empty($value['brand_link'])) {
                    $result[$key]['need_edit_brand'] = 1;
                } else {
                    $result[$key]['need_edit_brand'] = 0;

                    $result[$key]['popover_content_brand'] = $this->create_popover_brand($value);
                }

                $retail_price = $this->calculation_retail_price($value['price']);
                $result[$key]['retail_price'] = number_format($retail_price, 2, ',', ' ');
                $result[$key]['price'] = number_format($value['price'], 2, ',', ' ');
            }
        }

        return $result;
    }

    /**
     * Функция получает список id типов складов для поставщика
     * @param string $provider
     * @return array $result
     */
    public function get_ids_for_warehouses_types_provider($provider) {
        $result = array();

        $this->db->select('type_warehouse.id, type_warehouse.type');
        $this->db->from('type_warehouse');
        $this->db->join('provider', 'provider.id = type_warehouse.id_provider');
        $this->db->where('provider.name', $provider);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $data = $query->result_array();

        if (!empty($data)) {
            foreach ($data as $value) {
                $result[$value['type']] = $value['id'];
            }
        }

        return $result;
    }

    /**
     * Функция получает рейтинг для поставщика
     * @param string $provider_name
     * @return string $result['rating']
     */
    public function get_rating_provider($provider_name) {
        $this->db->select('rating');
        $this->db->from('provider');
        $this->db->where('name', $provider_name);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        return isset($result['rating']) ? $result['rating'] : 0;
    }

    /**
     * Функция высчитывает розничную цену
     * @param int/float $price
     * @return int $result or int/float $price
     */
    public function calculation_retail_price($price) {
        $this->db->select('percent');
        $this->db->from('prices');
        $this->db->where('price <=', $price);
        $this->db->order_by('price', 'desc');
        $this->db->limit(1);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        if (!empty($data = $query->row_array())) {
            $percent = $price * ($data['percent'] / 100);

            $result = ceil(($price + $percent) / 10) * 10;

            return $result;
        } else {
            return $price;
        }
    }

    /**
     * Функция получает дату последнего обновления списка брендов для поиска
     * @param string $article
     * @return string $result['date'] or array $result
     */
    public function get_date_update_list_brands_searchs($article) {
        $result = array();

        $this->db->select('date');
        $this->db->from('articles_search');
        $this->db->where('article', $article);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        if (!empty($result)) {
            return $result['date'];
        } else {
            return $result;
        }
    }

    /**
     * Функция получает данные из таблицы articles по id
     * @param int $id_article
     * @return array
     */
    public function get_article_by_id($id_article) {
        $this->db->select('id');
        $this->db->from('articles');
        $this->db->where('id', $id_article);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    /**
     * Функция проверяет наличие бренда в БД по id
     * @param int $id_brand
     * @return int $result['id'] or boolean
     */
    public function check_exist_brand_by_id($id_brand) {
        $this->db->select('id');
        $this->db->from('brands');
        $this->db->where('id', $id_brand);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        if (!empty($result)) {
            return $result['id'];
        } else {
            return FALSE;
        }
    }

    /**
     * Функция для обновления бренда
     * @param int $id_brand
     * @param array $data
     * @return boolean
     */
    public function update_brand($id_brand, $data) {
        $this->db->where('id', $id_brand);
        $this->db->update('brands', $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Функция получает данные от api и прайс-листов и записывает эти данные в БД
     * @param string $article
     * @param string $group_name
     * @return boolean
     */
    public function update_data_search_detail($article, $group_name) {
        $result = array();
        $providers_array = array();
        $data_array = array();

        $equal_brands = $this->get_equal_brands_by_group_name($group_name);

        $brands_search = $this->get_brands_search_by_article_equal_brands($article, $equal_brands);

        if (!empty($brands_search) and ! empty($equal_brands)) {
            foreach ($brands_search as $brand) {
                if (!in_array($brand['provider'], $providers_array)) {
                    $providers_array[] = $brand['provider'];
                }
            }

            $result = $this->get_api_data_from_curl($article, $brands_search, $providers_array);

            if (in_array('avtoto', $providers_array)) {
                $data_avtoto = $this->get_data_api_avtoto($article, $equal_brands);

                $result = array_merge($result, $data_avtoto);
            }
        }

        if (!empty($result)) {
            $result = $this->merge_api_data($result);

            $res_search_in_price_lists = array();
            $brands_in_list = array();

            foreach ($result as $res) {
                if(!in_array($res['brand'], $brands_in_list)) {
                    $brands_in_list[] = $res['brand'];
                }
            }

            $groups_brands = $this->get_groups_brands_for_brands($brands_in_list);

            $res_search_in_price_lists = $this->search_details_from_price_list($result);

            $result = $this->del_duplicate_brands_in_data($result, $article, $group_name, $groups_brands);

            if (!empty($res_search_in_price_lists)) {
                $result = $this->merge_price_data_and_api_data($result, $res_search_in_price_lists, $groups_brands);
            }
        }

        $brand = $group_name;

        if (empty($result)) {
            $result[] = array(
                'article' => $article,
                'brand' => $brand,
                'name' => '',
                'analog' => 0,
                'rating' => 'original',
            );
        }

        if ($this->save_data_search($result)) {
            return TRUE;
        } else {
            $id = $this->get_id_by_article_and_brand($article, $brand);

            if (!empty($id)) {
                $this->del_cross_for_detail($id);
                $this->del_detail_considering_cross($id);
            }

            return FALSE;
        }
    }

    /**
     * Функция получает в результете поиска данные от api по curl
     * @param string $article
     * @param array $brands_search
     * @param array $providers_array
     * @return array $result
     */
    public function get_api_data_from_curl($article, $brands_search, $providers_array) {
        $mh = curl_multi_init();
        $chs = array();
        $result = array();

        foreach ($brands_search as $brand) {
            if ($brand['provider'] == 'berg') {
                $analogs_berg = $this->config->item('analogs_berg');

                $url_berg = 'https://api.berg.ru/ordering/get_stock.json?items[0][resource_article]=' . $article . '&items[0][brand_name]=' . $brand['brand'] . '&analogs=' . $analogs_berg . '&key=' . $this->config->item('api_key_berg');

                $ch_berg = curl_init();

                $chs[] = array(
                    'ch' => $ch_berg,
                    'provider' => 'berg',
                    'article' => $article,
                    'brand' => $brand['brand'],
                );

                curl_setopt($ch_berg, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch_berg, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_berg, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch_berg, CURLOPT_TIMEOUT, 25);
                curl_setopt($ch_berg, CURLOPT_URL, $url_berg);
                curl_multi_add_handle($mh, $ch_berg);
            }
        }

        if (in_array('micado', $providers_array)) {
            $login = $this->config->item('login_micado');
            $password = $this->config->item('pas_micado');
            $from_stock_only = $this->config->item('from_stock_only_micado');

            $result = array();

            $url_micado = 'http://www.mikado-parts.ru/ws1/service.asmx/Code_Search?Search_Code=' . $article . '&ClientID=' . $login . '&Password=' . $password . '&FromStockOnly=' . $from_stock_only;

            $ch_micado = curl_init();

            $chs[] = array(
                'ch' => $ch_micado,
                'provider' => 'micado',
                'article' => $article,
            );

            curl_setopt($ch_micado, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch_micado, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch_micado, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch_micado, CURLOPT_TIMEOUT, 25);
            curl_setopt($ch_micado, CURLOPT_URL, $url_micado);
            curl_multi_add_handle($mh, $ch_micado);
        }

        $running = null;

        do {
            curl_multi_exec($mh, $running);
        } while ($running > 0);

        foreach ($chs as $ch) {
            if ($ch['provider'] == 'berg') {
                $data_berg = curl_multi_getcontent($ch['ch']);

                if ($data_berg !== false) {
                    $result_berg = $this->standard_view_data_berg($data_berg, $ch['article'], $ch['brand']);
                } else {
                    $result_berg = array();
                }

                $result = array_merge($result, $result_berg);
            }

            if ($ch['provider'] == 'micado') {
                $data_api_micado = curl_multi_getcontent($ch['ch']);

                if ($data_api_micado !== false) {
                    $result_api_micado = $this->standard_view_data_micado($data_api_micado);
                } else {
                    $result_api_micado = array();
                }
            }

            curl_multi_remove_handle($mh, $ch['ch']);
            curl_close($ch['ch']);
        }

        curl_multi_close($mh);

        if (isset($result_api_micado) && !empty($result_api_micado)) {
            foreach ($brands_search as $brand) {
                if ($brand['provider'] == 'micado') {
                    $slice_brand_from_data_api_micado = $this->get_slice_brand_from_data_api_micado($result_api_micado, $article, $brand['brand']);

                    $result = array_merge($result, $slice_brand_from_data_api_micado);
                }
            }
        }

        return $result;
    }

    /**
     * Функция получает данные от avtoto
     * @param string $article_get
     * @param array $equal_brands
     * @return array $result
     */
    public function get_data_api_avtoto($article_get, $equal_brands) {
		ini_set('default_socket_timeout', '300');

        $login_params = array(
            'user_id' => $this->config->item('api_id_avtoto'),
            'user_login' => $this->config->item('api_login_avtoto'),
            'user_password' => $this->config->item('api_pass_avtoto'),
        );

        $search_analogs = 'on';
        $limit = 1000;
        $result = array();
        $brands_array = array();

        $this->load->library('avtoto_parts', $login_params);

        $data_avtoto = $this->avtoto_parts->get_parts($article_get, $search_analogs, $limit);
        $errors = $this->avtoto_parts->get_errors();

        if (empty($errors) && isset($data_avtoto['Parts']) && !empty($data_avtoto['Parts'])) {
            $data_provider = $this->get_provider_by_name('Автото');
            $rating_provider = isset($data_provider['rating']) ? $data_provider['rating'] : 0;

            $ids_types_array = $this->get_ids_for_warehouses_types_provider('Автото');

            if (array_key_exists(1, $ids_types_array)) {
                $id_for_type_1 = $ids_types_array[1];
            } else {
                $id_for_type_1 = NULL;
            }

            if (array_key_exists(2, $ids_types_array)) {
                $id_for_type_2 = $ids_types_array[2];
            } else {
                $id_for_type_2 = NULL;
            }

            if (array_key_exists(3, $ids_types_array)) {
                $id_for_type_3 = $ids_types_array[3];
            } else {
                $id_for_type_3 = NULL;
            }

            foreach ($data_avtoto['Parts'] as $avtoto_value) {
                if (isset($avtoto_value['Manuf']) && !empty($avtoto_value['Manuf'])) {
                    $article = leave_eng_letters_numbers(mb_strtolower($avtoto_value['Code']));
                    $brand = mb_strtolower($avtoto_value['Manuf']);
                    $price = standard_price($avtoto_value['Price']);
                    $key_exist_detail = '';
                    $delivery_period = isset($avtoto_value['Delivery']) ? $avtoto_value['Delivery'] : '';

                    if (isset($avtoto_value['BackPercent'])
                            && ($avtoto_value['BackPercent'] != '-1')
                                    && (($delivery_period == '0') or ($delivery_period == '1') or ($delivery_period == '0-1'))) {
                        $warehouse_type = 1;
                        $id_warehouse_type = $id_for_type_1;
                    } elseif (isset($avtoto_value['BackPercent'])
                            && ($avtoto_value['BackPercent'] != '-1')) {
                        $warehouse_type = 2;
                        $id_warehouse_type = $id_for_type_2;
                    } else {
                        $warehouse_type = 3;
                        $id_warehouse_type = $id_for_type_3;
                    }

                    if (!empty($result)) {
                        foreach ($result as $res_key => $res_value) {
                            if (($res_value['article'] == $article) and ( $res_value['brand'] == $brand)) {
                                $key_exist_detail = $res_key;
                            }
                        }
                    }

                    if ($key_exist_detail === '') {
                        $data = array(
                            'article' => $article,
                            'brand' => $brand,
                            'name' => (isset($avtoto_value['Name']) and ( !empty($avtoto_value['Name']))) ? $avtoto_value['Name'] : '',
                            'offers' => array(array(
                                    'price' => $price,
                                    'description' => isset($avtoto_value['Name']) ? $avtoto_value['Name'] : '',
                                    'warehouse' => $avtoto_value['Storage'],
                                    'warehouse_type' => $warehouse_type,
                                    'quantity' => ($avtoto_value['MaxCount'] == -1) ? 'много, или неизвестно' : $avtoto_value['MaxCount'],
                                    'min_part' => $avtoto_value['BaseCount'],
                                    'delivery_period' => $delivery_period,
                                    'id_warehouse_type' => $id_warehouse_type,
                                )),
                        );

                        array_push($result, $data);
                    } else {
                        $result[$key_exist_detail]['offers'][] = array(
                            'price' => $price,
                            'description' => isset($avtoto_value['Name']) ? $avtoto_value['Name'] : '',
                            'warehouse' => $avtoto_value['Storage'],
                            'warehouse_type' => $warehouse_type,
                            'quantity' => ($avtoto_value['MaxCount'] == -1) ? 'много, или неизвестно' : $avtoto_value['MaxCount'],
                            'min_part' => $avtoto_value['BaseCount'],
                            'delivery_period' => $avtoto_value['Delivery'],
                            'id_warehouse_type' => $id_warehouse_type,
                        );
                    }
                }
            }

            if (!empty($equal_brands)) {
                foreach ($equal_brands as $brand) {
                    $brands_array[] = $brand['brand'];
                }
            }

            foreach ($result as $key_detail => $res) {
                if (($res['article'] == $article_get) and in_array($res['brand'], $brands_array)) {
                    $result[$key_detail]['analog'] = 0;
                    $result[$key_detail]['rating'] = 'original';
                } else {
                    $result[$key_detail]['analog'] = 1;
                    $result[$key_detail]['rating'] = array('avtoto' => $rating_provider);
                }
            }
        }

        return $result;
    }

    /**
     * Функция преобразует данные от berg в стандартный вид для данной программы
     * @param array $data_berg
     * @param string $article_get
     * @param string $brand_get
     * @return array $result
     */
    public function standard_view_data_berg($data_berg, $article_get, $brand_get) {
        $result = array();
        $data_berg = json_decode($data_berg, true);

        if (!empty($data_berg) and isset($data_berg['resources']) and ! empty($data_berg['resources'])) {
            $data_provider = $this->get_provider_by_name('Berg');
            $rating_provider = isset($data_provider['rating']) ? $data_provider['rating'] : 0;
            $warehouse_type_array = array();

            $ids_types_array = $this->get_ids_for_warehouses_types_provider('Berg');

            if (array_key_exists(2, $ids_types_array)) {
                $id_for_type_2 = $ids_types_array[2];
            } else {
                $id_for_type_2 = NULL;
            }

            if (array_key_exists(3, $ids_types_array)) {
                $id_for_type_3 = $ids_types_array[3];
            } else {
                $id_for_type_3 = NULL;
            }

            foreach ($data_berg['resources'] as $data) {
                if (isset($data['brand']['name']) && !empty($data['brand']['name'])) {
                    $article = leave_eng_letters_numbers(mb_strtolower($data['article']));
                    $brand = mb_strtolower($data['brand']['name']);

                    $final_data = array(
                        'article' => $article,
                        'brand' => $brand,
                        'name' => (isset($data['name']) and ( !empty($data['name']))) ? $data['name'] : '',
                    );

                    if (isset($data['offers']) and ! empty($data['offers'])) {
                        foreach ($data['offers'] as $offer) {
                            $price = standard_price($offer['price']);

                            if (($offer['warehouse']['type'] == 1) or ( $offer['warehouse']['type'] == 2)) {
                                $warehouse_type = 2;
                                $id_warehouse_type = $id_for_type_2;
                            }

                            if (($offer['warehouse']['type'] == 3)) {
                                $warehouse_type = 3;
                                $id_warehouse_type = $id_for_type_3;
                            }

                            $final_data['offers'][] = array(
                                'price' => $price,
                                'description' => isset($data['name']) ? $data['name'] : '',
                                'quantity' => $offer['quantity'],
                                'min_part' => $offer['multiplication_factor'],
                                'warehouse_type' => $warehouse_type,
                                'warehouse' => $offer['warehouse']['name'],
                                'delivery_period' => $offer['assured_period'],
                                'id_warehouse_type' => $id_warehouse_type,
                            );

                            if (!in_array($offer['warehouse']['type'], $warehouse_type_array)) {
                                $warehouse_type_array[] = $offer['warehouse']['type'];
                            }
                        }
                    }

                    if (($article == $article_get) and ( $brand == $brand_get)) {
                        $final_data['analog'] = 0;
                        $final_data['rating'] = 'original';
                    } else {
                        $final_data['analog'] = 1;
                        $final_data['rating'] = array('berg' => $rating_provider);
                    }

                    $result[] = $final_data;
                }
            }
        }

        return $result;
    }

    /**
     * Функция преобразует данные от tiss в стандартный вид для данной программы
     * @param array $data_tiss
     * @param string $article_get
     * @param string $brand_get
     * @return array $result
     */
    public function standard_view_data_tiss($data_tiss, $article_get, $brand_get) {
        $result = array();

        $data_tiss = json_decode($data_tiss, true);

        if (is_array($data_tiss) and ! empty($data_tiss) and ! array_key_exists('Message', $data_tiss)) {
            $data_provider = $this->get_provider_by_name('Tiss');
            $rating_provider = isset($data_provider['rating']) ? $data_provider['rating'] : 0;

            $ids_types_array = $this->get_ids_for_warehouses_types_provider('Tiss');

            if (array_key_exists(1, $ids_types_array)) {
                $id_for_type_1 = $ids_types_array[1];
            } else {
                $id_for_type_1 = NULL;
            }

            if (array_key_exists(2, $ids_types_array)) {
                $id_for_type_2 = $ids_types_array[2];
            } else {
                $id_for_type_2 = NULL;
            }

            if (array_key_exists(3, $ids_types_array)) {
                $id_for_type_3 = $ids_types_array[3];
            } else {
                $id_for_type_3 = NULL;
            }

            foreach ($data_tiss as $data) {
                if (isset($data['brand']) && !empty($data['brand'])) {
                    $article = leave_eng_letters_numbers(mb_strtolower($data['article']));
                    $brand = mb_strtolower($data['brand']);

                    $final_data = array(
                        'article' => $article,
                        'brand' => $brand,
                        'name' => (isset($data['article_name']) and ( !empty($data['article_name']))) ? $data['article_name'] : '',
                    );

                    if (isset($data['warehouse_offers']) and ! empty($data['warehouse_offers'])) {
                        foreach ($data['warehouse_offers'] as $offer) {
                            $price = standard_price($offer['price']);

                            if ($offer['is_main_warehouse'] == 1) {
                                $warehouse_type = 1;
                                $id_warehouse_type = $id_for_type_1;
                            } elseif (($offer['is_main_warehouse'] == 0) and ( $offer['warehouse_name'] == 'склад группы компаний')) {
                                $warehouse_type = 2;
                                $id_warehouse_type = $id_for_type_2;
                            } else {
                                $warehouse_type = 3;
                                $id_warehouse_type = $id_for_type_3;
                            }

                            $final_data['offers'][] = array(
                                'price' => $price,
                                'description' => isset($data['article_name']) ? $data['article_name'] : '',
                                'quantity' => $offer['quantity'],
                                'min_part' => $offer['min_part'],
                                'warehouse_type' => $warehouse_type,
                                'warehouse' => $offer['warehouse_name'],
                                'delivery_period' => $offer['delivery_period'],
                                'id_warehouse_type' => $id_warehouse_type,
                            );
                        }
                    }

                    if (( $article == $article_get) and ( $brand == $brand_get)) {
                        $final_data['analog'] = 0;
                        $final_data['rating'] = 'original';
                    } else {
                        $final_data['analog'] = 1;
                        $final_data['rating'] = array('tiss' => $rating_provider);
                    }

                    $result[] = $final_data;
                }
            }
        }

        return $result;
    }

    /**
     * Функция преобразует данные от adeo_pro в стандартный вид для данной программы
     * @param array $adeo_pro_data
     * @param string $article_get
     * @param string $brand_get
     * @return array $result
     */
    public function standard_view_data_adeo_pro($adeo_pro_data, $article_get, $brand_get) {
        $result = array();

        libxml_use_internal_errors(true);
        $adeo_pro_xml = simplexml_load_string($adeo_pro_data);

        if (!$adeo_pro_xml) {
            libxml_clear_errors();

            return array();
        }

        $adeo_pro_json = json_encode($adeo_pro_xml);
        $adeo_pro_data = json_decode($adeo_pro_json, TRUE);

        if (isset($adeo_pro_data['detail']) and ! empty($adeo_pro_data['detail'])) {
            $diff = count($adeo_pro_data['detail']) - count($adeo_pro_data['detail'], COUNT_RECURSIVE);

            if ($diff == 0) {
                $data = $adeo_pro_data['detail'];
                $adeo_pro_data['detail'] = array();
                $adeo_pro_data['detail'][] = $data;
            }

            $data_provider = $this->get_provider_by_name('Адео');
            $rating_provider = isset($data_provider['rating']) ? $data_provider['rating'] : 0;

            $ids_types_array = $this->get_ids_for_warehouses_types_provider('Адео');

            if (array_key_exists(3, $ids_types_array)) {
                $id_for_type_3 = $ids_types_array[3];
            } else {
                $id_for_type_3 = NULL;
            }

            foreach ($adeo_pro_data['detail'] as $value) {
                $article = leave_eng_letters_numbers(mb_strtolower($value['code']));
                $brand = mb_strtolower($value['producer']);
                $price = standard_price($value['price']);
                $warehouse_type = 3;
                $id_warehouse_type = $id_for_type_3;
                $key_exist_detail = '';

                if (!empty($result)) {
                    foreach ($result as $res_key => $res_value) {
                        if (($res_value['article'] == $article) and ( $res_value['brand'] == $brand)) {
                            $key_exist_detail = $res_key;
                        }
                    }
                }

                if ($key_exist_detail === '') {
                    $data = array(
                        'article' => $article,
                        'brand' => $brand,
                        'name' => (isset($value['caption']) and ( !empty($value['caption']))) ? $value['caption'] : '',
                        'offers' => array(array(
                                'price' => $price,
                                'description' => isset($value['caption']) ? $value['caption'] : '',
                                'warehouse' => $value['stock'] . $value['RegionName'],
                                'warehouse_type' => $warehouse_type,
                                'quantity' => $value['rest'],
                                'min_part' => $value['minOrderCount'],
                                'delivery_period' => (!empty($value['deliveryDisplay']) and ! is_array($value['deliveryDisplay'])) ? $value['deliveryDisplay'] : '',
                                'id_warehouse_type' => $id_warehouse_type,
                            )),
                    );

                    if (($data['article'] == $article_get) and ( $data['brand'] == $brand_get)) {
                        $data['analog'] = 0;
                        $data['rating'] = 'original';
                    } else {
                        $data['analog'] = 1;
                        $data['rating'] = array('adeo_pro' => $rating_provider);
                    }

                    array_push($result, $data);
                } else {
                    $result[$key_exist_detail]['offers'][] = array(
                        'price' => $price,
                        'description' => isset($value['caption']) ? $value['caption'] : '',
                        'warehouse' => $value['stock'] . $value['RegionName'],
                        'warehouse_type' => $warehouse_type,
                        'quantity' => $value['rest'],
                        'min_part' => $value['minOrderCount'],
                        'delivery_period' => (!empty($value['deliveryDisplay']) and ! is_array($value['deliveryDisplay'])) ? $value['deliveryDisplay'] : '',
                        'id_warehouse_type' => $id_warehouse_type,
                    );
                }
            }
        }

        return $result;
    }

    /**
     * Функция преобразует данные от abs в стандартный вид для данной программы
     * @param array $data_abs
     * @param string $article_get
     * @param string $brand_get
     * @return array $result
     */
    public function standard_view_data_abs($data_abs, $article_get, $brand_get) {
        $result = array();
        $data_abs = json_decode($data_abs, true);
        $array_abs_warehouse = array(
            'ABS Краснодар',
            'ABS Ростов',
            'ABS Ставрополь',
            'ABS Столица',
        );

        if (isset($data_abs['data']) and ! empty($data_abs['data'])) {
            $data_provider = $this->get_provider_by_name('ABS-auto');
            $rating_provider = isset($data_provider['rating']) ? $data_provider['rating'] : 0;

            $ids_types_array = $this->get_ids_for_warehouses_types_provider('ABS-auto');

            if (array_key_exists(2, $ids_types_array)) {
                $id_for_type_2 = $ids_types_array[2];
            } else {
                $id_for_type_2 = NULL;
            }

            if (array_key_exists(3, $ids_types_array)) {
                $id_for_type_3 = $ids_types_array[3];
            } else {
                $id_for_type_3 = NULL;
            }

            foreach ($data_abs['data'] as $abs_value) {
                if (isset($abs_value['brand']) && !empty($abs_value['brand'])) {
                    $article = leave_eng_letters_numbers(mb_strtolower($abs_value['article']));
                    $brand = mb_strtolower($abs_value['brand']);
                    $price = $abs_value['price'];
                    $key_exist_detail = '';

                    if (!empty($result)) {
                        foreach ($result as $res_key => $res_value) {
                            if (($res_value['article'] == $article) and ( $res_value['brand'] == $brand)) {
                                $key_exist_detail = $res_key;
                            }
                        }
                    }

                    if ($key_exist_detail === '') {
                        if (in_array($abs_value['warehouse_name'], $array_abs_warehouse)) {
                            $id_warehouse_type = $id_for_type_2;
                        } else {
                            $id_warehouse_type = $id_for_type_3;
                        }

                        $data = array(
                            'article' => $article,
                            'brand' => $brand,
                            'name' => (isset($abs_value['product_name']) and ( !empty($abs_value['product_name']))) ? $abs_value['product_name'] : '',
                            'offers' => array(array(
                                    'price' => $price,
                                    'description' => isset($abs_value['product_name']) ? $abs_value['product_name'] : '',
                                    'warehouse' => $abs_value['warehouse_name'],
                                    'warehouse_type' => in_array($abs_value['warehouse_name'], $array_abs_warehouse) ? 2 : 3,
                                    'quantity' => $abs_value['quantity'],
                                    'min_part' => $abs_value['mult_sale'],
                                    'delivery_period' => $abs_value['delivery_duration'],
                                    'id_warehouse_type' => $id_warehouse_type,
                                )),
                        );

                        if (($data['article'] == $article_get) and ( $data['brand'] == $brand_get)) {
                            $data['analog'] = 0;
                            $data['rating'] = 'original';
                        } else {
                            $data['analog'] = 1;
                            $data['rating'] = array('abs' => $rating_provider);
                        }

                        array_push($result, $data);
                    } else {
                        if (in_array($abs_value['warehouse_name'], $array_abs_warehouse)) {
                            $id_warehouse_type = $id_for_type_2;
                        } else {
                            $id_warehouse_type = $id_for_type_3;
                        }

                        $result[$key_exist_detail]['offers'][] = array(
                            'price' => $price,
                            'description' => isset($abs_value['product_name']) ? $abs_value['product_name'] : '',
                            'warehouse' => $abs_value['warehouse_name'],
                            'warehouse_type' => in_array($abs_value['warehouse_name'], $array_abs_warehouse) ? 2 : 3,
                            'quantity' => $abs_value['quantity'],
                            'min_part' => $abs_value['mult_sale'],
                            'delivery_period' => $abs_value['delivery_duration'],
                            'id_warehouse_type' => $id_warehouse_type,
                        );
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция преобразует данные от micado в стандартный вид для данной программы
     * @param array $micado_data
     * @return array
     */
    public function standard_view_data_micado($micado_data) {
        libxml_use_internal_errors(true);
        $micado_xml = simplexml_load_string($micado_data);

        if (!$micado_xml) {
            libxml_clear_errors();

            return array();
        }

        $micado_json = json_encode($micado_xml);
        $micado_data = json_decode($micado_json, TRUE);

        if (!empty($micado_data) and isset($micado_data['List']['Code_List_Row']['Source'])) {
            $data = $micado_data['List']['Code_List_Row'];
            $micado_data['List']['Code_List_Row'] = array();
            $micado_data['List']['Code_List_Row'][] = $data;
        }

        if (!empty($micado_data) and isset($micado_data['List']['Code_List_Row'])
                and ! empty($micado_data['List']['Code_List_Row'])) {
            return $micado_data['List']['Code_List_Row'];
        } else {
            return array();
        }
    }

    /**
     * Функция преобразует данные от armtek в стандартный вид для данной программы
     * @param array $data_armtek
     * @return array
     */
    public function standard_view_data_armtek($data_armtek, $article_get, $brand_get) {
        $result = array();
        $data_armtek = json_decode($data_armtek, true);

        $data_provider = $this->get_provider_by_name('Армтек');
        $rating_provider = isset($data_provider['rating']) ? $data_provider['rating'] : 0;

        $ids_types_array = $this->get_ids_for_warehouses_types_provider('Армтек');

        if (array_key_exists(1, $ids_types_array)) {
            $id_for_type_1 = $ids_types_array[1];
        } else {
            $id_for_type_1 = NULL;
        }

        if (array_key_exists(2, $ids_types_array)) {
            $id_for_type_2 = $ids_types_array[2];
        } else {
            $id_for_type_2 = NULL;
        }

        if (!empty($data_armtek) and isset($data_armtek['RESP']) and ! empty($data_armtek['RESP'])
                and ! isset($data_armtek['RESP']['MSG']) and ! isset($data_armtek['RESP']['ERROR'])) {
            $resp = $data_armtek['RESP'];

            $diff = count($resp) - count($resp, COUNT_RECURSIVE);

            if ($diff == 0) {
                $data = $resp;
                $resp = array();
                $resp[] = $data;
            }

            foreach ($resp as $value) {
                $key_exist_detail = '';
                $price = standard_price($value['PRICE']);
                $dlvdt = strtotime($value['DLVDT']);
                $dlvdt = new DateTime(date('Y-m-d', $dlvdt));
                $date_now = new DateTime(date('Y-m-d'));

                $delivery_period = $dlvdt->diff($date_now);
                $delivery_period = $delivery_period->days;

                $data = array(
                    'article' => leave_eng_letters_numbers(mb_strtolower($value['PIN'])),
                    'brand' => mb_strtolower($value['BRAND']),
                    'name' => (isset($value['NAME']) and ( !empty($value['NAME'])) and ! is_array($value['NAME'])) ? $value['NAME'] : '',
                    'offers' => array(),
                );

                if (($data['article'] == $article_get) && ($data['brand'] == $brand_get)) {
                    $data['analog'] = 0;
                    $data['rating'] = 'original';
                } else {
                    $data['analog'] = 1;
                    $data['rating'] = array('armtek' => $rating_provider);
                }

                if ($delivery_period <= 3) {
                    $warehouse_type = 1;
                    $id_warehouse_type = $id_for_type_1;
                } else {
                    $warehouse_type = 2;
                    $id_warehouse_type = $id_for_type_2;
                }

                $offer = array(
                    'price' => $price,
                    'description' => (isset($value['NAME']) and ( !empty($value['NAME'])) and ! is_array($value['NAME'])) ? $value['NAME'] : '',
                    'warehouse' => $value['KEYZAK'],
                    'warehouse_type' => $warehouse_type,
                    'quantity' => $value['RVALUE'],
                    'min_part' => $value['RDPRF'],
                    'delivery_period' => $delivery_period,
                    'id_warehouse_type' => $id_warehouse_type,
                );

                $data['offers'][] = $offer;

                if (!empty($result)) {
                    foreach ($result as $res_key => $res_value) {
                        if (($res_value['article'] == $data['article'])
                                and ( $res_value['brand'] == $data['brand'])) {
                            $key_exist_detail = $res_key;
                        }
                    }
                }

                if ($key_exist_detail === '') {
                    array_push($result, $data);
                } else {
                    foreach ($data['offers'] as $offer) {
                        if (!in_array($offer, $result[$key_exist_detail]['offers'])) {
                            $result[$key_exist_detail]['offers'][] = $offer;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция производит поиск деталей по данным из прайс-листов
     * @param array $data_from_api
     * @return array $result
     */
    public function search_details_from_price_list($data_from_api) {
        $result = array();
        $articles_api_array = array();

        foreach ($data_from_api as $value_api) {
            if (!in_array($value_api['article'], $articles_api_array)) {
                $articles_api_array[] = $value_api['article'];
            }
        }

        $this->db->select('content_price_list.article,
                           content_price_list.brand,
                           content_price_list.name,
                           content_price_list.price,
                           content_price_list.quantity,
                           content_price_list.min_part,
                           content_price_list.delivery_period,
                           provider.name as provider_name,
                           price_lists.custom_del_period,
                           type_warehouse.type,
                           provider.rating,
                           type_warehouse.id as id_warehouse_type');
        $this->db->from('content_price_list');
        $this->db->join('price_lists', 'price_lists.id = content_price_list.id_price_list');
        $this->db->join('type_warehouse', 'type_warehouse.id = price_lists.id_type_warehouse');
        $this->db->join('provider', 'provider.id = type_warehouse.id_provider');
        $this->db->where_in('content_price_list.article', $articles_api_array);
        $this->db->where('price_lists.is_active', 1);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        if (!empty($query->result_array())) {
            foreach ($query->result_array() as $value) {
                $price = standard_price($value['price']);
                $delivery_period = '-';

                if (!empty($value['delivery_period'])) {
                    $delivery_period = $value['delivery_period'];
                } elseif (!empty($value['custom_del_period'])) {
                    $delivery_period = $value['custom_del_period'];
                }

                $result[] = array(
                    'article' => mb_strtolower($value['article']),
                    'brand' => mb_strtolower($value['brand']),
                    'name' => $value['name'],
                    'price' => $price,
                    'quantity' => $value['quantity'],
                    'min_part' => !empty($value['min_part']) ? $value['min_part'] : 1,
                    'delivery_period' => $delivery_period,
                    'provider' => $value['provider_name'],
                    'warehouse_type' => $value['type'],
                    'id_warehouse_type' => $value['id_warehouse_type'],
                );
            }
        }

        return $result;
    }

    /**
     * Функция объединяет данные от api и данные от прайс-листов
     * @param array $api_data
     * @param array $price_lists_data
     * @param array $groups_brands
     * @return array $result
     */
    public function merge_price_data_and_api_data($api_data, $price_lists_data, $groups_brands) {
        $result = $api_data;

        foreach ($api_data as $key => $api_value) {
            $brands_search = array();

            foreach ($groups_brands as $group_brands) {
                if ($group_brands['group_name'] == $api_value['brand']) {
                    foreach ($group_brands['brands'] as $group_brand) {
                        if (!in_array($group_brand, $brands_search)) {
                            $brands_search[] = $group_brand;
                        }
                    }
                }
            }

            if (empty($brands_search)) {
                foreach ($price_lists_data as $price_list_value) {
                    if (($price_list_value['article'] == $api_value['article'])
                            and ( $price_list_value['brand'] == $api_value['brand'])) {
                        if (mb_strlen($api_value['name']) < mb_strlen($price_list_value['name'])) {
                            $result[$key]['name'] = $price_list_value['name'];
                        }

                        $offer = array(
                            'price' => $price_list_value['price'],
                            'description' => $price_list_value['name'],
                            'warehouse' => '-',
                            'warehouse_type' => $price_list_value['warehouse_type'],
                            'id_warehouse_type' => $price_list_value['id_warehouse_type'],
                            'quantity' => $price_list_value['quantity'],
                            'min_part' => $price_list_value['min_part'],
                            'delivery_period' => $price_list_value['delivery_period'],
                        );

                        if (!in_array($offer, $result[$key]['offers'])) {
                            $result[$key]['offers'][] = $offer;
                        }
                    }
                }
            } else {
                foreach ($brands_search as $brand_search) {
                    foreach ($price_lists_data as $price_list_value) {
                        if (($price_list_value['article'] == $api_value['article'])
                                and ( $price_list_value['brand'] == $brand_search)) {
                            if (mb_strlen($api_value['name']) < mb_strlen($price_list_value['name'])) {
                                $result[$key]['name'] = $price_list_value['name'];
                            }

                            $offer = array(
                                'price' => $price_list_value['price'],
                                'description' => $price_list_value['name'],
                                'warehouse' => '-',
                                'warehouse_type' => $price_list_value['warehouse_type'],
                                'id_warehouse_type' => $price_list_value['id_warehouse_type'],
                                'quantity' => $price_list_value['quantity'],
                                'min_part' => $price_list_value['min_part'],
                                'delivery_period' => $price_list_value['delivery_period'],
                            );

                            if (!in_array($offer, $result[$key]['offers'])) {
                                $result[$key]['offers'][] = $offer;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция получает список брендов от поставщиков и записывает их в БД
     * @param string $article
     * @return boolean
     */
    public function update_data_list_brands_searchs($article) {
        $list_brands = $this->get_api_brands_by_article($article);

        if (empty($list_brands)) {
            $id_array = $this->check_exist_article($article);

            if (!empty($id_array)) {
                foreach ($id_array as $value) {
                    $this->del_cross_for_detail($value['id']);
                    $this->del_detail_considering_cross($value['id']);
                }
            }

            $this->del_inf_brands_search_by_article($article);

            return TRUE;
        } else {
            if ($this->save_data_list_brands_searchs($article, $list_brands)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    /**
     * Функция удаляет из БД данные, по которым будет вестись поиск (артикул и бренды).
     * @param string $article
     */
    public function del_inf_brands_search_by_article($article) {
        $id_article_search = $this->check_exist_article_search($article);

        if ($id_article_search) {
            if ($this->db->delete('articles_search', array('id' => $id_article_search))) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * Функция получает список брендов которые можно объединить (для страницы найденных брендов)
     * @param string $article
     * @return array $result
     */
    public function get_list_merge_brands($article) {
        $result = array();

        $brands = $this->get_brands_search_by_article($article);

        if (!empty($brands)) {
            $groups_brands = $this->get_groups_brands_for_brands($brands);

            if(!empty($groups_brands)) {
                foreach ($groups_brands as $key_group => $value) {
                    $data = array();

                    $data['key_group'] = $key_group;
                    $data['name'] = $value['group_name'];

                    if(count($value['brands']) > 1) {
                        $data['type'] = 'группа';
                        $data['type_for_link'] = 'group';
                    } else {
                        $data['type'] = 'одиночный бренд';
                        $data['type_for_link'] = 'item';
                    }

                    $result[] = $data;
                }
            }
        }

        return $result;
    }

    /**
     * Функция получает список брендов которые можно объединить (для страницы найденных деталей)
     * @param string $article
     * @param string $key_group
     * @param string $field_sort
     * @param string $type_sort
     * @return array $result
     */
    public function get_list_merge_brands_for_list_details($article, $key_group, $field_sort, $type_sort) {
        $result = array();
        $data_array = array();

        $brand = $this->get_group_name_by_key_group($key_group);

        $id = $this->get_id_by_article_and_brand($article, $brand);

        if (!empty($brand) && !empty($id)) {
            $this->db->select('auto_parts.id,
                           auto_parts.name,
                           brands.id as brand_id,
                           brands.name as brand,
                           articles.article,
                           articles.is_complete');
            $this->db->from('auto_parts');
            $this->db->join('articles', 'articles.id = auto_parts.id_article');
            $this->db->join('brands', 'brands.id = articles.id_brand');
            $this->db->where('articles.id', $id);

            $query = $this->db->get();

            if (!$query) {
                return false;
            }

            if (!empty($query->row_array())) {
                $data = $query->row_array();

                $data['analog'] = 0;

                $data_array[] = $data;
            }

            if (isset($data['is_complete'])) {
                $is_complete = $data['is_complete'];
            } else {
                $is_complete = 1;
            }

            $this->db->select('auto_parts.id,
                           auto_parts.name,
                           brands.id as brand_id,
                           brands.name as brand,
                           articles.article');
            $this->db->from('detail_cross');
            $this->db->join('articles', 'articles.id = detail_cross.id_cross');
            $this->db->join('brands', 'brands.id = articles.id_brand');
            $this->db->join('auto_parts', 'articles.id = auto_parts.id_article');
            $this->db->where('detail_cross.id_detail', $id);

            $query = $this->db->get();

            if (!$query) {
                return false;
            }

            if (!empty($query->result_array())) {
                foreach ($query->result_array() as $value) {
                    $value['analog'] = 1;

                    $data_array[] = $value;
                }
            }

            if (!empty($data_array)) {
                foreach ($data_array as $value1) {
                    $count = 0;

                    foreach ($data_array as $value2) {
                        if ($value1['article'] == $value2['article']) {
                            $count++;
                        }
                    }

                    if ($count > 1) {
                        $result[] = $value1;
                    }
                }
            }

            if (!empty($result)) {
                foreach ($result as $key_res => $res) {
                    $equal_brands = $this->get_equal_brands_by_group_name($res['brand']);

                    if (count($equal_brands) > 1) {
                        $result[$key_res]['group_name'] = $res['brand'];
                        $result[$key_res]['type'] = 'группа';
                        $result[$key_res]['type_for_link'] = 'group';
                        $result[$key_res]['key_group'] = $equal_brands[0]['key_group'];
                    } else {
                        $result[$key_res]['type'] = 'одиночный бренд';
                        $result[$key_res]['type_for_link'] = 'item';
                    }
                }
            }

            if ($field_sort != '') {
                if (!empty($result)) {
                    $sort_array = array();

                    foreach ($result as $value) {
                        array_push($sort_array, $value[$field_sort]);
                    }

                    if ($type_sort == 'desc') {
                        array_multisort($sort_array, SORT_DESC, $result);
                    } else {
                        array_multisort($sort_array, SORT_ASC, $result);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция получает список брендов из таблицы brands_search по артикулу
     * @param string $article
     * @return array
     */
    public function get_brands_search_by_article($article) {
        $result = array();

        $this->db->select('*');
        $this->db->from('brands_search');
        $this->db->join('articles_search', 'articles_search.id = brands_search.id_article');
        $this->db->where('articles_search.article', $article);
        $this->db->order_by('brands_search.brand', 'asc');

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $res = $query->result_array();

        if(!empty($res)) {
            foreach($res as $value) {
                if(!in_array($value['brand'], $result)) {
                    $result[] = $value['brand'];
                }
            }
        }

        return $result;
    }

    /**
     * Функция получает список брендов из таблицы brands_search по артикулу и эквивалентным брендам
     * @param string $article
     * @param array $equal_brands
     * @return array
     */
    public function get_brands_search_by_article_equal_brands($article, $equal_brands) {
        if (!empty($equal_brands)) {
            $brands_array = array();

            foreach ($equal_brands as $data) {
                $brands_array[] = $data['brand'];
            }

            $this->db->select('*');
            $this->db->from('brands_search');
            $this->db->join('articles_search', 'articles_search.id = brands_search.id_article');
            $this->db->where('articles_search.article', $article);
            $this->db->where_in('brands_search.brand', $brands_array);

            $query = $this->db->get();

            if (!$query) {
                return false;
            }

            return $query->result_array();
        } else {
            return '';
        }
    }

    /**
     * Функция получает список эквивалентных брендов по ключу группы
     * @param string $key_group
     * @return array
     */
    public function get_equal_brands_by_key_group($key_group) {
        $this->db->select('*');
        $this->db->from('groups_brands');
        $this->db->where('key_group', $key_group);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция получает список брендов из таблицы brands_search по артикулу
     * @param int $id_article
     * @return array
     */
    public function get_brands_search_by_id_article($id_article) {
        $this->db->select('*');
        $this->db->from('brands_search');
        $this->db->where('id_article', $id_article);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция получает все данные из таблицы groups_brands
     * @return array
     */
    public function get_groups_brands() {
        $this->db->select('*');
        $this->db->from('groups_brands');

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция получает список брендов, а выдает список групп в которые данные бренды входят
     * @param array $brands
     * @return array
     */
    public function get_groups_brands_for_brands($brands) {
        $result = array();

        $this->db->select('key_group');
        $this->db->from('groups_brands');
        $this->db->where_in('brand', $brands);

        $sql = $this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('groups_brands');
        $this->db->where('`key_group` IN (' . $sql . ')');

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $res = $query->result_array();

        if(!empty($res)) {
            foreach ($res as $key => $value) {
                $result[$value['key_group']]['group_name'] = $value['group_name'];
                $result[$value['key_group']]['brands'][] = $value['brand'];
            }
        }

        return $result;
    }

    /**
     * Функция получает список эквивалентных брендов по группе
     * @param string $group_name
     * @return array
     */
    public function get_equal_brands_by_group_name($group_name) {
        $this->db->select('*');
        $this->db->from('groups_brands');
        $this->db->where('group_name', $group_name);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция сохраняет данные объединяемых брендов
     * @param array $merge_brands_array
     * @param array $final_result
     * @param string $name_group
     * @param string $key_group
     * @param array $description_brand
     * @return boolean
     */
    public function save_merge_brands($merge_brands_array, $final_result, $name_group, $key_group, $description_brand) {
        $this->db->trans_start();
        foreach ($merge_brands_array as $merge_brand) {
            $this->db->delete('groups_brands', array('group_name' => $merge_brand));
        }

        foreach ($final_result as $value) {
            $data = array(
                'brand' => $value,
                'group_name' => $name_group,
                'key_group' => $key_group,
            );

            $this->db->insert('groups_brands', $data);
        }

        $this->db->empty_table('articles');
        $this->db->empty_table('auto_parts');
        $this->db->empty_table('detail_cross');
        $this->db->empty_table('offers');

        $this->db->where_in('name', $merge_brands_array);
        $this->db->delete('brands');

        $data = array(
            'name' => $name_group,
            'description' => isset($description_brand['description']) ? $description_brand['description'] : NULL,
            'link' => isset($description_brand['link']) ? $description_brand['link'] : NULL,
            'country' => isset($description_brand['country']) ? $description_brand['country'] : NULL,
        );

        $this->db->insert('brands', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        } else {
            $this->db->trans_commit();

            return TRUE;
        }
    }

    /**
     * Функция проверяет есть ли в таблице groups_brands указанный ключ
     * @param string $key_group
     * @return array
     */
    public function check_exist_key_equal_brands($key_group) {
        $this->db->select('id');
        $this->db->from('groups_brands');
        $this->db->where('key_group', $key_group);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция создает конечный массив объединенных брендов, эти данные далее будут записаны в БД
     * @param array $merge_brands_array
     * @return array $final_result
     */
    public function create_final_data_merge_brands($merge_brands_array) {
        $final_result = array();

        foreach ($merge_brands_array as $merge_brand) {
            $equal_brands = $this->get_equal_brands_by_group_name($merge_brand);

            if (!empty($equal_brands)) {
                foreach ($equal_brands as $value) {
                    if (!in_array($value['brand'], $final_result)) {
                        $final_result[] = $value['brand'];
                    }
                }
            }
        }

        return $final_result;
    }

    /**
     * Функция получает ключ группы по названию группы
     * @param string $group_name
     * @return string $result['key_group'] or boolean
     */
    public function get_key_group_brands_by_group_name($group_name) {
        $this->db->select('key_group');
        $this->db->distinct();
        $this->db->from('groups_brands');
        $this->db->where('group_name', $group_name);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        if (!empty($result)) {
            return $result['key_group'];
        } else {
            return FALSE;
        }
    }

    /**
     * Функция получает название группы по ключу группы
     * @param string $key_group
     * @return string $result['group_name'] or boolean
     */
    public function get_group_name_brands_by_key_group($key_group) {
        $this->db->select('group_name');
        $this->db->distinct();
        $this->db->from('groups_brands');
        $this->db->where('key_group', $key_group);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        if (!empty($result)) {
            return $result['group_name'];
        } else {
            return FALSE;
        }
    }

    /**
     * Функция объединяет между собой данные, которые пришли от api
     * @param array $api_data
     * @return array $result
     */
    public function merge_api_data($api_data) {
        $result = array();

        if (!empty($api_data)) {
            foreach ($api_data as $value) {
                $key_exist_detail = '';

                if (!empty($result)) {
                    foreach ($result as $key => $res) {
                        if ((($res['article'] == $value['article'])
                                and ( $res['brand'] == $value['brand']))) {
                            $key_exist_detail = $key;
                        }
                    }
                }

                if ($key_exist_detail === '') {
                    $result[] = $value;
                } else {
                    if (isset($value['name']) && mb_strlen($result[$key_exist_detail]['name']) < mb_strlen($value['name'])) {
                        $result[$key_exist_detail]['name'] = $value['name'];
                    }

                    $old_rating = $result[$key_exist_detail]['rating'];

                    if (($old_rating != 'original') and ( $value['rating'] != 'original')) {
                        foreach ($value['rating'] as $key_rating => $value_rating) {
                            if (!array_key_exists($key_rating, $old_rating)) {
                                $result[$key_exist_detail]['rating'][$key_rating] = $value_rating;
                            }
                        }
                    } else {
                        $result[$key_exist_detail]['analog'] = 0;
                        $result[$key_exist_detail]['rating'] = 'original';
                    }

                    if (isset($value['offers']) and ! empty($value['offers'])) {
                        foreach ($value['offers'] as $offer) {
                            if (isset($result[$key_exist_detail]['offers'])) {
                                if (!in_array($offer, $result[$key_exist_detail]['offers'])) {
                                    $result[$key_exist_detail]['offers'][] = $offer;
                                }
                            } else {
                                $result[$key_exist_detail]['offers'][] = $offer;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Функция удаляет одинаковые бренды из данных, которые пришли от api и прайс-листов
     * @param array $api_data
     * @param string $article
     * @param string $group_name
     * @param array $groups_brands
     * @return array $result
     */
    public function del_duplicate_brands_in_data($api_data, $article, $group_name, $groups_brands) {
        $result = array();

        foreach ($api_data as $data) {
            $temporary_key = '';
            $new_brand = '';

            foreach ($groups_brands as $group_brands) {
                if(in_array($data['brand'], $group_brands['brands'])) {
                    $temporary_key = $group_brands['group_name'] . '_' . $data['article'];
                    $new_brand = $group_brands['group_name'];
                }

                continue;
            }

            if ($temporary_key === '') {
                $result[] = $data;
            } else {
                if (!array_key_exists($temporary_key, $result)) {
                    $data['brand'] = $new_brand;

                    $result[$temporary_key] = $data;
                } else {
                    $result[$temporary_key]['brand'] = $new_brand;

                    if (isset($data['name']) && mb_strlen($result[$temporary_key]['name']) < mb_strlen($data['name'])) {
                        $result[$temporary_key]['name'] = $data['name'];
                    }

                    $old_rating = $result[$temporary_key]['rating'];

                    if (($old_rating != 'original') and ( $data['rating'] != 'original')) {
                        foreach ($data['rating'] as $key_rating => $value_rating) {
                            if (!array_key_exists($key_rating, $old_rating)) {
                                $result[$temporary_key]['rating'][$key_rating] = $value_rating;
                            }
                        }
                    } else {
                        $result[$temporary_key]['analog'] = 0;
                        $result[$temporary_key]['rating'] = 'original';
                    }

                    if (isset($data['offers']) and ! empty($data['offers'])) {
                        foreach ($data['offers'] as $offer) {
                            if (isset($result[$temporary_key]['offers'])) {
                                if (!in_array($offer, $result[$temporary_key]['offers'])) {
                                    $result[$temporary_key]['offers'][] = $offer;
                                }
                            } else {
                                $result[$temporary_key]['offers'][] = $offer;
                            }
                        }
                    }
                }
            }
        }

        if (!empty($result)) {
            $have_detail = 0;

            foreach ($result as $value) {
                if ($value['analog'] == 0) {
                    $have_detail ++;
                }
            }

            if ($have_detail == 0) {
                $result[] = array(
                    'article' => $article,
                    'brand' => $group_name,
                    'name' => '',
                    'analog' => 0,
                    'rating' => 'original',
                );
            }
        }

        return $result;
    }

    /**
     * Функция проверяет есть ли неодинаковые артикулы у объединяемых брендов (используется для объединения брендов в списке найденных деталей)
     * @param array $merge_brands_array
     * @return boolean
     */
    public function check_all_articles_equal($merge_brands_array) {
        $result = array();

        foreach ($merge_brands_array as $data) {
            preg_match_all('/\*\*(.+)\*\*/iu', $data, $out);

            if (isset($out[1][0])) {
                $result[] = $out[1][0];
            }
        }

        $temporary_item = $result[0];

        foreach ($result as $res) {
            if ($res != $temporary_item) {
                return FALSE;
            }
        }

        return TRUE;
    }

    /**
     * Функция получает дату последнего поиска данной детали
     * @param string $article
     * @param string $key_group
     * @return array $result
     */
    public function get_date_update_detail($article, $key_group) {
        $result = array();

        $brand = $this->get_group_name_by_key_group($key_group);

        $this->db->select('date');
        $this->db->from('articles');
        $this->db->join('brands', 'brands.id = articles.id_brand');
        $this->db->where('articles.article', $article);
        $this->db->where('brands.name', $brand);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        if (!empty($result)) {
            return $result['date'];
        } else {
            return '';
        }
    }

    /**
     * Функция получает ключи групп для объединяемых брендов
     * @param array $merge_brands_array
     * @return array $result
     */
    public function get_key_group_for_merge_brands($merge_brands_array) {
        $result = array();

        $this->db->select('key_group');
        $this->db->from('groups_brands');
        $this->db->where_in('group_name', $merge_brands_array);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $data = $query->result_array();

        if (!empty($data)) {
            foreach ($data as $value) {
                $result[] = $value['key_group'];
            }
        }

        return $result;
    }

    /**
     * Функция сохраняет данные объединяемых брендов в БД (используется для страницы со списком найденных деталей)
     * @param array $merge_brands_array
     * @param array $final_result
     * @param string $name_group
     * @param string $key_group
     * @param array $description_brand
     * @return boolean
     */
    public function save_merge_brands_for_list_detail($merge_brands_array, $final_result, $name_group, $key_group, $description_brand) {
        $this->db->trans_start();
        foreach ($merge_brands_array as $merge_brand) {
            $this->db->delete('groups_brands', array('group_name' => $merge_brand));
        }

        foreach ($final_result as $value) {
            $data = array(
                'brand' => $value,
                'group_name' => $name_group,
                'key_group' => $key_group,
            );

            $this->db->insert('groups_brands', $data);
        }

        $this->db->empty_table('articles');
        $this->db->empty_table('auto_parts');
        $this->db->empty_table('detail_cross');
        $this->db->empty_table('offers');

        $this->db->where_in('name', $merge_brands_array);
        $this->db->delete('brands');

        $data = array(
            'name' => $name_group,
            'description' => isset($description_brand['description']) ? $description_brand['description'] : NULL,
            'link' => isset($description_brand['link']) ? $description_brand['link'] : NULL,
            'country' => isset($description_brand['country']) ? $description_brand['country'] : NULL,
        );

        $this->db->insert('brands', $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        } else {
            $this->db->trans_commit();

            return TRUE;
        }
    }

    /**
     * Функция проверяет наличие в БД артикула и бренда для поиска
     * @param string $article
     * @param string $brand
     * @return array $result or boolean
     */
    public function check_exist_article_search_brand_search($article, $brand) {
        $this->db->select('*');
        $this->db->from('articles_search');
        $this->db->join('brands_search', 'brands_search.id_article = articles_search.id');
        $this->db->where('articles_search.article', $article);
        $this->db->where('brands_search.brand', $brand);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->result_array();

        if (!empty($result)) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * Функция сохраняет в БД те данные, по которым в дальнейшем будет происходить поиск
     * @param string $article
     * @param string $brand
     * @return boolean
     */
    public function save_data_for_search_by_article_brand($article, $brand) {
        $provider_array = $this->config->item('list_providers');

        $this->db->trans_start();

        $data_table_articles_search = array(
            'article' => $article,
        );

        $this->db->insert('articles_search', $data_table_articles_search);

        $id_article_search = $this->db->insert_id();

        foreach ($provider_array as $provider) {
            $data_table_brands_search[] = array(
                'brand' => $brand,
                'provider' => $provider,
                'id_article' => $id_article_search,
            );
        }

        foreach ($data_table_brands_search as $data) {
            $this->db->insert('brands_search', $data);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        } else {
            $this->db->trans_commit();

            return TRUE;
        }
    }

    /**
     * Функция создает ссылки для сортировки списка найденных деталей
     * @param array $fields_sort_array
     * @param string $field_sort
     * @param string $type_sort
     * @return array $result
     */
    public function create_sort_links($fields_sort_array, $field_sort, $type_sort) {
        $result = array();

        foreach ($fields_sort_array as $value) {
            if ($value == $field_sort) {
                $result[$value] = array(
                    'field_name' => $value,
                    'type_sort' => $type_sort == 'asc' ? 'desc' : 'asc',
                );
            } else {
                $result[$value] = array(
                    'field_name' => $value,
                    'type_sort' => 'asc',
                );
            }
        }

        return $result;
    }

    /**
     * Функция получает список поставщиков, которые работают по api
     * @return array
     */
    public function get_list_provider_with_api() {
        $this->db->select('*');
        $this->db->from('provider');
        $this->db->where('source_data', 'api');

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция получает полное описание бренда
     * @param string $brand_name
     * @return array
     */
    public function get_description_brand_by_name_brand($brand_name) {
        $this->db->select('description,
                           link,
                           country');
        $this->db->from('brands');
        $this->db->where('name', $brand_name);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    /**
     * Функция производит фильтр предложений для детали по типу склада и результат сортирует
     * @param int $id_detail
     * @param array $list_offers
     * @param string $filter_warehouse
     * @param string $name_sort
     * @param string $type_sort
     * @return array
     */
    public function filter_and_sort_list_offers($list_offers, $filter_warehouse, $name_sort, $type_sort, $is_complete) {
        if ($is_complete != '1') {
            foreach ($list_offers as $key_offer => $value) {
                if ($value['type_search'] != 'main') {
                    unset($list_offers[$key_offer]);
                }
            }
        }

        if ($filter_warehouse == 'three') {
            $final_list_offers = $list_offers;
        } elseif ($filter_warehouse == 'one_two') {
            foreach ($list_offers as $value) {
                if (($value['warehouse_type'] == 1) or ( $value['warehouse_type'] == 2)) {
                    $final_list_offers[] = $value;
                }
            }
        } else {
            foreach ($list_offers as $value) {
                if (($value['warehouse_type'] == 1) or ( $value['warehouse_type'] == 2)) {
                    $final_list_offers[] = $value;
                }
            }

            if (empty($final_list_offers)) {
                $final_list_offers = $list_offers;
            }
        }

        if (empty($final_list_offers)) {
            return $final_list_offers;
        }

        foreach ($final_list_offers as $key => $offer) {
            $final_list_offers[$key]['retail_price'] = $this->calculation_retail_price($offer['price']);
        }

        $sort_array = array();

        if ($name_sort == 'type_warehouse') {
            $key = 'warehouse_type';
        } elseif ($name_sort == 'description') {
            $key = 'offers_description';
        } elseif ($name_sort == 'opportunity_return') {
            $key = 'opportunity_return';
        } elseif ($name_sort == 'provider') {
            $key = 'provider';
        } elseif ($name_sort == 'delivery_period') {
            $key = 'delivery_period';
        } elseif ($name_sort == 'retail_price') {
            $key = 'retail_price';
        } elseif ($name_sort == 'price') {
            $key = 'price';
        } elseif ($name_sort == 'quantity') {
            $key = 'quantity';
        } else {
            $key = 'id';
        }

        if (empty($type_sort) or ! in_array($type_sort, array('asc', 'desc'))) {
            $type_sort = 'asc';
        }

        foreach ($final_list_offers as $offer) {
            $sort_array[] = $offer[$key];
        }

        if ($type_sort == 'desc') {
            array_multisort($sort_array, SORT_DESC, $final_list_offers);
        } else {
            array_multisort($sort_array, SORT_ASC, $final_list_offers);
        }

        return $final_list_offers;
    }

    /**
     * Функция получает данные детали по коду предложения
     * @param string $code_offer
     * @return array
     */
    public function get_data_detail_by_code_offer($code_offer) {
        $this->db->select('articles.article,
                           brands.name as brand,
                           auto_parts.name as auto_part_name,
                           offers.id as id_offer,
                           offers.price,
                           offers.code,
                           offers.quantity,
                           offers.id_warehouse_type,
                           offers.delivery_period,
                           provider.name as provider,
                           provider.percent as provider_percent,');
        $this->db->from('articles');
        $this->db->join('brands', 'brands.id = articles.id_brand');
        $this->db->join('auto_parts', 'articles.id = auto_parts.id_article');
        $this->db->join('offers', 'offers.id_auto_part = auto_parts.id');
        $this->db->join('type_warehouse', 'type_warehouse.id = offers.id_warehouse_type');
        $this->db->join('provider', 'provider.id = type_warehouse.id_provider');
        $this->db->where('offers.code', $code_offer);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    /**
     * Функция получает данные детали по id auto_part
     * @param int $id_auto_part
     * @return array
     */
    public function get_data_detail_by_id_auto_part($id_auto_part) {
        $this->db->select('articles.article,
                           articles.is_complete,
                           brands.name as brand,
                           auto_parts.name as auto_part_name');
        $this->db->from('articles');
        $this->db->join('brands', 'brands.id = articles.id_brand');
        $this->db->join('auto_parts', 'articles.id = auto_parts.id_article');
        $this->db->where('auto_parts.id', $id_auto_part);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    /**
     * Функция получает данные предложения по коду предложения
     * @param string $code_offer
     * @return array
     */
    public function get_offer_by_code_offer($code_offer) {
        $this->db->select('*');
        $this->db->from('offers');
        $this->db->where('offers.code', $code_offer);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    /**
     * Функция получает данные поставщика по названию поставщика
     * @param string $name
     * @return array
     */
    public function get_provider_by_name($name) {
        $this->db->select('*');
        $this->db->from('provider');
        $this->db->where('name', $name);
        $this->db->limit(1);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    /**
     * Функция проверяет завершен ли поиск деталей для указанного артикула и бренда
     * @param int $id
     * @return int $result['is_complete']
     */
    public function check_search_detail_is_complete($id) {
        $this->db->select('is_complete');
        $this->db->from('articles');
        $this->db->where('id', $id);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        $result = $query->row_array();

        return $result;
    }

    /**
     * Функция в фоновом режиме получает данные от api и прайс-листов и записывает эти данные в БД
     * @param string $article
     * @param string $group_name
     * @return boolean
     */
    public function background_update_data_search_detail($article, $group_name) {
        $result = array();
        $providers_array = array();
        $data_array = array();

        $equal_brands = $this->get_equal_brands_by_group_name($group_name);

        $brands_search = $this->get_brands_search_by_article_equal_brands($article, $equal_brands);

        if (!empty($brands_search) and ! empty($equal_brands)) {
            foreach ($brands_search as $brand) {
                if (!in_array($brand['provider'], $providers_array)) {
                    $providers_array[] = $brand['provider'];
                }
            }

            $result = $this->background_get_api_data_from_curl($article, $brands_search, $providers_array);
        }

        if (!empty($result)) {
            $result = $this->merge_api_data($result);

            $res_search_in_price_lists = array();
            $brands_in_list = array();

            foreach ($result as $res) {
                if(!in_array($res['brand'], $brands_in_list)) {
                    $brands_in_list[] = $res['brand'];
                }
            }

            $groups_brands = $this->get_groups_brands_for_brands($brands_in_list);

            $res_search_in_price_lists = $this->search_details_from_price_list($result);

            $result = $this->del_duplicate_brands_in_data($result, $article, $group_name, $groups_brands);

            if (!empty($res_search_in_price_lists)) {
                $result = $this->merge_price_data_and_api_data($result, $res_search_in_price_lists, $groups_brands);
            }
        }

        $brand = $group_name;

        if (empty($result)) {
            $result[] = array(
                'article' => $article,
                'brand' => $brand,
                'name' => '',
                'analog' => 0,
                'rating' => 'original',
            );
        }

        if ($this->background_save_data_search($result)) {
            return TRUE;
        }

        $id = $this->get_id_by_article_and_brand($article, $brand);

        if (!empty($id)) {
            $this->del_cross_for_detail($id);
            $this->del_detail_considering_cross($id);
        }

        return FALSE;
    }

    /**
     * Функция в фоновом режиме получает в результете поиска данные от api по curl
     * @param string $article
     * @param array $brands_search
     * @param array $providers_array
     * @return array $result
     */
    public function background_get_api_data_from_curl($article, $brands_search, $providers_array) {
        $mh = curl_multi_init();
        $chs = array();
        $result = array();

        if (in_array('abs', $providers_array)) {
            $key_abs = $this->get_key_abs();
            $user_context_abs = $this->get_user_context_abs($key_abs);

            if (isset($user_context_abs['user_agreements'][0]['ua_id'])) {
                $agreement_id = $user_context_abs['user_agreements'][0]['ua_id'];
            } else {
                $agreement_id = '';
            }
        }

        foreach ($brands_search as $brand) {
            if ($brand['provider'] == 'tiss') {
                $is_main_warehouse_tiss = $this->config->item('is_main_warehouse_tiss');

                $ch_tiss = curl_init();

                $chs[] = array(
                    'ch' => $ch_tiss,
                    'provider' => 'tiss',
                    'article' => $article,
                    'brand' => $brand['brand'],
                );

                $fields = array("JSONparameter" => "{'Brand': '" . $brand['brand'] . "', 'Article': '" . $article . "', 'is_main_warehouse': " . $is_main_warehouse_tiss . " }");
                $headers = array(
                    'Authorization: Bearer ' . $this->config->item('api_key_tiss')
                );

                curl_setopt($ch_tiss, CURLOPT_URL, "api.tmparts.ru/api/StockByArticle?" . http_build_query($fields));
                curl_setopt($ch_tiss, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_tiss, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch_tiss, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch_tiss, CURLOPT_TIMEOUT, 25);
                curl_multi_add_handle($mh, $ch_tiss);
            }

            if ($brand['provider'] == 'adeo_pro') {
                $login = $this->config->item('login_adeo_pro');
                $password = $this->config->item('pas_adeo_pro');
                $force_diler_replace = $this->config->item('force_diler_replace_adeo_pro');

                $xml = '<?xml version="1.0" encoding="UTF-8" ?>
                        <message>
                          <param>
                            <action>price</action>
                            <login>' . $login . '</login>
                            <password>' . $password . '</password>
                            <code>' . $article . '</code>
                            <brand>' . $brand['brand'] . '</brand>
                            <force_dilerReplace>' . $force_diler_replace . '</force_dilerReplace>
                         </param>
                       </message>';

                $data = array('xml' => $xml);
                $address = "http://adeo.pro/pricedetals2.php";

                $ch_adeo_pro = curl_init($address);

                $chs[] = array(
                    'ch' => $ch_adeo_pro,
                    'provider' => 'adeo_pro',
                    'article' => $article,
                    'brand' => $brand['brand'],
                );

                curl_setopt($ch_adeo_pro, CURLOPT_HEADER, 0);
                curl_setopt($ch_adeo_pro, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_adeo_pro, CURLOPT_POST, 1);
                curl_setopt($ch_adeo_pro, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch_adeo_pro, CURLOPT_TIMEOUT, 25);
                curl_setopt($ch_adeo_pro, CURLOPT_POSTFIELDS, $data);
                curl_multi_add_handle($mh, $ch_adeo_pro);
            }

            if ($brand['provider'] == 'abs') {
                $brand['brand'] = str_replace(" ", "+", $brand['brand']);
                $cross_abs = $this->config->item('cross_abs');

                $url_abs = 'http://api.abs-auto.ru/api-search?auth=' . $key_abs . '&article=' . $article . '&brand=' . $brand['brand'] . '&with_cross=' . $cross_abs . '&agreement_id=' . $agreement_id . '&format=json';

                $ch_abs = curl_init();

                $chs[] = array(
                    'ch' => $ch_abs,
                    'provider' => 'abs',
                    'article' => $article,
                    'brand' => $brand['brand'],
                );

                curl_setopt($ch_abs, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch_abs, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_abs, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch_abs, CURLOPT_TIMEOUT, 25);
                curl_setopt($ch_abs, CURLOPT_URL, $url_abs);
                curl_multi_add_handle($mh, $ch_abs);
            }

            if ($brand['provider'] == 'armtek') {
                $login_armtek = $this->config->item('login_armtek');
                $pas_armtek = $this->config->item('pas_armtek');

                $user_vkorg_list = $this->get_user_vkorg_list_armtek($login_armtek, $pas_armtek);

                if (!empty($user_vkorg_list)) {
                    $chs_armtek = array();

                    foreach ($user_vkorg_list as $vkorg) {
                        $user_info_armtek = $this->get_user_info_armtek($login_armtek, $pas_armtek, $vkorg['VKORG']);

                        if (!empty($user_info_armtek) and isset($user_info_armtek['STRUCTURE']['RG_TAB'][0]['KUNNR'])) {
                            $data_armtek = array('VKORG' => $vkorg['VKORG'],
                                'KUNNR_RG' => $user_info_armtek['STRUCTURE']['RG_TAB'][0]['KUNNR'],
                                'PIN' => $article,
                                'BRAND' => $brand['brand'],
                            );

                            $ch_armtek = curl_init();

                            $chs[] = array(
                                'ch' => $ch_armtek,
                                'provider' => 'armtek',
                                'article' => $article,
                                'brand' => $brand['brand'],
                            );

                            curl_setopt($ch_armtek, CURLOPT_URL, "http://ws.armtek.ru/api/ws_search/search?format=json");
                            curl_setopt($ch_armtek, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch_armtek, CURLOPT_POST, true);
                            curl_setopt($ch_armtek, CURLOPT_POSTFIELDS, http_build_query($data_armtek));
                            curl_setopt($ch_armtek, CURLOPT_USERPWD, $login_armtek . ':' . $pas_armtek);
                            curl_setopt($ch_armtek, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($ch_armtek, CURLOPT_TIMEOUT, 25);
                            curl_multi_add_handle($mh, $ch_armtek);
                        }
                    }
                }
            }
        }

        $running = null;

        do {
            curl_multi_exec($mh, $running);
        } while ($running > 0);

        foreach ($chs as $ch) {
            if ($ch['provider'] == 'tiss') {
                $data_tiss = curl_multi_getcontent($ch['ch']);

                if ($data_tiss !== false) {
                    $result_tiss = $this->standard_view_data_tiss($data_tiss, $ch['article'], $ch['brand']);
                } else {
                    $result_tiss = array();
                }

                $result = array_merge($result, $result_tiss);
            }

            if ($ch['provider'] == 'adeo_pro') {
                $data_adeo_pro = curl_multi_getcontent($ch['ch']);

                if ($data_adeo_pro !== false) {
                    $result_adeo_pro = $this->standard_view_data_adeo_pro($data_adeo_pro, $ch['article'], $ch['brand']);
                } else {
                    $result_adeo_pro = array();
                }

                $result = array_merge($result, $result_adeo_pro);
            }

            if ($ch['provider'] == 'abs') {
                $data_abs = curl_multi_getcontent($ch['ch']);

                if ($data_abs !== false) {
                    $result_abs = $this->standard_view_data_abs($data_abs, $ch['article'], $ch['brand']);
                } else {
                    $result_abs = array();
                }

                $result = array_merge($result, $result_abs);
            }

            if ($ch['provider'] == 'armtek') {
                $data_api_armtek = curl_multi_getcontent($ch['ch']);

                if ($data_api_armtek !== false) {
                    $result_api_armtek = $this->standard_view_data_armtek($data_api_armtek, $ch['article'], $ch['brand']);
                } else {
                    $result_api_armtek = array();
                }

                $result = array_merge($result, $result_api_armtek);
            }

            curl_multi_remove_handle($mh, $ch['ch']);
            curl_close($ch['ch']);
        }

        curl_multi_close($mh);

        return $result;
    }

    /**
     * Функция в фоновом режиме сохраняет все результаты поиска в БД
     * @param array $data
     * @return boolean
     */
    public function background_save_data_search($data) {
        foreach ($data as $value) {
            if ($value['analog'] === 0) {
                $id_detail = $this->background_save_detail($value);
                break;
            }
        }

        if ($id_detail) {
            $new_details_array = array();

            $new_details_array[] = $id_detail;

            foreach ($data as $value) {
                if ($value['analog'] != 0) {
                    if (!$new_cross_detail = $this->background_save_cross_detail($value, $id_detail)) {
                        return FALSE;
                    } else {
                        $new_details_array[] = $new_cross_detail;
                    }
                }
            }

            if ($this->del_old_offers_background_search($id_detail, $new_details_array)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function del_old_offers_background_search($id, $new_details_array) {
        $id_cross_array = $this->get_id_cross_by_id_detail($id);

        $id_array[] = $id;

        if (!empty($id_cross_array)) {
            foreach ($id_cross_array as $value) {
                $id_array[] = $value['id_cross'];
            }
        }

        foreach ($id_array as $key => $value) {
            if (in_array($value, $new_details_array)) {
                unset($id_array[$key]);
            }
        }

        if (empty($id_array)) {
            return TRUE;
        }

        $this->db->select('offers.id');
        $this->db->from('offers');
        $this->db->join('auto_parts', 'auto_parts.id = offers.id_auto_part');
        $this->db->join('articles', 'articles.id = auto_parts.id_article');
        $this->db->where('offers.type_search', 'background');
        $this->db->where_in('articles.id', $id_array);

        $query = $this->db->get();

        if (!$query) {
            return FALSE;
        }

        $offers_array = $query->result_array();

        if (!empty($offers_array)) {
            $offer_ids = array();

            foreach ($offers_array as $value) {
                $offer_ids[] = $value['id'];
            }

            $this->db->where_in('id', $offer_ids);
            if ($this->db->delete('offers')) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return TRUE;
        }
    }

    /**
     * Функция сохраняет данные оригинальной детали в БД (используется для поиска деталей, который происходит в фоновом режиме)
     * @param array $data
     * @return int $id_row_article or boolean
     */
    public function background_save_detail($data) {
        $data_articles_table = array(
            'article' => strip_tags($data['article']),
            'is_complete' => 1,
        );

        if (!isset($data['name'])) {
            $data['name'] = '';
        }

        $data_auto_parts_table = array(
            'name' => strip_tags($data['name']),
        );

        $id_brand = $this->check_exist_brand($data['brand']);

        $this->db->trans_start();
        if (!$id_brand) {
            $this->db->insert('brands', array('name' => strip_tags($data['brand'])));

            $id_brand = $this->db->insert_id();
        }

        $data_articles_table['id_brand'] = $id_brand;

        if (!$id_row_article = $this->get_id_by_article_and_brand($data['article'], $data['brand'])) {
            $data_articles_table['date'] = date('Y-m-d');

            $this->db->insert('articles', $data_articles_table);

            $id_row_article = $this->db->insert_id();

            $data_auto_parts_table['id_article'] = $id_row_article;
        } else {
            $this->db->where('id', $id_row_article);
            $this->db->update('articles', $data_articles_table);

            $data_auto_parts_table['id_article'] = $id_row_article;
        }

        if (empty($data_detail = $this->get_detail_by_article_brand($data['article'], $data['brand']))) {
            $this->db->insert('auto_parts', $data_auto_parts_table);

            $id_auto_part = $this->db->insert_id();
        } else {
            $id_old_auto_part = $data_detail['auto_part_id'];

            if (mb_strlen($data_auto_parts_table['name']) > mb_strlen($data_detail['auto_part_name'])) {
                $this->db->where('id', $id_old_auto_part);
                $this->db->update('auto_parts', $data_auto_parts_table);
            }

            $id_auto_part = $id_old_auto_part;

            $old_offers = $this->get_offers_by_id_auto_part($id_auto_part);

            $this->db->delete('offers', array('id_auto_part' => $id_old_auto_part, 'type_search' => 'background'));
        }

        if (isset($data['offers']) and ! empty($data['offers'])) {
            foreach ($data['offers'] as $offer) {
                $data_offers_table[] = array(
                    'price' => round(strip_tags($offer['price']), 2),
                    'quantity' => preg_replace("/(\s*)шт(\.*)/iu", "", strip_tags($offer['quantity'])),
                    'min_part' => preg_replace("/(\s*)шт(\.*)/iu", "", strip_tags($offer['min_part'])),
                    'id_warehouse_type' => $offer['id_warehouse_type'],
                    'description' => strip_tags($offer['description']),
                    'warehouse' => strip_tags($offer['warehouse']),
                    'delivery_period' => preg_replace("/(\s*)дней|день|дня|дн(\.*)/iu", "", strip_tags($offer['delivery_period'])),
                    'id_auto_part' => $id_auto_part,
                    'type_search' => 'background',
                );
            }

            $codes_array = array();

            foreach ($data_offers_table as $n_key => $n_offer) {
                $exists_offer = FALSE;

                if (!empty($old_offers)) {
                    foreach ($old_offers as $o_key => $o_offer) {
                        if (($n_offer['price'] == $o_offer['price']) && ($n_offer['quantity'] == $o_offer['quantity']) && ($n_offer['min_part'] == $o_offer['min_part']) && ($n_offer['id_warehouse_type'] == $o_offer['id_warehouse_type']) && ($n_offer['description'] == $o_offer['description']) && ($n_offer['warehouse'] == $o_offer['warehouse']) && ($n_offer['delivery_period'] == $o_offer['delivery_period'])) {
                            $exists_offer = $o_key;
                            break;
                        }
                    }
                }

                if (!$exists_offer) {
                    do {
                        $code = create_random_code(5) . md5(date('Y-m-d H:i:s'));
                    } while (in_array($code, $codes_array));
                } else {
                    $code = $old_offers[$exists_offer]['code'];
                }

                $data_offers_table[$n_key]['code'] = $code;
                $codes_array[] = $code;
            }

            $this->db->insert_batch('offers', $data_offers_table);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        } else {
            $this->db->trans_commit();

            return $id_row_article;
        }
    }

    /**
     * Функция сохраняет аналоги для детали в БД (используется для поиска деталей, который происходит в фоновом режиме)
     * @param array $data
     * @param int $id_detail
     * @return int $id_row_article or boolean
     */
    public function background_save_cross_detail($data, $id_detail) {
        $compatibility = 0;

        $data_articles_table = array(
            'article' => strip_tags($data['article']),
        );

        if (!isset($data['name'])) {
            $data['name'] = '';
        }

        $data_auto_parts_table = array(
            'name' => strip_tags($data['name']),
        );

        $id_brand = $this->check_exist_brand($data['brand']);

        $this->db->trans_start();
        if (!$id_brand) {
            $this->db->insert('brands', array('name' => strip_tags($data['brand'])));

            $id_brand = $this->db->insert_id();
        }

        $data_articles_table['id_brand'] = $id_brand;

        if (!$id_row_article = $this->get_id_by_article_and_brand($data['article'], $data['brand'])) {
            $this->db->insert('articles', $data_articles_table);

            $id_row_article = $this->db->insert_id();

            $data_auto_parts_table['id_article'] = $id_row_article;
        } else {
            $this->db->where('id', $id_row_article);
            $this->db->update('articles', $data_articles_table);

            $data_auto_parts_table['id_article'] = $id_row_article;
        }

        if (empty($data_detail = $this->get_detail_by_article_brand($data['article'], $data['brand']))) {
            $this->db->insert('auto_parts', $data_auto_parts_table);

            $id_auto_part = $this->db->insert_id();
        } else {
            $id_old_auto_part = $data_detail['auto_part_id'];

            if (mb_strlen($data_auto_parts_table['name']) > mb_strlen($data_detail['auto_part_name'])) {
                $this->db->where('id', $id_old_auto_part);
                $this->db->update('auto_parts', $data_auto_parts_table);
            }

            $id_auto_part = $id_old_auto_part;

            $old_offers = $this->get_offers_by_id_auto_part($id_auto_part);

            $this->db->delete('offers', array('id_auto_part' => $id_old_auto_part, 'type_search' => 'background'));
        }

        if (isset($data['offers']) and ! empty($data['offers'])) {
            foreach ($data['offers'] as $offer) {
                $data_offers_table[] = array(
                    'id_auto_part' => $id_auto_part,
                    'price' => round(strip_tags($offer['price']), 2),
                    'quantity' => preg_replace("/(\s*)шт(\.*)/iu", "", strip_tags($offer['quantity'])),
                    'min_part' => preg_replace("/(\s*)шт(\.*)/iu", "", strip_tags($offer['min_part'])),
                    'id_warehouse_type' => $offer['id_warehouse_type'],
                    'warehouse' => strip_tags($offer['warehouse']),
                    'description' => strip_tags($offer['description']),
                    'delivery_period' => preg_replace("/(\s*)дней|день|дня|дн(\.*)/iu", "", strip_tags($offer['delivery_period'])),
                    'type_search' => 'background',
                );
            }

            $codes_array = array();

            foreach ($data_offers_table as $n_key => $n_offer) {
                $exists_offer = FALSE;

                if (!empty($old_offers)) {
                    foreach ($old_offers as $o_key => $o_offer) {
                        if (($n_offer['price'] == $o_offer['price']) && ($n_offer['quantity'] == $o_offer['quantity']) && ($n_offer['min_part'] == $o_offer['min_part']) && ($n_offer['id_warehouse_type'] == $o_offer['id_warehouse_type']) && ($n_offer['description'] == $o_offer['description']) && ($n_offer['warehouse'] == $o_offer['warehouse']) && ($n_offer['delivery_period'] == $o_offer['delivery_period'])) {
                            $exists_offer = $o_key;
                            break;
                        }
                    }
                }

                if (!$exists_offer) {
                    do {
                        $code = create_random_code(5) . md5(date('Y-m-d H:i:s'));
                    } while (in_array($code, $codes_array));
                } else {
                    $code = $old_offers[$exists_offer]['code'];
                }

                $data_offers_table[$n_key]['code'] = $code;
                $codes_array[] = $code;
            }

            $this->db->insert_batch('offers', $data_offers_table);
        }

        foreach ($data['rating'] as $rating) {
            $compatibility = $compatibility + $rating;
        }

        $data_cross = $this->check_exist_cross_in_detail_cross($id_detail, $id_row_article);

        if (!empty($data_cross)) {
            $data_detail_cross_table = array(
                'compatibility' => $data_cross['compatibility'] + $compatibility,
            );

            $this->db->where('id', $data_cross['id']);
            $this->db->update('detail_cross', $data_detail_cross_table);
        } else {
            $data_detail_cross_table = array(
                'id_detail' => $id_detail,
                'id_cross' => $id_row_article,
                'compatibility' => $compatibility,
            );

            $this->db->insert('detail_cross', $data_detail_cross_table);
        }

        if (!$this->check_exist_brand_in_table_groups_brands($data['brand'])) {
            do {
                $key_group = create_random_code(10);
            } while (!empty($this->check_exist_key_equal_brands($key_group)));

            $data = array(
                'brand' => strip_tags($data['brand']),
                'group_name' => strip_tags($data['brand']),
                'key_group' => $key_group,
            );

            $this->db->insert('groups_brands', $data);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        } else {
            $this->db->trans_commit();

            return $id_row_article;
        }
    }

    public function check_exist_cross_in_detail_cross($id_detail, $id_cross) {
        $this->db->select('*');
        $this->db->from('detail_cross');
        $this->db->where('id_detail', $id_detail);
        $this->db->where('id_cross', $id_cross);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    /**
     * Функция получает список уникальных брендов из списка найденных деталей
     * @param array $list_details
     * @return array $list_brands
     */
    public function get_unique_brands_from_list_details($list_details) {
        $result = array();
        $list_brands = array();

        if (isset($list_details['cross']) && !empty($list_details['cross'])) {
            foreach ($list_details['cross'] as $key => $value) {
                if (!array_key_exists('element_' . $value['brand_id'], $list_brands)) {
                    $list_brands['element_' . $value['brand_id']] = array(
                        'brand' => strtoupper($value['brand']),
                        'recommended' => $value['recommended'],
                    );
                }

                $exist_my_storage = 0;

                foreach($list_details['cross'][$key]['offers'] as $offers) {
                    if($offers['type_provider'] == 1) {
                        $exist_my_storage++;
                    }
                }

                if(!isset($list_brands['element_' . $value['brand_id']]['exist_my_storage'])
                        or $list_brands['element_' . $value['brand_id']]['exist_my_storage'] == 0) {
                    $list_brands['element_' . $value['brand_id']]['exist_my_storage'] = ($exist_my_storage > 0) ? 1 : 0;
                }
            }
        }

        if (!empty($list_brands)) {
            $brands_array = array();

            foreach ($list_brands as $value) {
                $brands_array[] = $value['brand'];
            }

            array_multisort($brands_array, SORT_REGULAR, $list_brands);
        }

        if (isset($list_details['original'])) {
            $original_detail = array();
            $exist_my_storage = 0;

            foreach ($list_details['original']['offers'] as $offers) {
                if($offers['type_provider'] == 1) {
                    $exist_my_storage++;
                }
            }

            $original_detail['element_' . $list_details['original']['brand_id']] = array(
                'brand' => strtoupper($list_details['original']['brand']),
                'recommended' => $list_details['original']['recommended'],
                'exist_my_storage' => ($exist_my_storage > 0) ? 1 : 0,
            );

            $list_brands = $original_detail + $list_brands;
        }

        return $list_brands;
    }

    public function filter_list_details_by_price($details_array, $price_between_beg, $price_between_end) {
        if (!empty($details_array)) {
            if (isset($details_array['original'])) {
                foreach ($details_array['original']['offers'] as $key => $offer) {
                    if (($offer['retail_price'] >= $price_between_beg) && ($offer['retail_price'] <= $price_between_end)) {
                        continue;
                    } else {
                        unset($details_array['original']['offers'][$key]);
                    }
                }

                if (empty($details_array['original']['offers'])) {
                    unset($details_array['original']);
                } else {
                    $offers = $details_array['original']['offers'];

                    $first_offer = array_shift($offers);

                    $details_array['original']['retail_price'] = $first_offer['retail_price'];
                }
            }

            if (isset($details_array['cross'])) {
                foreach ($details_array['cross'] as $key_detail => $detail) {
                    foreach ($detail['offers'] as $key_offer => $offer) {
                        if (($offer['retail_price'] >= $price_between_beg) && ($offer['retail_price'] <= $price_between_end)) {
                            continue;
                        } else {
                            unset($details_array['cross'][$key_detail]['offers'][$key_offer]);
                        }
                    }

                    if (empty($details_array['cross'][$key_detail]['offers'])) {
                        unset($details_array['cross'][$key_detail]);
                    } else {
                        $offers = $details_array['cross'][$key_detail]['offers'];

                        $first_offer = array_shift($offers);

                        $details_array['cross'][$key_detail]['retail_price'] = $first_offer['retail_price'];
                    }
                }

                if (empty($details_array['cross'])) {
                    unset($details_array['cross']);
                }
            }
        }

        return $details_array;
    }

    /**
     * Функция получает список доступных интервалов доставки из списка деталей
     * @param array $list_details
     * @return array $result
     */
    public function get_intervals_deliv_period($list_details) {
        $result = array(
            '0_1' => 0,
            '2' => 0,
            '3_4' => 0,
            '5_7' => 0,
        );

        $all_offers_array = array();

        if (empty($list_details)) {
            return $result;
        }

        if (isset($list_details['original']) && !empty($list_details['original']['offers'])) {
            foreach ($list_details['original']['offers'] as $offer) {
                $all_offers_array[] = $offer;
            }
        }

        if (isset($list_details['cross'])) {
            foreach ($list_details['cross'] as $cross) {
                foreach ($cross['offers'] as $offer) {
                    $all_offers_array[] = $offer;
                }
            }
        }

        foreach ($all_offers_array as $value) {
            if ($value['del_period_for_filter'] <= 1) {
                $result['0_1'] = $result['0_1'] + 1;
            } elseif ($value['del_period_for_filter'] == 2) {
                $result['2'] = $result['2'] + 1;
            } elseif (($value['del_period_for_filter'] >= 3) && ($value['del_period_for_filter'] <= 4)) {
                $result['3_4'] = $result['3_4'] + 1;
            } elseif (($value['del_period_for_filter'] >= 5) && ($value['del_period_for_filter'] <= 7)) {
                $result['5_7'] = $result['5_7'] + 1;
            }
        }

        return $result;
    }

    /**
     * Функция получает максимальный срок доставки для конкретного предложения (используется для фильтра по сроку доставки)
     * @param array $delivery_period
     * @return int $delivery_period
     */
    public function get_del_period_for_filter($delivery_period) {
        if (empty($delivery_period)) {
            return 10000;
        }

        preg_match_all('/[A-Za-zА-Яа-я]/ui', $delivery_period, $out);

        if (isset($out[1][0])) {
            return 10000;
        }

        preg_match_all('/-/ui', $delivery_period, $out);

        if (isset($out[0][0])) {
            $del_period_parts = explode('-', $delivery_period);

            if(isset($del_period_parts[1]) && !empty(trim($del_period_parts[1]))) {
                return (int) trim($del_period_parts[1]);
            } else {
                return 10000;
            }
        }

        preg_match_all('/>/ui', $delivery_period, $out);

        if (isset($out[0][0])) {
            $delivery_period = preg_replace("/>/iu", "", $delivery_period);

            $delivery_period = (int) trim($delivery_period);

            return $delivery_period + 1;
        }

        preg_match_all('/</ui', $delivery_period, $out);

        if (isset($out[0][0])) {
            $delivery_period = preg_replace("/</iu", "", $delivery_period);

            $delivery_period = (int) trim($delivery_period);

            return $delivery_period - 1;
        }

        return (int) $delivery_period;
    }

    /**
     * Функция получает максимальный срок доставки для конкретного предложения (используется для фильтра по сроку доставки)
     * @param array $details_array
     * @param array $intervals_deliv_period
     * @return array
     */
    public function filter_list_details_by_delivery_period($details_array, $other_data) {
        if (!isset($other_data['intervals_deliv_period']) or empty($other_data['intervals_deliv_period'])) {
            return $details_array;
        }

        if (in_array('all', $other_data['intervals_deliv_period'])) {
            return $details_array;
        }

        if (isset($details_array['original'])) {
            $new_offers = array();

            foreach ($details_array['original']['offers'] as $key_offer => $offer) {
                foreach ($other_data['intervals_deliv_period'] as $del_period) {
                    if ($del_period == '0-1') {
                        if (($offer['del_period_for_filter'] >= 0) && ($offer['del_period_for_filter'] <= 1)) {
                            $new_offers[] = $offer;
                        }
                    } elseif ($del_period == '2') {
                        if ($offer['del_period_for_filter'] == 2) {
                            $new_offers[] = $offer;
                        }
                    } elseif ($del_period == '3-4') {
                        if (($offer['del_period_for_filter'] >= 3) && ($offer['del_period_for_filter'] <= 4)) {
                            $new_offers[] = $offer;
                        }
                    } else {
                        if (($offer['del_period_for_filter'] >= 5) && ($offer['del_period_for_filter'] <= 7)) {
                            $new_offers[] = $offer;
                        }
                    }
                }
            }

            $details_array['original']['offers'] = $new_offers;
        }

        if (isset($details_array['cross'])) {
            foreach ($details_array['cross'] as $key_cross => $cross) {
                $new_offers = array();

                foreach ($cross['offers'] as $key_offer => $offer) {
                    foreach ($other_data['intervals_deliv_period'] as $del_period) {
                        if ($del_period == '0-1') {
                            if (($offer['del_period_for_filter'] >= 0) && ($offer['del_period_for_filter'] <= 1)) {
                                $new_offers[] = $offer;
                            }
                        } elseif ($del_period == '2') {
                            if ($offer['del_period_for_filter'] == 2) {
                                $new_offers[] = $offer;
                            }
                        } elseif ($del_period == '3-4') {
                            if (($offer['del_period_for_filter'] >= 3) && ($offer['del_period_for_filter'] <= 4)) {
                                $new_offers[] = $offer;
                            }
                        } else {
                            if (($offer['del_period_for_filter'] >= 5) && ($offer['del_period_for_filter'] <= 7)) {
                                $new_offers[] = $offer;
                            }
                        }
                    }
                }

                $details_array['cross'][$key_cross]['offers'] = $new_offers;
            }
        }

        return $details_array;
    }

}
