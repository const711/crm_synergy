<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Amocrm_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function auth() {
        $user = array(
            'USER_LOGIN' => $this->config->item('login_amocrm'),
            'USER_HASH' => $this->config->item('pas_amocrm')
        );

        $domain_amocrm = $this->config->item('domain_amocrm');

        $link = 'https://' . $domain_amocrm . '.amocrm.ru/private/api/auth.php?type=json';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($user));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, FCPATH . 'amo_cookie/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, FCPATH . 'amo_cookie/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_TIMEOUT, 25);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $response = json_decode($out, true);

        if (($code == 200) and isset($response['response']['auth'])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function list_leads($link) {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, FCPATH . 'amo_cookie/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, FCPATH . 'amo_cookie/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_TIMEOUT, 25);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $response = json_decode($out, true);

        if (($code == 200) and isset($response['_embedded']['items'])) {
            return $response['_embedded']['items'];
        } else {
            return FALSE;
        }
    }

    public function set_leads($leads, $type) {
        if (empty($leads)) {
            return FALSE;
        }

        if (!in_array($type, array('add', 'update'))) {
            return FALSE;
        }

        $domain_amocrm = $this->config->item('domain_amocrm');

        $link = 'https://' . $domain_amocrm . '.amocrm.ru/api/v2/leads';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($leads));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, FCPATH . 'amo_cookie/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, FCPATH . 'amo_cookie/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_TIMEOUT, 25);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($type == 'add') {
            $response = json_decode($out, true);

            if (($code == 200) and isset($response['_embedded']['items'])) {
                return $response['_embedded']['items'];
            } else {
                return FALSE;
            }
        } else {
            if ($code != 200) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    public function set_notes($notes, $type) {
        if (empty($notes)) {
            return FALSE;
        }

        if (!in_array($type, array('add', 'update'))) {
            return FALSE;
        }

        $domain_amocrm = $this->config->item('domain_amocrm');

        $link = 'https://' . $domain_amocrm . '.amocrm.ru/api/v2/notes';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($notes));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, FCPATH . 'amo_cookie/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, FCPATH . 'amo_cookie/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_TIMEOUT, 25);

        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($type == 'add') {
            $response = json_decode($out, true);

            if (($code == 200) and isset($response['_embedded']['items'])) {
                return $response['_embedded']['items'];
            } else {
                return FALSE;
            }
        } else {
            if ($code != 200) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

}
