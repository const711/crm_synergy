<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shopping_cart_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('search_detail_model');
        $this->load->helper('work_helper');
    }

    public function add_to_cart($code_offer, $count_items, $id_manager, $is_custom) {
        if ($is_custom != 1) {
            $data_detail = $this->search_detail_model->get_data_detail_by_code_offer($code_offer);

            if (empty($data_detail)) {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );

                return $result;
            }
        }

        $detail_cart = $this->get_detail_cart_by_code_offer_id_manager($code_offer, $id_manager);

        if (empty($detail_cart)) {
            $this->db->trans_start();

            $order_cart = $this->get_order_cart_by_id_manager($id_manager);

            if (empty($order_cart)) {
                $data = array(
                    'id_manager' => $id_manager,
                );

                $this->db->insert('man_orders_shopping_cart', $data);

                $id_order = $this->db->insert_id();

                $data = array(
                    'article' => $data_detail['article'],
                    'brand' => $data_detail['brand'],
                    'name' => $data_detail['auto_part_name'],
                    'price' => $this->search_detail_model->calculation_retail_price($data_detail['price']),
                    'base_price' => $this->search_detail_model->calculation_retail_price($data_detail['price']),
                    'price_from_provider' => $data_detail['price'],
                    'id_warehouse_type' => $data_detail['id_warehouse_type'],
                    'quantity_from_provider' => $data_detail['quantity'],
                    'code_offer' => strip_tags($code_offer),
                    'count' => $count_items,
                    'delivery_period' => $data_detail['delivery_period'],
                    'id_manager' => $id_manager,
                    'id_order_cart' => $id_order,
                    'date' => date('Y-m-d'),
                );

                $data['base_price'] = $data['price'];
            } else {
                $data = array(
                    'article' => $data_detail['article'],
                    'brand' => $data_detail['brand'],
                    'name' => $data_detail['auto_part_name'],
                    'price' => $this->search_detail_model->calculation_retail_price($data_detail['price']),
                    'price_from_provider' => $data_detail['price'],
                    'id_warehouse_type' => $data_detail['id_warehouse_type'],
                    'quantity_from_provider' => $data_detail['quantity'],
                    'code_offer' => strip_tags($code_offer),
                    'count' => $count_items,
                    'delivery_period' => $data_detail['delivery_period'],
                    'id_manager' => $id_manager,
                    'id_order_cart' => $order_cart['id'],
                    'date' => date('Y-m-d'),
                );

                $data['base_price'] = $data['price'];
            }

            $this->db->insert('man_details_shopping_cart', $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();

                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );

                return $result;
            } else {
                $this->db->trans_commit();

                $result = array(
                    'status' => 'ok',
                );

                return $result;
            }
        } else {
            if ($is_custom != 1) {
                if (empty(preg_replace('/[0-9]/iu', '', $data_detail['quantity']))) {
                    if ((int) $data_detail['quantity'] < $count_items) {
                        $result = array(
                            'status' => 'error',
                            'message' => 'На складе больше нет деталей данного типа'
                        );

                        return $result;
                    }
                }
            }

            $data = array(
                'count' => $count_items,
                'date' => date('Y-m-d'),
            );

            $this->db->where('code_offer', $code_offer);
            $this->db->where('id_manager', $id_manager);

            if ($this->db->update('man_details_shopping_cart', $data)) {
                $result = array(
                    'status' => 'ok',
                );

                return $result;
            } else {
                $result = array(
                    'status' => 'error',
                    'message' => 'Произошла неизвестная ошибка'
                );

                return $result;
            }
        }
    }

    public function remove_from_cart($code_offer, $id_manager) {
        $detail_cart = $this->get_detail_cart_by_code_offer_id_manager($code_offer, $id_manager);

        if (empty($detail_cart)) {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );

            return $result;
        }

        if ($detail_cart['count'] <= 0) {
            $result = array(
                'status' => 'empty',
            );

            return $result;
        }

        $new_count = $detail_cart['count'] - 1;

        $data = array(
            'count' => $new_count,
            'date' => date('Y-m-d'),
        );

        $this->db->where('id_manager', $id_manager);
        $this->db->where('code_offer', $code_offer);
        $this->db->update('man_details_shopping_cart', $data);

        if ($this->db->affected_rows() > 0) {
            if ($new_count <= 0) {
                $this->del_detail_from_cart_by_id_detail_id_manager($detail_cart['id'], $id_manager);

                $order_cart = $this->get_order_cart_by_id_manager($id_manager);

                if (!empty($order_cart)) {
                    $id_order = $order_cart['id'];

                    $details_in_order = $this->get_details_cart_by_id_order($id_order, $id_manager);

                    if (empty($details_in_order)) {
                        $this->del_order_from_cart_by_id_order_id_manager($id_order, $id_manager);
                    }
                }
            }

            $result = array(
                'status' => 'ok',
            );
        } else {
            $result = array(
                'status' => 'error',
                'message' => 'Произошла неизвестная ошибка'
            );
        }

        return $result;
    }

    public function get_detail_cart_by_article_brand_name($article, $brand, $name, $id_manager) {
        $this->db->select('*');
        $this->db->from('man_details_shopping_cart');
        $this->db->where('article', $article);
        $this->db->where('brand', $brand);
        $this->db->where('name', $name);
        $this->db->where('id_manager', $id_manager);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    public function get_detail_cart_by_code_offer_id_manager($code_offer, $id_manager) {
        $this->db->select('*');
        $this->db->from('man_details_shopping_cart');
        $this->db->where('code_offer', $code_offer);
        $this->db->where('id_manager', $id_manager);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    /**
     * Функция определяет общее количество деталей в заказе
     * @param int $id_manager
     * @return int
     */
    public function count_details_shopping_cart_for_manager($id_manager) {
        $shopping_cart = $this->get_shopping_cart_by_id_manager($id_manager);
        $count = 0;

        if (isset($shopping_cart['details']) && !empty($shopping_cart['details'])) {
            foreach ($shopping_cart['details'] as $value) {
                $count = $count + $value['count'];
            }
        }

        return $count;
    }

    /**
     * Функция определяет количество наименований деталей в заказе
     * @param int $id_manager
     * @return int
     */
    public function count_rows_details_shopping_cart_for_manager($id_manager) {
        $shopping_cart = $this->get_shopping_cart_by_id_manager($id_manager);

        if (isset($shopping_cart['details']) && !empty($shopping_cart['details'])) {
            return count($shopping_cart['details']);
        } else {
            return 0;
        }
    }

    public function del_order_from_cart_by_id_order_id_manager($id_order, $id_manager) {
        $this->db->delete('man_orders_shopping_cart', array('id' => $id_order, 'id_manager' => $id_manager));

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function del_detail_from_cart_by_id_detail_id_manager($id_detail, $id_manager) {
        $this->db->delete('man_details_shopping_cart', array('id' => $id_detail, 'id_manager' => $id_manager));

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function check_orders_in_shopping_cart($shopping_cart) {
        if (isset($shopping_cart['details']) && !empty($shopping_cart['details'])) {
            $id_manager = $this->session->userdata['manager_data']['id_user'];

            foreach ($shopping_cart['details'] as $key => $value) {
                if ($value['is_custom'] == 0) {
                    $data_offer = $this->search_detail_model->get_offer_by_code_offer($value['code_offer']);

                    if (empty($data_offer)) {
                        $shopping_cart['details'][$key]['status'][] = 'not exist';

                        continue;
                    }
                }

                $date_end = strtotime("+15 day", strtotime($value['date']));
                $date_now = strtotime(date('Y-m-d'));

                if ($date_end < $date_now) {
                    $this->del_detail_from_cart_by_id_detail_id_manager($value['id_detail'], $id_manager);

                    unset($shopping_cart['details'][$key]);

                    if (empty($shopping_cart['details'])) {
                        $this->del_order_from_cart_by_id_order_id_manager($shopping_cart['id'], $id_manager);

                        $shopping_cart = array();
                    }

                    continue;
                }
            }
        }

        return $shopping_cart;
    }

    /**
     * Функция создает ссылку поставщика для конкретного предложения
     * @param string $article
     * @param string $link
     * @param string $provider
     * @return string $result
     */
    public function create_link_for_provider($article, $link, $provider) {
        if (!empty($link)) {

            $link = str_replace(' ', '', $link);

            $link = str_replace('{article}', $article, $link);

            $result = anchor('http://' . $link, $provider, array('target' => '_blank'));
        } else {
            $result = $provider;
        }

        return $result;
    }

    public function get_shopping_cart_by_id_manager($id_manager) {
        $result = $this->get_order_cart_by_id_manager($id_manager);

        if (!empty($result)) {
            $details = $this->get_details_cart_by_id_order($result['id'], $id_manager);

            if (!empty($details)) {
                foreach ($details as $key => $detail) {
                    $details[$key]['provider_name'] = $this->create_link_for_provider($detail['article'], $detail['provider_link'], $detail['provider_name']);
                }

                $result['details'] = $details;

                return $result;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    public function get_order_cart_by_id_manager($id_manager) {
        $this->db->select('*');
        $this->db->from('man_orders_shopping_cart');
        $this->db->where('id_manager', $id_manager);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    public function get_details_cart_by_id_order($id_order, $id_manager) {
        $this->db->select('man_details_shopping_cart.id AS id_detail,
                           man_details_shopping_cart.code_offer,
                           man_details_shopping_cart.article,
                           man_details_shopping_cart.brand,
                           man_details_shopping_cart.name,
                           man_details_shopping_cart.price,
                           man_details_shopping_cart.base_price,
                           man_details_shopping_cart.price_from_provider,
                           man_details_shopping_cart.id_warehouse_type,
                           man_details_shopping_cart.quantity_from_provider,
                           man_details_shopping_cart.count,
                           man_details_shopping_cart.delivery_period,
                           man_details_shopping_cart.date,
                           man_details_shopping_cart.is_changed,
                           man_details_shopping_cart.is_custom,
                           provider.name as provider_name,
                           provider.link as provider_link');
        $this->db->from('man_details_shopping_cart');
        $this->db->join('man_orders_shopping_cart', 'man_orders_shopping_cart.id = man_details_shopping_cart.id_order_cart');
        $this->db->join('type_warehouse', 'type_warehouse.id = man_details_shopping_cart.id_warehouse_type');
        $this->db->join('provider', 'provider.id = type_warehouse.id_provider');
        $this->db->where('man_orders_shopping_cart.id', $id_order);
        $this->db->where('man_orders_shopping_cart.id_manager', $id_manager);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    public function update_order_shopping_cart($data, $id_order, $id_manager) {
        $this->db->where('id', $id_order);
        $this->db->where('id_manager', $id_manager);
        $this->db->update('man_orders_shopping_cart', $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function update_detail_shopping_cart($data, $filter, $id_manager) {
        if (!empty($filter)) {
            foreach ($filter as $key => $value) {
                $this->db->where($key, $value);
            }
        }

        $this->db->where('id_manager', $id_manager);
        $this->db->update('man_details_shopping_cart', $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_detail_cart_by_id_detail_id_manager($id_detail, $id_manager) {
        $this->db->select('*');
        $this->db->from('man_details_shopping_cart');
        $this->db->where('id', $id_detail);
        $this->db->where('id_manager', $id_manager);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    public function save_custom_detail_to_cart($post, $id_manager) {
        $order_cart = $this->get_order_cart_by_id_manager($id_manager);

        if (empty($order_cart)) {
            return FALSE;
        }

        do {
            $code = create_random_code(5) . md5(date('Y-m-d H:i:s'));
        } while ($this->get_detail_cart_by_code_offer_id_manager($code, $id_manager));

        $data = array(
            'code_offer' => $code,
            'article' => strip_tags($post['article']),
            'brand' => strip_tags($post['brand']),
            'name' => strip_tags($post['name']),
            'price' => strip_tags($post['price']),
            'base_price' => strip_tags($post['base_price']),
            'price_from_provider' => strip_tags($post['price_from_provider']),
            'count' => strip_tags($post['count']),
            'id_manager' => $id_manager,
            'id_order_cart' => $order_cart['id'],
            'id_warehouse_type' => strip_tags($post['type_warehouse']),
            'is_custom' => 1,
            'date' => date('Y-m-d'),
        );

        $this->db->insert('man_details_shopping_cart', $data);

        if ($this->db->insert_id()) {
            $data['id_detail'] = $this->db->insert_id();

            return $data;
        } else {
            return FALSE;
        }
    }

    /**
     * Функция получает поставщика по id
     * @param int $id_provider
     * @return boolean
     */
    public function get_provider_by_id($id_provider) {
        $this->db->select('*');
        $this->db->from('provider');
        $this->db->where('id', $id_provider);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    public function get_provider_by_id_warehouse_type($id_warehouse_type) {
        $this->db->select('provider.*');
        $this->db->from('provider');
        $this->db->join('type_warehouse', 'type_warehouse.id_provider = provider.id');
        $this->db->where('type_warehouse.id', $id_warehouse_type);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    /**
     * Функция получает список складов поставщика
     * @param int $id_provider
     * @return array
     */
    public function get_list_warehouses_for_provider($id_provider) {
        $this->db->select('type_warehouse.id, type_warehouse.type');
        $this->db->from('type_warehouse');
        $this->db->join('provider', 'provider.id = type_warehouse.id_provider', 'left outer');
        $this->db->where('provider.id', $id_provider);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    /**
     * Функция получает список поставщиков у которых еще нет прайс-листов в БД
     * @return array
     */
    public function get_list_providers() {
        $this->db->select('*');
        $this->db->from('provider');
        $this->db->order_by('name', 'asc');

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->result_array();
    }

    public function get_type_warehouse_by_id($id_warehouse) {
        $this->db->select('*');
        $this->db->from('type_warehouse');
        $this->db->where('id', $id_warehouse);

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        if (!empty($query->row_array())) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }

    public function create_row_new_detail($data, $shopping_cart) {
        if ($shopping_cart['percent'] > 0) {
            $price = $data['price'] + ($data['price'] * $shopping_cart['percent'] / 100);
        } else {
            $price = $data['price'];
        }

        $data_provider = $this->get_provider_by_id_warehouse_type($data['id_warehouse_type']);

        $provider['name'] = $this->create_link_for_provider($data['article'], $data_provider['link'], $data_provider['name']);

        $result = '<tr class="tr_detail_row_cart detail_row_cart_' . $data['id_detail'] . '">
                        <td>
                            <input type="checkbox" class="ckb_row_detail" name="selected_details[]" value="' . $data['id_detail'] . '">
                        </td>
                        <td>' . $data['article'] . '</td>
                        <td>' . $data['brand'] . '</td>
                        <td>' . $data['name'] . '</td>
                        <td>' . $data['price_from_provider'] . '</td>
                        <td class="td_provider_name">' . $provider['name'] . '</td>
                        <td>-</td>
                        <td>-</td>
                        <td class="cell_item_price item_price_' . $data['id_detail'] . '" data-price_without_percent="' . $data['price'] . '" data-item_price="' . $price . '">
                            <span class="price_value" data-id_detail="' . $data['id_detail'] . '">' . number_format($price, 2, '.', '') . '</span>
                            <input type="text" class="price_input" name="price_input" placeholder="Цена" value="' . number_format($price, 2, '.', '') . '">
                            <img class="img_price_change" data-id_detail="' . $data['id_detail'] . '" src="' . base_url('images/tick.png') . '" title="Изменить цену" alt="Изменить цену">
                        </td>
                        <td>
                            <div class="control_counter">
                                <img class="img_minus_blue" src="' . base_url('images/minus-blue.png') . '" data-id_detail="' . $data['id_detail'] . '" data-code_offer="' . $data['code_offer'] . '" title="Уменьшить количество">
                                <input type="text" class="inp_count_items inp_' . $data['id_detail'] . '" value="' . $data['count'] . '">
                                <img class="img_plus_blue" src="' . base_url('images/plus-blue.png') . '" data-is_custom="1" data-id_detail="' . $data['id_detail'] . '" data-code_offer="' . $data['code_offer'] . '" title="Увеличить количество">
                            </div>
                        </td>';

        $result .= '<td class="cell_item_total_price item_total_price_' . $data['id_detail'] . '" data-item_total_price="' . $price * $data['count'] . '">
                        ' . number_format($price * $data['count'], 2, '.', '') . '
                    </td>
                    <td>
                        <img class="detail_changed img_is_changed hide_img_is_changed" data-id_detail="' . $data['id_detail'] . '" src="' . base_url('images/pencil.png') . '" title="Данные изменены" alt="Данные изменены">
                    </td>
                </tr>';

        return $result;
    }

    public function create_data_lead_for_update($id_lead, $data_order, $list_details, $delivery_date) {
        $sum = 0;
        $discount = $data_order['discount'];

        foreach ($list_details as $detail) {
            if ($data_order['percent'] > 0) {
                $price = $detail['price'] + ($detail['price'] * $data_order['percent'] / 100);
            } elseif ($data_order['percent'] < 0) {
                $discount = $discount + ($detail['price'] * ($data_order['percent'] * -1) / 100);
                $price = $detail['price'];
            } else {
                $price = $detail['price'];
            }

            $sum = $sum + $price * $detail['count'];
        }

        $total_sum = $sum - $discount;

        $leads['update'] = array(
            array(
                'id' => $id_lead,
                'updated_at' => time(),
                'sale' => number_format($total_sum, 2, '.', ''),
                'custom_fields' => array(
                    array(
                        'id' => 585718,
                        'values' => array(
                            array(
                                'value' => base_url('/orders_widget/search_order/1?search_order=' . $id_lead),
                            )
                        )
                    ),
                    array(
                        'id' => 607544,
                        'values' => array(
                            array(
                                'value' => $delivery_date,
                            )
                        )
                    ),
                ),
            ),
        );

        return $leads;
    }

    public function save_order_in_widget($id_deal_amo, $id_manager, $data_order, $list_details, $delivery_date) {
        $db2 = $this->load->database('db_amo_widget', TRUE);
        $array_new_details = array();

        $db2->trans_start();

        $data_new_order = array(
            'amoCrmEntityId' => $id_deal_amo,
            'discountValue' => $data_order['discount'],
            'percentValue' => $data_order['percent'],
            'deliveryDate' => strip_tags($delivery_date),
        );

        $db2->insert('amoCrmProductsWidgetLeadsDiscount', $data_new_order);

        foreach ($list_details as $detail) {
            $array_new_details[] = array(
                'amoCrmEntityId' => $id_deal_amo,
                'vendorCode' => $detail['article'],
                'brand' => $detail['brand'],
                'name' => $detail['name'],
                'requestPrice' => $detail['price_from_provider'],
                'provider' => $detail['provider_name'],
                'providerLink' => $detail['provider_link'],
                'retailPrice' => $detail['price'],
                'baseRequestPrice' => $detail['price_from_provider'],
                'baseRetailPrice' => $detail['base_price'],
                'quantity' => $detail['count'],
                'created_at' => date('Y-m-d H:i:s'),
            );
        }

        $db2->insert_batch('amoCrmProductsWidgetRows', $array_new_details);

        if ($db2->trans_status() === FALSE) {
            $db2->trans_rollback();

            return FALSE;
        } else {
            $db2->trans_commit();

            $this->del_order_from_cart_by_id_order_id_manager($data_order['id'], $id_manager);

            return TRUE;
        }
    }

    public function get_order_widget_by_id_deal_amo($id_deal_amo) {
        $db2 = $this->load->database('db_amo_widget', TRUE);

        $db2->select('*');
        $db2->from('amoCrmProductsWidgetLeadsDiscount');
        $db2->where('amoCrmEntityId', $id_deal_amo);

        $query = $db2->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

    /**
     * Высчитывает общую сумму и прибыль заказа
     * @param array $shopping_cart
     * @return array
     */
    public function calculate_total_sum_and_profit($shopping_cart) {
        if (isset($shopping_cart['details']) && !empty($shopping_cart['details'])) {
            $total_sum = 0;
            $base_total_sum = 0;
            $profit = 0;
            $discount = $shopping_cart['discount'];

            foreach ($shopping_cart['details'] as $detail) {
                if ($shopping_cart['percent'] > 0) {
                    $price = $detail['price'] + ($detail['price'] * $shopping_cart['percent'] / 100);
                } elseif ($shopping_cart['percent'] < 0) {
                    $discount = $discount + ($detail['price'] * ($shopping_cart['percent'] * -1) / 100);
                    $price = $detail['price'];
                } else {
                    $price = $detail['price'];
                }

                $total_sum = $total_sum + $price * $detail['count'];
                $base_total_sum = $base_total_sum + $detail['base_price'] * $detail['count'];
            }

            $total_sum = $total_sum - $discount;

            if ($base_total_sum > 3000) {
                $profit = $total_sum - $base_total_sum;

                if ($profit < -300) {
                    $profit = $profit + 300;
                } elseif(($profit < 0) && ($profit > -300)) {
                    $profit = 0;
                }
            } else {
                $profit = $total_sum - $base_total_sum;
            }
        } else {
            $total_sum = 0;
            $profit = 0;
        }

        return array(
            'total_sum' => number_format($total_sum, 2, '.', ''),
            'profit' => number_format($profit, 2, '.', ''),
        );
    }

    /**
     * Функция создает массив данных для добавления нового примечания к сделке в amocrm
     * @param string $id_deal_amo
     * @param array $data_order
     * @param array $list_details
     * @param string $delivery_date
     * @return array $notes
     */
    public function create_data_note_amocrm_for_add($id_deal_amo, $data_order, $list_details, $delivery_date) {
        $text = '';

        $sum = 0;
        $discount = $data_order['discount'];

        $text .= 'Данные заказа:' . "\n";

        foreach ($list_details as $detail) {
            if ($data_order['percent'] > 0) {
                $price = $detail['price'] + ($detail['price'] * $data_order['percent'] / 100);
            } elseif ($data_order['percent'] < 0) {
                $discount = $discount + ($detail['price'] * ($data_order['percent'] * -1) / 100);
                $price = $detail['price'];
            } else {
                $price = $detail['price'];
            }

            $sum = $sum + $price * $detail['count'];

            $price = number_format($price, 2, '.', '');

            $text .= $detail['article'] . ' ' . $detail['brand'] . ' ' . $price . ' ' . $detail['provider_name'];

            if (!empty($detail['delivery_period'])) {
                $text .= ' ' . $detail['delivery_period'] . 'дн.';
            } else {
                $text .= ' - дн.';
            }

            if (!empty($detail['count'])) {
                $text .= ' ' . $detail['count'] . " шт. " . "\n";
            } else {
                $text .= ' - шт. ' . "\n";
            }
        }

        $total_sum = $sum - $discount;

        $text .= 'Наценка: ' . $data_order['percent'] . "\n";
        $text .= 'Скидка: ' . $discount . "\n";
        $text .= 'Сумма заказа: ' . number_format($total_sum, 2, '.', '') . "\n";

        $notes['add'] = array(
            array(
                'element_id' => $id_deal_amo,
                'element_type' => 2,
                'note_type' => 4,
                'text' => $text,
                'responsible_user_id' => 469183
            ),
        );

        return $notes;
    }
}
