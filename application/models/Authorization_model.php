<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Authorization_model extends CI_Model {

    /**
     * Функция проверяет авторизован ли менеджер
     * @return boolean
     */
    public function check_auth() {
        if (isset($this->session->userdata['manager_data']['id_user'])
                and isset($this->session->userdata['manager_data']['type_user'])
                    and ($this->session->userdata['manager_data']['type_user'] == 'manager')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Функция находит менеджера в БД по email и паролю
     * @param string $email_login
     * @param string $password_login
     * @return array
     */
    public function get_manager_by_email_pass($email_login, $password_login) {
        $this->db->select('*');
        $this->db->from('managers');
        $this->db->where('email', $email_login);
        $this->db->where('pass', md5($password_login));

        $query = $this->db->get();

        if (!$query) {
            return false;
        }

        return $query->row_array();
    }

}
