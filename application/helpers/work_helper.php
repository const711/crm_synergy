<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Функция оставляет в строке только цифры и маленькие буквы лат. алфавита 
 * @param string $str
 * @return string $str
 */
if (!function_exists('leave_letters_numbers')) {

    function leave_eng_letters_numbers($str) {
        $str = preg_replace("/[^a-z0-9]/iu", "", $str);

        return $str;
    }

}

/**
 * Функция обрезает строку до указанной длинны и прикрепляет к строке троеточие
 * @param string $str
 * @param int $length
 * @param string $postfix
 * @param string $encoding
 * @return string
 */
if (!function_exists('cut_string')) {

    function cut_string($str, $length, $postfix = '...', $encoding = 'UTF-8') {
        if (mb_strlen($str, $encoding) <= $length) {
            return $str;
        }

        $tmp = mb_substr($str, 0, $length, $encoding);

        $position_last_space = mb_strripos($tmp, ' ', 0, $encoding);

        if ($position_last_space === FALSE) {
            return $tmp . $postfix;
        } else {
            return mb_substr($tmp, 0, $position_last_space, $encoding) . $postfix;
        }
    }

}

/**
 * Функция генерирует рандомную строку заданной длины
 * @param int $length
 * @return string $string
 */
if (!function_exists('create_random_code')) {

    function create_random_code($length = 1) {
        $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ0123456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }

        return $string;
    }

}

/**
 * Функция заменяет символы в строке на символ "звездочка"
 * @param string $str
 * @param int $start
 * @param int $end
 * @return string $str_res
 */
if (!function_exists('change_symbols_on_stars')) {

    function change_symbols_on_stars($str, $start = 0, $end = 0) {
        $str_res = '';

        for ($i = 0; $i < mb_strlen($str, "UTF-8"); $i++) {
            if ($end > 0 and $end > $start) {
                if ($i >= $start and $i < $end) {
                    $str_res .= '*';
                } else {
                    $str_res .= mb_substr($str, $i, 1, "UTF-8");
                }
            } else {
                if ($i >= $start) {
                    $str_res .= '*';
                } else {
                    $str_res .= mb_substr($str, $i, 1, "UTF-8");
                }
            }
        }

        return $str_res;
    }

}

/**
 * Функция удаляет все лишние символы в цене
 * @param string $price
 * @return string $price
 */
if (!function_exists('standard_price')) {

    function standard_price($price) {
        $price = preg_replace('/[^0-9.,]/iu', '', $price);
        $price = trim($price, '.');
        $price = preg_replace('/,/iu', '.', $price);

        return $price;
    }

}
