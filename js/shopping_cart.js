$(function () {
    $(document).on("click", "#form_list_details .btn_add_in_shopping_card", function () {
        var element = $(this);
        var code_offer = element.data('code_offer');
        var count_items_cart = Number(element.find('.add_item_in_cart').data('count_items_cart'));
        var new_count_items = count_items_cart + 1;
        element.prop('disabled', true);
        element.find('.loader_add_to_cart').show();
        element.find('.add_item_in_cart').hide();

        $.post(base_url + "index.php/shopping_cart/add_to_cart", {code_offer: code_offer, is_custom: 0, new_count_items: new_count_items}, function (event) {
            element.prop('disabled', false);
            element.find('.loader_add_to_cart').hide();
            element.find('.add_item_in_cart').show();

            if (event.status == 'ok') {
                element.find('.add_item_in_cart').data('count_items_cart', new_count_items).html('(' + new_count_items + ')').show();

                $('.cart-btn .count_rows_details_cart').html(event.count_rows_details_shopping_cart);
                $('.cart-btn .count_rows_details_cart').removeClass('hide_counter');
            } else {
                element.popover({
                    'content': event.message,
                    'placement': 'bottom',
                    'html': true,
                    'trigger': "hover",
                    'template': '<div class="popover popover_error_add_in_card"> \
                                        <div class="arrow"></div> \
                                        <div class="popover-inner"> \
                                                <div class="popover-content"> \
                                                        <p></p> \
                                                </div> \
                                        </div> \
                                    </div>'
                }).popover('show');
            }
        }, 'json');
    });

    $(".list_shopping_cart .inp_count_items").blur(function () {
        var element = $(this);
        var code_offer = element.data('code_offer');
        var id_detail = element.data('id_detail');
        var count_items = Number($('.list_shopping_cart .inp_' + id_detail).val());
        var old_count_items = Number($('.list_shopping_cart .inp_' + id_detail).data('old_count'));
        var item_price = Number($('.list_shopping_cart .item_price_' + id_detail).data('item_price'));
        var item_total_price = Number($('.list_shopping_cart .item_total_price_' + id_detail).data('item_total_price'));
        var total_sum = Number($('.list_shopping_cart .total_sum').data('total_sum'));
        var is_custom = element.data('is_custom');

        if (count_items != old_count_items) {
            $.post(base_url + "index.php/shopping_cart/add_to_cart", {code_offer: code_offer, is_custom: is_custom, new_count_items: count_items}, function (event) {
                if (event.status == 'ok') {
                    var new_item_total_price = (item_price * count_items);
                    $('.list_shopping_cart .inp_' + id_detail).attr('data-old_count', count_items).data('old_count', count_items);;
                    $('.list_shopping_cart .item_total_price_' + id_detail).data('item_total_price', new_item_total_price).html(new_item_total_price.toFixed(2));
                    var new_total_sum = event.total_sum;

                    $('.list_shopping_cart .total_sum').data('total_sum', new_total_sum).html(new_total_sum);
                    $('#form_shopping_cart .profit_tag').removeClass('profit_bad').removeClass('profit_good');

                    if (event.profit < 0) {
                        $('#form_shopping_cart .profit_tag').addClass('profit_bad');
                    } else {
                        $('#form_shopping_cart .profit_tag').addClass('profit_good');
                    }

                    $('#form_shopping_cart .profit_tag').html(event.profit + ' руб.');
                } else {
                    $('.list_shopping_cart .inp_' + id_detail).val(old_count_items);

                    alert(event.message);
                }
            }, 'json');
        }
    });

    $(document).on("click", ".list_shopping_cart .img_plus_blue", function () {
        var element = $(this);
        var code_offer = element.data('code_offer');
        var id_detail = element.data('id_detail');
        var count_items = Number($('.list_shopping_cart .inp_' + id_detail).val());
        var item_price = Number($('.list_shopping_cart .item_price_' + id_detail).data('item_price'));
        var item_total_price = Number($('.list_shopping_cart .item_total_price_' + id_detail).data('item_total_price'));
        var total_sum = Number($('.list_shopping_cart .total_sum').data('total_sum'));
        var is_custom = element.data('is_custom');
        var new_count_items = count_items + 1;

        $.post(base_url + "index.php/shopping_cart/add_to_cart", {code_offer: code_offer, is_custom: is_custom, new_count_items: new_count_items}, function (event) {
            if (event.status == 'ok') {
                $('.list_shopping_cart .inp_' + id_detail).val(new_count_items);
                $('.list_shopping_cart .inp_' + id_detail).attr('data-old_count', new_count_items).data('old_count', new_count_items);
                var new_item_total_price = item_total_price + item_price;
                $('.list_shopping_cart .item_total_price_' + id_detail).data('item_total_price', new_item_total_price).html(new_item_total_price.toFixed(2));
                var new_total_sum = total_sum + item_price;
                $('.list_shopping_cart .total_sum').data('total_sum', new_total_sum).html(new_total_sum.toFixed(2));

                $('#form_shopping_cart .profit_tag').removeClass('profit_bad').removeClass('profit_good');

                if (event.profit < 0) {
                    $('#form_shopping_cart .profit_tag').addClass('profit_bad');
                } else {
                    $('#form_shopping_cart .profit_tag').addClass('profit_good');
                }

                $('#form_shopping_cart .profit_tag').html(event.profit + ' руб.');
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $(document).on("click", ".list_shopping_cart .img_minus_blue", function () {
        var element = $(this);
        var code_offer = element.data('code_offer');
        var id_detail = element.data('id_detail');
        var count_items = Number($('.list_shopping_cart .inp_' + id_detail).val());
        var item_price = Number($('.list_shopping_cart .item_price_' + id_detail).data('item_price'));
        var item_total_price = Number($('.list_shopping_cart .item_total_price_' + id_detail).data('item_total_price'));
        var total_sum = Number($('.list_shopping_cart .total_sum').data('total_sum'));

        $.post(base_url + "index.php/shopping_cart/remove_from_cart", {code_offer: code_offer}, function (event) {
            if (event.status == 'ok') {
                var new_count_items = count_items - 1;

                $('.list_shopping_cart .inp_' + id_detail).val(new_count_items);
                $('.list_shopping_cart .inp_' + id_detail).attr('data-old_count', new_count_items).data('old_count', new_count_items);
                var new_item_total_price = item_total_price - item_price;
                $('.list_shopping_cart .item_total_price_' + id_detail).data('item_total_price', item_total_price - item_price).html(new_item_total_price.toFixed(2));
                var new_total_sum = total_sum - item_price;
                $('.list_shopping_cart .total_sum').data('total_sum', total_sum - item_price).html(new_total_sum.toFixed(2));

                $('.count_rows_details_cart').html(event.count_rows_details_shopping_cart);

                if (new_count_items == 0) {
                    $('.list_shopping_cart .detail_row_cart_' + id_detail).remove();
                }

                if (event.count_rows_details_shopping_cart == 0) {
                    $('.count_rows_details_cart').addClass('hide_counter');
                }

                $('#form_shopping_cart .profit_tag').removeClass('profit_bad').removeClass('profit_good');

                if (event.profit < 0) {
                    $('#form_shopping_cart .profit_tag').addClass('profit_bad');
                } else {
                    $('#form_shopping_cart .profit_tag').addClass('profit_good');
                }

                $('#form_shopping_cart .profit_tag').html(event.profit + ' руб.');
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $('.list_shopping_cart #check_all_rows').click(function (event) {
        if ($(this).is(':checked')) {
            $('.ckb_row_detail').prop('checked', true);
        } else {
            $('.ckb_row_detail').prop('checked', false);
        }
    });

    $("#form_shopping_cart").submit(function (event) {
        if (!confirm("Вы уверены что хотите удалить?")) {
            return false;
        }
    });

    $(document).on("click", "#form_shopping_cart .btn_add_discount", function () {
        $.post(base_url + "index.php/shopping_cart/add_discount", {need_add_discount: 1}, function (event) {
            if (event.status == 'ok') {
                $('#form_shopping_cart .total_sum').html(event.total_sum);
                $('#form_shopping_cart .discount_value').html('300.00');
                $('#form_shopping_cart .discount_input').val(300);
                $('#form_shopping_cart .profit_tag').removeClass('profit_bad').removeClass('profit_good');

                if (event.profit < 0) {
                    $('#form_shopping_cart .profit_tag').addClass('profit_bad');
                } else {
                    $('#form_shopping_cart .profit_tag').addClass('profit_good');
                }

                $('#form_shopping_cart .profit_tag').html(event.profit + 'руб.');

                $('#form_shopping_cart .discount_row').show();
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $(document).on("click", "#form_shopping_cart .img_discount_delete", function () {
        $.post(base_url + "index.php/shopping_cart/del_discount", {need_del_discount: 1}, function (event) {
            if (event.status == 'ok') {
                $('#form_shopping_cart .total_sum').html(event.total_sum);
                $('#form_shopping_cart .discount_value').html('300.00');
                $('#form_shopping_cart .discount_row').hide();
                $('#form_shopping_cart .profit_tag').removeClass('profit_bad').removeClass('profit_good');

                if (event.profit < 0) {
                    $('#form_shopping_cart .profit_tag').addClass('profit_bad');
                } else {
                    $('#form_shopping_cart .profit_tag').addClass('profit_good');
                }

                $('#form_shopping_cart .profit_tag').html(event.profit + 'руб.');
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $(document).on("click", "#form_shopping_cart .btn_add_price_percent", function () {
        var price_percent = $('#form_shopping_cart .input_add_price_percent').val().replace(/[^0-9,\.\-]/iug, '').replace(/,/iug, '.');
                if (price_percent != '') {
            $.post(base_url + "index.php/shopping_cart/add_price_percent", {price_percent: price_percent}, function (event) {
                if (event.status == 'ok') {
                    $('#form_shopping_cart .total_sum').html(event.total_sum).data('total_sum', event.total_sum);

                    $("#form_shopping_cart .tr_detail_row_cart").each(function (index) {
                        var tr = $(this);

                        var item_price = Number(tr.find('.cell_item_price').data('price_without_percent'));
                        var count_items = tr.find('.inp_count_items').val();
                        var new_item_price = item_price + (item_price * price_percent / 100);
                        var new_item_total_price = new_item_price * count_items;

                        tr.find('.cell_item_price').attr('data-item_price', new_item_price).html(new_item_price.toFixed(2));
                        tr.find('.cell_item_total_price').attr('data-item_total_price', new_item_total_price).html(new_item_total_price.toFixed(2));
                    });

                    $('#form_shopping_cart .profit_tag').removeClass('profit_bad').removeClass('profit_good');

                    if (event.profit < 0) {
                        $('#form_shopping_cart .profit_tag').addClass('profit_bad');
                    } else {
                        $('#form_shopping_cart .profit_tag').addClass('profit_good');
                    }

                    $('#form_shopping_cart .profit_tag').html(event.profit + 'руб.');

                    alert(event.message);
                } else {
                    alert(event.message);
                }
            }, 'json');
        } else {
            alert('Введены некорректные данные!');
        }
    });

    $(document).on("click", "#form_shopping_cart .detail_changed", function () {
        var element = $(this);
        var id_detail = element.data('id_detail');

        $.post(base_url + "index.php/shopping_cart/reset_detail_changed", {id_detail: id_detail}, function (event) {
            if (event.status == 'ok') {
                element.hide();
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $(document).on("click", "#form_shopping_cart .discount_changed", function () {
        var element = $(this);

        $.post(base_url + "index.php/shopping_cart/reset_discount_changed", {need_reset_discount_changed: 1}, function (event) {
            if (event.status == 'ok') {
                element.addClass('hide_img_is_changed');
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $(document).on("click", "#form_shopping_cart .price_value", function () {
        hide_all_inputs_change_order();

        var element = $(this);
        var id_detail = element.data('id_detail');

        element.hide();
        $('.detail_row_cart_' + id_detail + ' .price_input').show();
        $('.detail_row_cart_' + id_detail + ' .img_price_change').show();
    });

    $(document).on("click", "#form_shopping_cart .img_price_change", function () {
        var element = $(this);
        var id_detail = element.data('id_detail');
        var new_price = $('.item_price_' + id_detail + ' .price_input').val();

        $.post(base_url + "index.php/shopping_cart/price_change", {id_detail: id_detail, new_price: new_price}, function (event) {
            if (event.status == 'ok') {
                $('.item_price_' + id_detail).attr('data-item_price', new_price);
                $('.item_price_' + id_detail + ' .price_value').html(new_price).show();
                $('#form_shopping_cart .total_sum').html(event.total_sum).data('total_sum', event.total_sum);
                $('.item_price_' + id_detail + ' .price_input').val(new_price);
                $('.item_price_' + id_detail + ' .price_input').hide();
                $('.item_price_' + id_detail + ' .img_price_change').hide();
                $('.detail_row_cart_' + id_detail + ' .img_is_changed').show();
                var count_items = $('.detail_row_cart_' + id_detail + ' .inp_count_items').val();
                var new_item_total_price = new_price * count_items;
                $('.item_total_price_' + id_detail).attr('data-item_total_price', new_item_total_price.toFixed(2)).html(new_item_total_price.toFixed(2));
                $('#form_shopping_cart .profit_tag').removeClass('profit_bad').removeClass('profit_good');

                if (event.profit < 0) {
                    $('#form_shopping_cart .profit_tag').addClass('profit_bad');
                } else {
                    $('#form_shopping_cart .profit_tag').addClass('profit_good');
                }

                $('#form_shopping_cart .profit_tag').html(event.profit + 'руб.');
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $(document).on("click", "#form_shopping_cart .discount_value", function () {
        hide_all_inputs_change_order();

        var element = $(this);
        element.hide();
        $('#form_shopping_cart .discount_currency').hide();
        $('#form_shopping_cart .discount_input').show();
        $('#form_shopping_cart .img_discount_change').show();
    });

    $('#form_shopping_cart .img_discount_change').click(function (event) {
        var new_discount = $('#form_shopping_cart .discount_input').val();

        $.post(base_url + "index.php/shopping_cart/discount_change", {new_discount: new_discount}, function (event) {
            if (event.status == 'ok') {
                $('#form_shopping_cart .discount_value').show();
                $('#form_shopping_cart .discount_currency').show();
                $('#form_shopping_cart .discount_input').hide();
                $('#form_shopping_cart .img_discount_change').hide();
                $('#form_shopping_cart .total_sum').html(event.total_sum).attr('data-total_sum', event.total_sum);
                $('#form_shopping_cart .discount_input').val(new_discount);
                $('#form_shopping_cart .discount_value').html(new_discount).show();
                $('#form_shopping_cart .profit_tag').removeClass('profit_bad').removeClass('profit_good');

                if (event.profit < 0) {
                    $('#form_shopping_cart .profit_tag').addClass('profit_bad');
                } else {
                    $('#form_shopping_cart .profit_tag').addClass('profit_good');
                }

                $('#form_shopping_cart .profit_tag').html(event.profit + 'руб.');
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    var pathname = window.location.pathname;
    if (pathname.search(/shopping_cart\/shopping_cart/i) >= 0) {
        $('html').click(function (e) {
            if (!$(e.target).is("#form_shopping_cart .discount_value")
                    && !$(e.target).is("#form_shopping_cart .img_discount_change") && !$(e.target).is("#form_shopping_cart .discount_input")) {
                $('#form_shopping_cart .discount_value').show();
                $('#form_shopping_cart .discount_currency').show();
                $('#form_shopping_cart .discount_input').hide();
                $('#form_shopping_cart .img_discount_change').hide();
            }

            if (!$(e.target).is("#form_shopping_cart .price_value")
                    && !$(e.target).is("#form_shopping_cart .img_price_change") && !$(e.target).is("#form_shopping_cart .price_input")) {
                $("#form_shopping_cart .price_value").show();
                $("#form_shopping_cart .price_input").hide();
                $("#form_shopping_cart .img_price_change").hide();
            }
        });
    }

    $('#form_shopping_cart .btn_confirm_order').click(function (event) {
        var id_deal_amo = $('#form_shopping_cart .inp_id_deal_amo').val();
        var delivery_date = $('#form_shopping_cart .inp_delivery_date').val();
        $(this).hide();
        $('#form_shopping_cart .loader_confirm_order').show();

        if (typeof id_deal_amo != 'undefined' && id_deal_amo != '') {
            $.post(base_url + "index.php/shopping_cart/create_order", {id_deal_amo: id_deal_amo, delivery_date: delivery_date}, function (event) {
                if (event.status == 'ok') {
                    alert(event.message);

                    document.location.href = base_url + 'index.php/search_detail/index';
                } else {
                    alert(event.message);
                }
            }, 'json');
        } else {
            alert('Укажите правильный id сделки');
            $('#form_shopping_cart .loader_confirm_order').hide();
            $(this).show();
        }
    });

    $('#form_shopping_cart .btn_add_new_detail').click(function (event) {
        $('#form_add_new_detail #type_warehouse').html();
        $('#form_add_new_detail .select_type_warehouse').hide();
        $('#form_add_new_detail').trigger('reset');
        $('#pop_up_add_new_detail').modal('show');
    });

    $("#form_add_new_detail").submit(function (event) {
        event.preventDefault();
        $('.form_error').html('');

        var error_flag = 0;

        if ($('#form_add_new_detail #article').val() == '') {
            $('#form_add_new_detail .article_form_error').append('<p>Заполните поле "Артикул"</p>');
            error_flag++;
        }

        if ($('#form_add_new_detail #brand').val() == '') {
            $('#form_add_new_detail .brand_form_error').append('<p>Заполните поле "Бренд"</p>');
            error_flag++;
        }

        if ($('#form_add_new_detail #name').val() == '') {
            $('#form_add_new_detail .name_form_error').append('<p>Заполните поле "Название"</p>');
            error_flag++;
        }

        if ($('#form_add_new_detail #type_warehouse').val() == '') {
            $('#form_add_new_detail .type_warehouse_form_error').append('<p>Заполните поле "Тип склада"</p>');
            error_flag++;
        }

        if ($('#form_add_new_detail #price_from_provider').val() == '') {
            $('#form_add_new_detail .price_from_provider_form_error').append('<p>Заполните поле "Цена от поставщика"</p>');
            error_flag++;
        }

        if ($('#form_add_new_detail #count').val() == '') {
            $('#form_add_new_detail .count_form_error').append('<p>Заполните поле "Количество"</p>');
            error_flag++;
        }

        if (error_flag != 0) {
            return false
        } else {
            $.post(base_url + "index.php/shopping_cart/add_new_detail", $('#form_add_new_detail').serialize(), function (event) {
                if (event.status == 'ok') {
                    $('.list_shopping_cart .last_row_order').after(event.row_new_detail);

                    $('#form_shopping_cart .total_sum').html(event.total_sum).attr('data-total_sum', event.total_sum);
                    $('#form_shopping_cart .profit_tag').removeClass('profit_bad').removeClass('profit_good');

                    if (event.profit < 0) {
                        $('#form_shopping_cart .profit_tag').addClass('profit_bad');
                    } else {
                        $('#form_shopping_cart .profit_tag').addClass('profit_good');
                    }

                    $('#form_shopping_cart .profit_tag').html(event.profit + 'руб.');

                    $('#form_add_new_detail #type_warehouse').html();
                    $('#form_add_new_detail .select_type_warehouse').hide();
                    $('#form_add_new_detail').trigger('reset');
                    $('#pop_up_add_new_detail').modal('hide');
                } else if (event.status == 'error_empty_input') {
                    $.each(event.errors_array, function (index, value) {
                        $('#form_add_new_detail .' + index + '_form_error').html(value);
                    });
                } else {
                    $('#form_add_new_detail .main_form_error').html(event.message);
                }
            }, 'json');
        }
    });

    $('#form_add_new_detail #provider').change(function () {
        var id_provider = $(this).val();

        get_list_type_warehouses_for_provider(id_provider);
    });
});

/* Функция, которая получает типы складов для выбранного поставщика в форме по загрузке нового прайса  */

function get_list_type_warehouses_for_provider(id_provider) {
    $('#form_add_new_detail #type_warehouse').html('');

    if (id_provider != '') {
        $.post(base_url + "index.php/shopping_cart/get_warehouse_for_provider", {'id_provider': id_provider}, function (event) {
            if (event.status == 'ok') {
                var list_warehouses = event.list_warehouses;

                if (list_warehouses.length > 0) {
                    var options = "<option value=''>Выберите тип склада</option>";

                    $.each(list_warehouses, function (key, value) {
                        options = options + "<option value='" + value.id + "'>" + value.type + "</option>";
                    });

                    $('#form_add_new_detail #type_warehouse').html(options);

                    $('#form_add_new_detail .select_type_warehouse').show();
                } else {
                    $('#form_add_new_detail .select_type_warehouse').hide();
                }
            } else {
                $('#form_add_new_detail .main_form_error').html(event.message);
                $('#form_add_new_detail .select_type_warehouse').hide();
            }
        }, "json");
    } else {
        $('#form_add_new_detail .select_type_warehouse').hide();
    }
}

function hide_all_inputs_change_order() {
    $('#form_shopping_cart .discount_value').show();
    $('#form_shopping_cart .discount_currency').show();
    $('#form_shopping_cart .discount_input').hide();
    $('#form_shopping_cart .img_discount_change').hide();

    $("#form_shopping_cart .price_value").show();
    $("#form_shopping_cart .price_input").hide();
    $("#form_shopping_cart .img_price_change").hide();
}