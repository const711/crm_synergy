$(function () {
    var pathname = window.location.pathname;

    /* Скрипт для формы поиска детали (НАЧАЛО) */

    $("#search_detail_form").submit(function (event) {
        $('.form_error').html('');

        if ($('#article_detail').val() == '') {
            $('.form_error').append('<p>Заполните поле "Артикул"</p>');
            return false;
        }
    });

    /* Скрипт для формы поиска детали (КОНЕЦ) */

    /* Скрипт для формы поиска детали, которая находится в верхнем меню (НАЧАЛО) */

    $("#search_form_top").submit(function (event) {

        var data = $("#search_form_top input[name='article_detail']").val();

        if (data == '') {
            $("#search_form_top input[name='article_detail']").css('border', '1px solid red').val('').attr("placeholder", "Введите артикул");

            return false;
        }
    });

    /* Скрипт для формы поиска детали, которая находится в верхнем меню (КОНЕЦ) */

    /* Скрипт для обновления списка найденных брендов (НАЧАЛО) */

    $('.control_panel_list_brands .need_update_list_brands').click(function (event) {
        $('.control_panel_list_brands .need_update_list_brands').hide();
        $('.control_panel_list_brands .img_loader').show();
        $.post(base_url + "index.php/search_detail/update_list_brands", {need_update: 1, article_search: $(this).data('article_search')}, function (event) {
            if (event == 'error') {
                alert('Произошла ошибка!');

                $('.control_panel_list_brands .need_update_list_brands').show();
                $('.control_panel_list_brands .img_loader').hide();
            } else {
                alert('Данные обновлены успешно!');
                window.location.reload();
            }
        }, 'json');
    });

    /* Скрипт для обновления списка найденных брендов (КОНЕЦ) */

    /* Скрипт для обновления списка найденных деталей (НАЧАЛО) */

    $('.control_panel_list_details .need_update_list_details').click(function (event) {
        $('.control_panel_list_details .need_update_list_details').hide();
        $('.control_panel_list_details .img_loader').show();
    });

    /* Скрипт для обновления списка найденных деталей (КОНЕЦ) */

    /* Скрипт для страницы найденных деталей (НАЧАЛО) */

    $('.btn_show_all_offers').click(function (event) {
        window.location.reload();
    });

    $(document).on("click", '#form_list_details .row_detail a', function () {
        event.stopPropagation();
    });

    $(document).on("click", "#form_list_details .row_offer", function (e) {
        if (!$(e.target).is(".btn_add_in_shopping_card")) {
            $(this).toggleClass('selected_offer');

            var id_offer = $(this).data('id_offer');

            if (!$('#checkbox_for_row_offer_' + id_offer).is(':checked')) {
                $('#checkbox_for_row_offer_' + id_offer).prop('checked', true);
            } else {
                $('#checkbox_for_row_offer_' + id_offer).prop('checked', false);
            }
        }
    });

    $('#form_edit_brand').trigger('reset');

    show_popover_for_list_details();

    $(".filter_by_brand .more_brands").click(function (event) {
        if ($(".filter_by_brand .list_brands_for_filter").css('overflow') == 'hidden') {
            $(".filter_by_brand .list_brands_for_filter").css('overflow', 'auto');
            $(".filter_by_brand .more_brands").text('Скрыть бренды');
        } else {
            $(".filter_by_brand .list_brands_for_filter").scrollTop(0);
            $(".filter_by_brand .list_brands_for_filter").css('overflow', 'hidden');
            $(".filter_by_brand .more_brands").text('Все бренды');
        }
    });

    $(document).on("click", ".search_list_details .show_detail_offers", function (e) {
        var id_detail = $(this).data("id_detail");
        $(".search_list_details .hidden_offers_detail_" + id_detail).toggle();
        $("#show_list_offers_" + id_detail).toggle();
        $("#hide_list_offers_" + id_detail).toggle();
    });

    $(document).on("click", ".search_list_details .show_more_names_detail", function (e) {
        var id_detail = $(this).data("id_detail");

        $(".search_list_details .offer_for_detail_" + id_detail + " .hidden_offer_description").toggle();
    });

    $(".filter_by_brand input[type='checkbox']:not(:first)").prop('checked', false);

    $(".filter_by_brand input[type='checkbox']").change(function () {
        filter_by_brand();
    });

    $(document).on("click", ".dropdown .dropdown-menu li", function (e) {
        var type_sort = $(this).parents('.dropdown').find('input').attr('value');

        var intervals_deliv_period = [];

        $.each($(".body_filter_by_delivery_period input[type='checkbox']:checked"), function () {
            intervals_deliv_period.push($(this).val());
        });

        var type_sort = $('.sort_list_details .dropdown').find('input').val();

        ajax_send_filter_for_list_details(article, key_group, intervals_deliv_period, price_between_beg, price_between_end, search_is_complete, type_sort, type_warehouse, recom_brands, private_data);
    });

    if (pathname.search(/search_detail\/list_details/i) >= 0) {
        $(".div_top_filter .filter_by_price").slider({
            range: true,
            min: 0,
            max: max_price_for_filter,
            values: [price_between_beg, price_between_end],
            change: function (event, ui) {
                var intervals_deliv_period = [];
                $.each($(".body_filter_by_delivery_period input[type='checkbox']:checked"), function () {
                    intervals_deliv_period.push($(this).val());
                });
                price_between_beg = ui.values[ 0 ];
                price_between_end = ui.values[ 1 ];
                var type_sort = $('.sort_list_details .dropdown').find('input').val();

                ajax_send_filter_for_list_details(article, key_group, intervals_deliv_period, price_between_beg, price_between_end, search_is_complete, type_sort, type_warehouse, recom_brands, private_data);
            },
            slide: function (event, ui) {
                $('.body_filter_by_price .price_between_beg').text(ui.values[ 0 ]);
                $('.body_filter_by_price .price_between_end').text(ui.values[ 1 ]);
            }
        });
    }

    $(".div_top_filter .show_filter_details").click(function (event) {
        if ($('.div_top_filter .filter_body').is(':visible')) {
            $('.div_top_filter .filter_body').hide();
            $(this).text('Показать фильтр');
        } else {
            $('.div_top_filter .filter_body').show();
            $(this).text('Скрыть фильтр');
        }
    });

    $(".body_filter_by_delivery_period input[type='checkbox']").prop('checked', false);

    $(".body_filter_by_delivery_period input[type='checkbox']").change(function () {
        var intervals_deliv_period = [];

        $.each($(".body_filter_by_delivery_period input[type='checkbox']:checked"), function () {
            intervals_deliv_period.push($(this).val());
        });

        var type_sort = $('.sort_list_details .dropdown').find('input').val();

        ajax_send_filter_for_list_details(article, key_group, intervals_deliv_period, price_between_beg, price_between_end, search_is_complete, type_sort, type_warehouse, recom_brands, private_data);
    });

    $(".div_top_filter .btn_sort_by_retail_price").click(function (event) {
        event.preventDefault();
        var type_sort = 2;
        var intervals_deliv_period = [];

        $.each($(".body_filter_by_delivery_period input[type='checkbox']:checked"), function () {
            intervals_deliv_period.push($(this).val());
        });

        ajax_send_filter_for_list_details(article, key_group, intervals_deliv_period, price_between_beg, price_between_end, search_is_complete, type_sort, type_warehouse, recom_brands, private_data);
    });

    $(".div_top_filter .btn_sort_by_delivery_period").click(function (event) {
        event.preventDefault();
        var type_sort = 4;
        var intervals_deliv_period = [];

        $.each($(".body_filter_by_delivery_period input[type='checkbox']:checked"), function () {
            intervals_deliv_period.push($(this).val());
        });

        ajax_send_filter_for_list_details(article, key_group, intervals_deliv_period, price_between_beg, price_between_end, search_is_complete, type_sort, type_warehouse, recom_brands, private_data);
    });

    /* Скрипт для страницы найденных деталей (КОНЕЦ) */

    /* Скрипты для объединения брендов на странице со списком брендов (НАЧАЛО) */

    $("#form_merge_brands_in_brands_list .brand_row").click(function (event) {
        $(this).toggleClass('selected_brand');
        var id_row_brand = $(this).data('id_row_brand');

        if (!$('#checkbox_for_row_' + id_row_brand).is(':checked')) {
            $('#checkbox_for_row_' + id_row_brand).prop('checked', true);
        } else {
            $('#checkbox_for_row_' + id_row_brand).prop('checked', false);
        }
    });

    $("#form_merge_brands_in_brands_list .btn_merge_brands").click(function (event) {
        var result = new Array();
        var str_ul = '<ul>';
        var str_select_1 = '';
        var str_select_2 = '<div> \
                             <input class="brand_for_description" name="brand_for_description" id="brand_for_desc_1" type="radio" value="" checked> \
                             <label for="brand_for_desc_1">Без описания</label> \
                            </div>';
        var counter_select_1 = 1;
        var counter_select_2 = 2;


        $('#form_merge_brands_in_brands_list .checkbox_brand:checked').each(function () {
            str_ul = str_ul + '<li>' + $(this).data('brand_name') + '</li>';

            str_select_1 = str_select_1 + '<div> \
                                            <input class="name_group" name="name_group" id="name_group_' + counter_select_1 + '" type="radio" value="' + escapeHtml($(this).val()) + '"> \
                                            <label for="name_group_' + counter_select_1 + '">' + $(this).val() + '</label> \
                                           </div>';

            str_select_2 = str_select_2 + '<div> \
                                            <input class="brand_for_description" name="brand_for_description" id="brand_for_desc_' + counter_select_2 + '" type="radio" value="' + escapeHtml($(this).val()) + '"> \
                                            <label for="brand_for_desc_' + counter_select_2 + '">' + $(this).val() + '</label> \
                                           </div>';

            counter_select_1++;
            counter_select_2++;
            result.push($(this).val());
        });

        str_ul = str_ul + '</ul>';

        if (result.length > 1) {
            $('#pop_up_merge_brands .list_merge_brands').html(str_ul);
            $('#pop_up_merge_brands .select_name_group_brand').html(str_select_1);
            $('#pop_up_merge_brands .select_brand_for_description').html(str_select_2);

            $('#pop_up_merge_brands').modal('show');
            $('#form_name_group_in_brands_list .form_error').html('');
            $('#pop_up_merge_brands .selected_description_brand').html('');
        } else {
            alert('Выберите минимум пару брендов!');
        }
    });

    $(document).on("change", "#pop_up_merge_brands .brand_for_description", function () {
        var brand_for_description = $('#form_name_group_in_brands_list input:radio[name=brand_for_description]:checked').val();

        if (brand_for_description != '') {
            $.post(base_url + "index.php/search_detail/get_description_brand", {brand_for_description: brand_for_description}, function (event) {
                if (event.status_ajax == 'ok') {
                    var str = '';

                    if ((event.data_brand.country !== null) && (event.data_brand.country != '')) {
                        var content_country = event.data_brand.country;
                    } else {
                        var content_country = 'Нет';
                    }

                    if ((event.data_brand.link !== null) && (event.data_brand.link != '')) {
                        var content_link = event.data_brand.link;
                    } else {
                        var content_link = 'Нет';
                    }

                    if ((event.data_brand.description !== null) && (event.data_brand.description != '')) {
                        var content_description = event.data_brand.description;
                    } else {
                        var content_description = 'Нет';
                    }

                    str = str + '<div> \
                                    <strong>Страна</strong> \
                                    <p>' + content_country + '</p> \
                                </div>';

                    str = str + '<div> \
                                    <strong>Ссылка</strong> \
                                    <p>' + content_link + '</p> \
                                </div>';

                    str = str + '<div> \
                                    <strong>Описание</strong> \
                                    <p>' + content_description + '</p> \
                                </div>';

                    $('#pop_up_merge_brands .selected_description_brand').html(str)
                } else {
                    $('#pop_up_merge_brands .selected_description_brand').html('<strong class="empty_description_brand">Описания нет!</strong>');
                }
            }, 'json');
        } else {
            $('#pop_up_merge_brands .selected_description_brand').html('');
        }
    });

    $("#pop_up_merge_brands .btn_name_group").click(function (event) {
        var article_search = $('#form_merge_brands_in_brands_list #article_search').val();
        var name_group = $('#form_name_group_in_brands_list input:radio[name=name_group]:checked').val();
        var brand_for_description = $('#form_name_group_in_brands_list input:radio[name=brand_for_description]:checked').val();
        var private = $('#form_merge_brands_in_brands_list #private').val();

        if (typeof name_group == "undefined") {
            $('#form_name_group_in_brands_list .form_error').html('Выберите название для группы').show(0).delay(7000).hide(0);
        } else {
            var full_data = $('#form_merge_brands_in_brands_list').serializeArray();
            full_data.push({name: "name_group", value: name_group});
            full_data.push({name: "brand_for_description", value: brand_for_description});

            $.post(base_url + "index.php/search_detail/merge_brands_in_brand_list", $.param(full_data), function (event) {
                if (event.status == 'error') {
                    $('#form_name_group_in_brands_list .form_error').html(event.message).show(0).delay(7000).hide(0);
                } else {
                    alert(event.message);
                    document.location.href = base_url + 'index.php/search_detail/list_brands/' + article_search + '/' + private;
                }
            }, 'json');
        }
    });

    /* Скрипты для объединения брендов на странице со списком брендов (КОНЕЦ) */

    /* Скрипты для объединения брендов на странице с найденными деталями (НАЧАЛО) */

    $("#form_merge_brands_for_list_details .brand_row").click(function (event) {
        $(this).toggleClass('selected_brand');
        var id_row_brand = $(this).data('id_row_brand');

        if (!$('#checkbox_for_row_' + id_row_brand).is(':checked')) {
            $('#checkbox_for_row_' + id_row_brand).prop('checked', true);
        } else {
            $('#checkbox_for_row_' + id_row_brand).prop('checked', false);
        }
    });

    $("#form_merge_brands_for_list_details .btn_merge_brands").click(function (event) {
        var result = new Array();
        var list_selected_articles = new Array();
        var str_ul = '<ul>';
        var str_select_1 = '';
        var str_select_2 = '<div> \
                             <input class="brand_for_description" name="brand_for_description" id="brand_for_desc_1" type="radio" value="" checked> \
                             <label for="brand_for_desc_1">Без описания</label> \
                            </div>';
        var counter_select_1 = 1;
        var counter_select_2 = 2;

        $('#form_merge_brands_for_list_details .checkbox_brand:checked').each(function () {
            str_ul = str_ul + '<li>' + $(this).data('brand_name') + ' (' + $(this).data('brand_type') + ')</li>';

            str_select_1 = str_select_1 + '<div> \
                                            <input class="name_group" name="name_group" id="name_group_' + counter_select_1 + '" type="radio" value="' + escapeHtml($(this).data('brand_name')) + '"> \
                                            <label for="name_group_' + counter_select_1 + '">' + $(this).data('brand_name') + '</label> \
                                           </div>';

            str_select_2 = str_select_2 + '<div> \
                                            <input class="brand_for_description" name="brand_for_description" id="brand_for_desc_' + counter_select_2 + '" type="radio" value="' + escapeHtml($(this).data('brand_name')) + '"> \
                                            <label for="brand_for_desc_' + counter_select_2 + '">' + $(this).data('brand_name') + '</label> \
                                           </div>';

            counter_select_1++;
            counter_select_2++;
            result.push($(this).val());
            list_selected_articles.push($(this).data('article'));
        });

        str_ul = str_ul + '</ul>';

        var unique_articles = array_unique(list_selected_articles);

        if (result.length < 1) {
            alert('Выберите минимум пару брендов!');
        } else if (unique_articles.length > 0) {
            alert('В выбранном списке есть бренды с разными артикулами');
        } else {
            $('#pop_up_merge_brands_for_list_details .list_merge_brands').html(str_ul);
            $('#pop_up_merge_brands_for_list_details .select_name_group_brand').html(str_select_1);
            $('#pop_up_merge_brands_for_list_details .select_brand_for_description').html(str_select_2);

            $('#pop_up_merge_brands_for_list_details').modal('show');
            $('#form_name_group_in_brands_list .form_error').html('');
            $('#pop_up_merge_brands_for_list_details .selected_description_brand').html('');
        }
    });

    $(document).on("change", "#pop_up_merge_brands_for_list_details .brand_for_description", function () {
        var brand_for_description = $('#form_name_group_for_list_details input:radio[name=brand_for_description]:checked').val();

        if (brand_for_description != '') {
            $.post(base_url + "index.php/search_detail/get_description_brand", {brand_for_description: brand_for_description}, function (event) {
                if (event.status_ajax == 'ok') {
                    var str = '';

                    if ((event.data_brand.country !== null) && (event.data_brand.country != '')) {
                        var content_country = event.data_brand.country;
                    } else {
                        var content_country = 'Нет';
                    }

                    if ((event.data_brand.link !== null) && (event.data_brand.link != '')) {
                        var content_link = event.data_brand.link;
                    } else {
                        var content_link = 'Нет';
                    }

                    if ((event.data_brand.description !== null) && (event.data_brand.description != '')) {
                        var content_description = event.data_brand.description;
                    } else {
                        var content_description = 'Нет';
                    }

                    str = str + '<div> \
                                    <strong>Страна</strong> \
                                    <p>' + content_country + '</p> \
                                </div>';

                    str = str + '<div> \
                                    <strong>Ссылка</strong> \
                                    <p>' + content_link + '</p> \
                                </div>';

                    str = str + '<div> \
                                    <strong>Описание</strong> \
                                    <p>' + content_description + '</p> \
                                </div>';

                    $('#pop_up_merge_brands_for_list_details .selected_description_brand').html(str)
                } else {
                    $('#pop_up_merge_brands_for_list_details .selected_description_brand').html('<strong class="empty_description_brand">Описания нет!</strong>');
                }
            }, 'json');
        } else {
            $('#pop_up_merge_brands_for_list_details .selected_description_brand').html('');
        }
    });

    $("#pop_up_merge_brands_for_list_details .btn_name_group").click(function (event) {
        var name_group = $('#form_name_group_for_list_details input:radio[name=name_group]:checked').val();
        var brand_for_description = $('#form_name_group_for_list_details input:radio[name=brand_for_description]:checked').val();
        var article_search = $('#form_merge_brands_for_list_details #article_search').val();
        var key_group_search = $('#form_merge_brands_for_list_details #key_group_search').val();
        var private = $('#form_merge_brands_for_list_details #private').val();

        if (typeof name_group == "undefined") {
            $('#form_name_group_for_list_details .form_error').html('Выберите название для группы').show(0).delay(7000).hide(0);
        } else {
            var full_data = $('#form_merge_brands_for_list_details').serializeArray();
            full_data.push({name: "name_group", value: name_group});
            full_data.push({name: "brand_for_description", value: brand_for_description});

            $.post(base_url + "index.php/search_detail/merge_brands_for_list_details", $.param(full_data), function (event) {
                if (event.status == 'error') {
                    $('#form_name_group_for_list_details .form_error').html(event.message).show(0).delay(7000).hide(0);
                } else {
                    alert(event.message);
                    document.location.href = base_url + 'index.php/search_detail/list_details/' + article_search + '/' + key_group_search + '?private_data=' + private;
                }
            }, 'json');
        }
    });

    /* Скрипты для объединения брендов на странице с найденными деталями (КОНЕЦ) */

    /* Скрипты для редактирования данных бренда (НАЧАЛО) */

    $('.open_wind_edit_brand').click(function (event) {
        event.stopPropagation();

        var id_brand = $(this).data('id_brand');

        $('.id_form_edit_brand').val(id_brand);

        $('#form_edit_brand').trigger('reset');

        $('.form_error').html('');

        $('#pop_up_edit_brand').modal('show');
    });

    $("#form_edit_brand").submit(function (event) {
        event.preventDefault();
        $('.form_error').html('');

        var id_brand = $('#id_form_edit_brand').val();

        var description_brand = $('#description_form_edit_brand').val();
        var link_brand = $('#link_form_edit_brand').val();
        var country_brand = $('#country_form_edit_brand').val();
        var recommended = $('#recommended_form_edit_brand').val();

        if ((description_brand == '') && (link_brand == '') && (country_brand == '')) {
            $('.form_error').html('Заполните хоть одно поле формы');
        } else {
            $.post(base_url + "index.php/search_detail/edit_brand", $('#form_edit_brand').serialize(), function (event) {
                if (event.status_ajax == 'ok') {
                    $('.edit_icon_brand_' + id_brand).hide();

                    $('#form_edit_brand').trigger('reset');

                    $('.form_error').html('');

                    $('#pop_up_edit_brand').modal('hide');

                    if (recommended == 1) {
                        $('.icon_recommended_brand_' + id_brand).show();
                    }

                    var str = '';

                    if (description_brand != '') {
                        str = str + '<strong>Описание</strong>:<br>' + description_brand + '<br>';
                    }

                    if (country_brand != '') {
                        str = str + '<strong>Страна</strong>:<br>' + country_brand + '<br>';
                    }

                    if (link_brand != '') {
                        str = str + '<strong>Ссылка</strong>:<br><a href=\'http://' + link_brand + '\' target=\'_blank\'>' + link_brand + '</a><br>';
                    }

                    $('.name_brand_' + id_brand).popover({
                        'trigger': "click",
                        'title': 'Информация о бренде',
                        'placement': 'bottom',
                        'html': true,
                        'container': 'body',
                        'template': '<div class="popover popover_inf_brand_style"> \
                                        <div class="arrow"></div> \
                                        <div class="popover-inner"> \
                                                <h3 class="popover-title"></h3> \
                                                <div class="popover-content"> \
                                                        <p></p> \
                                                </div> \
                                        </div> \
                                    </div>',
                        'content': "<div>" + str + "</div>",
                    }).click(function (event) {
                        event.stopPropagation();
                    });

                    alert('Данные бренда успешно изменены');
                } else if (event.status_ajax == 'error_empty_form') {
                    $('.form_error').html('Заполните хоть одно поле формы');
                } else {
                    $('.form_error').html('Произошла неизвестная ошибка');
                }
            }, 'json');
        }
    });

    /* Скрипты для редактирования данных бренда (КОНЕЦ) */

    /* Скрипт для удаления лишнего предложения (НАЧАЛО) */

    $('#form_selected_offers .delete_selected_offers').click(function (event) {
        if (confirm("Вы уверены что хотите удалить?")) {
            var id_selected_offer = $(this).data('id_selected_offer');

            $('#form_selected_offers #checkbox_for_selected_offer_' + id_selected_offer).prop('checked', false);

            $('#form_selected_offers').submit();
        }
    });

    /* Скрипт для удаления лишнего предложения (КОНЕЦ) */

    /* Валидация формы авторизации (НАЧАЛО) */

    $("#form_login_project").submit(function (event) {
        $('.form_error').html('');

        var error_flag = 0;

        if ($('#email_login').val() == '') {
            $('.form_error').append('<p>Заполните поле "Email"</p>');
            error_flag++;
        }

        if ($('#password_login').val() == '') {
            $('.form_error').append('<p>Заполните поле "Пароль"</p>');
            error_flag++;
        }

        if ($('#password_login').val().length < 6 && $('#password_login').val() != '') {
            $('.form_error').append('<p>Длина поля "Пароль" не меньше 6 символов</p>');
            error_flag++;
        }

        if (error_flag != 0) {
            return false
        }
    });

    /* Валидация формы авторизации (КОНЕЦ) */

    /* Скрипты для списка выбранных деталей (НАЧАЛО) */

    $('#form_selected_offers .img_minus_blue').click(function (event) {
        var element = $(this);
        var id_offer = element.data('id_offer');
        var count_items = $('#form_selected_offers .quantity_for_offer_' + id_offer).text();

        if (count_items != 0) {
            var new_count_items = Number(count_items) - 1;

            var count_items = $('#form_selected_offers .quantity_for_offer_' + id_offer).html(new_count_items);
        }
    });

    $('#form_selected_offers .img_plus_blue').click(function (event) {
        var element = $(this);
        var id_offer = element.data('id_offer');
        var count_items = $('#form_selected_offers .quantity_for_offer_' + id_offer).text();

        var new_count_items = Number(count_items) + 1;

        var count_items = $('#form_selected_offers .quantity_for_offer_' + id_offer).html(new_count_items);
    });

    /* Скрипты для списка выбранных деталей (КОНЕЦ) */
});

function array_unique(A) {
    var n = A.length, B = [];
    for (var i = 1, j = 0, t; i < n + 1; i++)
    {
        if (A[i - 1] === A[ i ])
            t = A[i - 1];
        if (A[i - 1] !== t)
            B[j++] = A[i - 1];
    }

    return B;
}

function escapeHtml(text) {
    return text
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
}

function filter_by_brand() {
    var list_brands = [];

    $.each($(".filter_by_brand input[type='checkbox']:checked:not(:first)"), function () {
        list_brands.push($(this).val());
    });

    if (list_brands.length !== 0) {
        list_brands.push($(".filter_by_brand input[type='checkbox']").first().val());
        $('.search_list_details .row_offer').hide();
        $('.search_list_details .show_detail_offers').hide();

        $.each(list_brands, function (index, value) {
            $('.search_list_details .' + value).show();
        });
    } else {
        $('.search_list_details .row_offer').show();
        $('.search_list_details .show_detail_offers').show();
    }

    $('.search_list_details .row_offer').each(function () {
        if ($(this).hasClass('hide_offer')) {
            $(this).hide();
        }
    });

    $('.search_list_details .show_list_offers').show();
    $('.search_list_details .hide_list_offers').hide();
}

function show_popover_for_list_details() {
    $('#form_list_details .popover_type_warehouse').popover({
        'trigger': "hover",
        'title': 'Информация о складе',
        'placement': 'bottom',
        'html': true,
        'template': '<div class="popover popover_type_warehouse_style"> \
                        <div class="arrow"></div> \
                        <div class="popover-inner"> \
                                <h3 class="popover-title"></h3> \
                                <div class="popover-content"> \
                                        <p></p> \
                                </div> \
                        </div> \
                    </div>',
    });

    $('#form_list_details .popover_with_description_for_type_warehouse').popover({
        'trigger': "hover",
        'title': 'Информация о доставке',
        'placement': 'bottom',
        'html': true,
        'template': '<div class="popover popover_type_warehouse_style"> \
                        <div class="arrow"></div> \
                        <div class="popover-inner"> \
                                <h3 class="popover-title"></h3> \
                                <div class="popover-content"> \
                                        <p></p> \
                                </div> \
                        </div> \
                    </div>',
    });

    $('#form_list_details .popover_inf_brand').popover({
        'trigger': "click",
        'title': 'Информация о бренде',
        'placement': 'bottom',
        'html': true,
        'container': 'body',
        'template': '<div class="popover popover_inf_brand_style"> \
                        <div class="arrow"></div> \
                        <div class="popover-inner"> \
                                <h3 class="popover-title"></h3> \
                                <div class="popover-content"> \
                                        <p></p> \
                                </div> \
                        </div> \
                    </div>',
    }).click(function (event) {
        event.stopPropagation();
    });

    $('#form_list_details .popover_list_provider_rating').popover({
        'trigger': 'hover',
        'title': 'Список поставщиков',
        'placement': 'bottom',
        'html': true,
        'container': 'body',
        'template': '<div class="popover popover_list_provider_rating_style"> \
                        <div class="arrow"></div> \
                        <div class="popover-inner"> \
                                <h3 class="popover-title"></h3> \
                                <div class="popover-content"> \
                                        <p></p> \
                                </div> \
                        </div> \
                    </div>',
    }).click(function (event) {
        event.stopPropagation();
    });
}

function ajax_send_filter_for_list_details(article, key_group, intervals_deliv_period, price_between_beg, price_between_end, search_is_complete, type_sort, type_warehouse, recom_brands, private_data) {
    $('.div_top_filter_list_details .list_details').html('<div class="div_img_loader_search_detail"> \
                                                            <img src="' + base_url + 'images/loader.gif" class="img_loader_search_detail" title="Идет загрузка"> \
                                                            </div>');

    var post_data = {
        intervals_deliv_period: intervals_deliv_period,
        article: article,
        key_group: key_group,
        price_between_beg: price_between_beg,
        price_between_end: price_between_end,
        is_complete: search_is_complete,
        type_sort: type_sort,
        type_warehouse: type_warehouse,
        recom_brands: recom_brands,
        private_data: private_data
    }

    $.post(base_url + "index.php/search_detail/filter_for_list_details", post_data, function (event) {
        if (event.status == 'ok') {
            $('.div_top_filter_list_details .list_details').html(event.template_list_details);
            show_popover_for_list_details();
            filter_by_brand();
        }
    }, 'json');
}