$(function () {

    /* Скрипты для раздела "Заказы (Менеджер) (НАЧАЛО)" */

    $('.list_orders_widget .discount_value').click(function () {
        hide_all_inputs_change_order();
        var element = $(this);
        var id_order = element.data('id_order');

        element.hide();
        $('#row_order_' + id_order + ' .discount_input').show();
        $('#row_order_' + id_order + ' .img_discount_change').show();
    });

    $('.list_orders_widget .img_discount_change').click(function () {
        var element = $(this);
        var id_order = element.data('id_order');
        var new_discount = $('#row_order_' + id_order + ' .discount_input').val();

        $.post(base_url + "index.php/orders_widget/change_data_order", {id_order: id_order, type: 'discount', new_discount: new_discount}, function (event) {
            if (event.status == 'ok') {
                $('#row_order_' + id_order + ' .discount_input').hide();
                $('#row_order_' + id_order + ' .img_discount_change').hide();
                $('#row_order_' + id_order + ' .discount_input').val(new_discount);
                $('#row_order_' + id_order + ' .discount_value').html(new_discount).show();

                $('#row_order_' + id_order + ' .td_total_sum').html(event.total_sum);
                $('#row_order_' + id_order + ' .td_profit').html(event.profit);

                $('#row_order_' + id_order + ' .td_profit').removeClass('profit_bad').removeClass('profit_good');

                if (event.profit < 0) {
                    $('#row_order_' + id_order + ' .td_profit').addClass('profit_bad');
                } else {
                    $('#row_order_' + id_order + ' .td_profit').addClass('profit_good');
                }

                element.parent().find('.img_is_changed').show();

                alert(event.message);
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $('.list_orders_widget .percent_value').click(function () {
        hide_all_inputs_change_order();
        var element = $(this);
        var id_order = element.data('id_order');

        element.hide();
        $('#row_order_' + id_order + ' .percent_input').show();
        $('#row_order_' + id_order + ' .img_percent_change').show();
    });

    $('.list_orders_widget .img_percent_change').click(function () {
        var element = $(this);
        var id_order = element.data('id_order');
        var new_percent = $('#row_order_' + id_order + ' .percent_input').val();

        $.post(base_url + "index.php/orders_widget/change_data_order", {id_order: id_order, type: 'percent', new_percent: new_percent}, function (event) {
            if (event.status == 'ok') {
                $('#row_order_' + id_order + ' .percent_input').hide();
                $('#row_order_' + id_order + ' .img_percent_change').hide();
                $('#row_order_' + id_order + ' .percent_input').val(new_percent);
                $('#row_order_' + id_order + ' .percent_value').html(new_percent).show();

                $('#row_order_' + id_order + ' .td_total_sum').html(event.total_sum);
                $('#row_order_' + id_order + ' .td_profit').html(event.profit);

                $('#row_order_' + id_order + ' .td_profit').removeClass('profit_bad').removeClass('profit_good');

                if (event.profit < 0) {
                    $('#row_order_' + id_order + ' .td_profit').addClass('profit_bad');
                } else {
                    $('#row_order_' + id_order + ' .td_profit').addClass('profit_good');
                }

                element.parent().find('.img_is_changed').show();

                alert(event.message);
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $('.list_orders_widget .delivery_value').click(function () {
        hide_all_inputs_change_order();
        var element = $(this);
        var id_order = element.data('id_order');

        element.hide();
        $('#row_order_' + id_order + ' .delivery_input').show();
        $('#row_order_' + id_order + ' .img_delivery_change').show();
    });

    $('.list_orders_widget .img_delivery_change').click(function () {
        var element = $(this);
        var id_order = element.data('id_order');
        var new_delivery = $('#row_order_' + id_order + ' .delivery_input').val();

        $.post(base_url + "index.php/orders_widget/change_data_order", {id_order: id_order, type: 'delivery', new_delivery: new_delivery}, function (event) {
            if (event.status == 'ok') {
                $('#row_order_' + id_order + ' .delivery_input').hide();
                $('#row_order_' + id_order + ' .img_delivery_change').hide();
                $('#row_order_' + id_order + ' .delivery_input').val(new_delivery);
                $('#row_order_' + id_order + ' .delivery_value').html(new_delivery).show();
                element.parent().find('.img_is_changed').show();

                alert(event.message);
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $('.list_orders_widget .img_is_changed').click(function () {
        var element = $(this);
        var id_order = element.data('id_order');
        var type = element.data('type');

        $.post(base_url + "index.php/orders_widget/reset_change_order", {id_order: id_order, type: type}, function (event) {
            if (event.status == 'ok') {
                element.hide();
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    /* Скрипт удаления заказа (Виджет) */

    $(".list_orders_widget .delete_order").click(function (event) {
        if (confirm("Вы уверены что хотите удалить?")) {
            var element = $(this);
            var id_amo_crm = element.data('id_amo_crm');

            $.post(base_url + "index.php/orders_widget/delete_order", {id_amo_crm: id_amo_crm}, function (event) {
                if (event.status == 'ok') {
                    $('#row_order_' + event.id_order).fadeOut(300);
                } else {
                    alert(event.message);
                }
            }, "json");
        }
    });

    /* Скрипт удаления детали из заказа (Виджет) */

    $(".list_details_in_order_widget .delete_detail").click(function (event) {
        if (confirm("Вы уверены что хотите удалить?")) {
            var element = $(this);
            var id_detail = element.data('id_detail');

            $.post(base_url + "index.php/orders_widget/delete_detail", {id_detail: id_detail}, function (event) {
                if (event.status == 'del_detail_ok') {
                    $('#row_detail_' + id_detail).fadeOut(300);
                } else if (event.status == 'del_order_ok') {
                    alert(event.message);

                    document.location.href = '/index.php/orders_widget/list_orders';
                } else {
                    alert(event.message);
                }
            }, "json");
        }
    });

    $(".list_details_in_order_widget .detail_is_ordered").click(function (event) {
        var element = $(this);
        var id_detail = element.data('id_detail');
        var is_ordered = element.data('is_ordered');

        $.post(base_url + "index.php/orders_widget/change_status_detail", {id_detail: id_detail, is_ordered: is_ordered}, function (event) {
            if (event.status == 'ok') {
                if (is_ordered == 1) {
                    element.html('Заказать').data('is_ordered', 0);
                } else {
                    element.html('Заказано').data('is_ordered', 1);
                }
            } else {
                alert(event.message);
            }
        }, "json");
    });

    $('.list_details_in_order_widget .vendor_code_value').click(function () {
        hide_all_inputs_change_detail();
        var element = $(this);
        var id_detail = element.data('id_detail');

        element.hide();
        $('#row_detail_' + id_detail + ' .vendor_code_input').show();
        $('#row_detail_' + id_detail + ' .img_vendor_code_change').show();
    });

    $('.list_details_in_order_widget .img_vendor_code_change').click(function () {
        var element = $(this);
        var id_detail = element.data('id_detail');
        var new_vendor_code = $('#row_detail_' + id_detail + ' .vendor_code_input').val();

        $.post(base_url + "index.php/orders_widget/change_data_detail", {id_detail: id_detail, type: 'vendor_code', new_vendor_code: new_vendor_code}, function (event) {
            if (event.status == 'ok') {
                $('#row_detail_' + id_detail + ' .vendor_code_input').hide();
                $('#row_detail_' + id_detail + ' .img_vendor_code_change').hide();
                $('#row_detail_' + id_detail + ' .vendor_code_input').val(new_vendor_code);
                $('#row_detail_' + id_detail + ' .vendor_code_value').html(new_vendor_code).show();
                element.parent().find('.img_is_changed').show();

                alert(event.message);
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $('.list_details_in_order_widget .brand_value').click(function () {
        hide_all_inputs_change_detail();
        var element = $(this);
        var id_detail = element.data('id_detail');

        element.hide();
        $('#row_detail_' + id_detail + ' .brand_input').show();
        $('#row_detail_' + id_detail + ' .img_brand_change').show();
    });

    $('.list_details_in_order_widget .img_brand_change').click(function () {
        var element = $(this);
        var id_detail = element.data('id_detail');
        var new_brand = $('#row_detail_' + id_detail + ' .brand_input').val();

        $.post(base_url + "index.php/orders_widget/change_data_detail", {id_detail: id_detail, type: 'brand', new_brand: new_brand}, function (event) {
            if (event.status == 'ok') {
                $('#row_detail_' + id_detail + ' .brand_input').hide();
                $('#row_detail_' + id_detail + ' .img_brand_change').hide();
                $('#row_detail_' + id_detail + ' .brand_input').val(new_brand);
                $('#row_detail_' + id_detail + ' .brand_value').html(new_brand).show();
                element.parent().find('.img_is_changed').show();

                alert(event.message);
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $('.list_details_in_order_widget .name_value').click(function () {
        hide_all_inputs_change_detail();
        var element = $(this);
        var id_detail = element.data('id_detail');

        element.hide();
        $('#row_detail_' + id_detail + ' .cell_name').css('white-space', 'nowrap');
        $('#row_detail_' + id_detail + ' .name_input').show();
        $('#row_detail_' + id_detail + ' .img_name_change').show();
    });

    $('.list_details_in_order_widget .img_name_change').click(function () {
        var element = $(this);
        var id_detail = element.data('id_detail');
        var new_name = $('#row_detail_' + id_detail + ' .name_input').val();

        $.post(base_url + "index.php/orders_widget/change_data_detail", {id_detail: id_detail, type: 'name', new_name: new_name}, function (event) {
            if (event.status == 'ok') {
                $('#row_detail_' + id_detail + ' .cell_name').css('white-space', '');
                $('#row_detail_' + id_detail + ' .name_input').hide();
                $('#row_detail_' + id_detail + ' .img_name_change').hide();
                $('#row_detail_' + id_detail + ' .name_input').val(new_name);
                $('#row_detail_' + id_detail + ' .name_value').html(new_name).show();
                element.parent().find('.img_is_changed').show();

                alert(event.message);
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $('.list_details_in_order_widget .provider_value').click(function () {
        hide_all_inputs_change_detail();
        var element = $(this);
        var id_detail = element.data('id_detail');

        element.hide();
        $('#row_detail_' + id_detail + ' .provider_input').show();
        $('#row_detail_' + id_detail + ' .img_provider_change').show();
    });

    $('.list_details_in_order_widget .img_provider_change').click(function () {
        var element = $(this);
        var id_detail = element.data('id_detail');
        var new_provider = $('#row_detail_' + id_detail + ' .provider_input').val();

        $.post(base_url + "index.php/orders_widget/change_data_detail", {id_detail: id_detail, type: 'provider', new_provider: new_provider}, function (event) {
            if (event.status == 'ok') {
                $('#row_detail_' + id_detail + ' .provider_input').hide();
                $('#row_detail_' + id_detail + ' .img_provider_change').hide();
                $('#row_detail_' + id_detail + ' .provider_input').val(new_provider);
                $('#row_detail_' + id_detail + ' .provider_value').html(new_provider).show();
                element.parent().find('.img_is_changed').show();

                alert(event.message);
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $('.list_details_in_order_widget .request_price_value').click(function () {
        hide_all_inputs_change_detail();
        var element = $(this);
        var id_detail = element.data('id_detail');

        element.hide();
        $('#row_detail_' + id_detail + ' .request_price_input').show();
        $('#row_detail_' + id_detail + ' .img_request_price_change').show();
    });

    $('.list_details_in_order_widget .img_request_price_change').click(function () {
        var element = $(this);
        var id_detail = element.data('id_detail');
        var new_request_price = $('#row_detail_' + id_detail + ' .request_price_input').val();

        $.post(base_url + "index.php/orders_widget/change_data_detail", {id_detail: id_detail, type: 'request_price', new_request_price: new_request_price}, function (event) {
            if (event.status == 'ok') {
                $('#row_detail_' + id_detail + ' .request_price_input').hide();
                $('#row_detail_' + id_detail + ' .img_request_price_change').hide();
                $('#row_detail_' + id_detail + ' .request_price_input').val(new_request_price);
                $('#row_detail_' + id_detail + ' .request_price_value').html(new_request_price).show();
                $('#row_detail_' + id_detail + ' .need_calculate_retail_price').show();
                element.parent().find('.img_is_changed').show();

                alert(event.message);
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $('.list_details_in_order_widget .retail_price_value').click(function () {
        hide_all_inputs_change_detail();
        var element = $(this);
        var id_detail = element.data('id_detail');

        element.hide();
        $('#row_detail_' + id_detail + ' .retail_price_input').show();
        $('#row_detail_' + id_detail + ' .img_retail_price_change').show();
    });

    $('.list_details_in_order_widget .img_retail_price_change').click(function () {
        var element = $(this);
        var id_detail = element.data('id_detail');
        var new_retail_price = $('#row_detail_' + id_detail + ' .retail_price_input').val();

        $.post(base_url + "index.php/orders_widget/change_data_detail", {id_detail: id_detail, type: 'retail_price', new_retail_price: new_retail_price}, function (event) {
            if (event.status == 'ok') {
                $('#row_detail_' + id_detail + ' .retail_price_input').hide();
                $('#row_detail_' + id_detail + ' .img_retail_price_change').hide();
                $('#row_detail_' + id_detail + ' .retail_price_input').val(new_retail_price);
                $('#row_detail_' + id_detail + ' .retail_price_value').html(new_retail_price).show();
                element.parent().find('.img_is_changed').show();

                alert(event.message);
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $('.list_details_in_order_widget .quantity_value').click(function () {
        hide_all_inputs_change_detail();
        var element = $(this);
        var id_detail = element.data('id_detail');

        element.hide();
        $('#row_detail_' + id_detail + ' .quantity_input').show();
        $('#row_detail_' + id_detail + ' .img_quantity_change').show();
    });

    $('.list_details_in_order_widget .img_quantity_change').click(function () {
        var element = $(this);
        var id_detail = element.data('id_detail');
        var new_quantity = $('#row_detail_' + id_detail + ' .quantity_input').val();

        $.post(base_url + "index.php/orders_widget/change_data_detail", {id_detail: id_detail, type: 'quantity', new_quantity: new_quantity}, function (event) {
            if (event.status == 'ok') {
                $('#row_detail_' + id_detail + ' .quantity_input').hide();
                $('#row_detail_' + id_detail + ' .img_quantity_change').hide();
                $('#row_detail_' + id_detail + ' .quantity_input').val(new_quantity);
                $('#row_detail_' + id_detail + ' .quantity_value').html(new_quantity).show();
                element.parent().find('.img_is_changed').show();

                alert(event.message);
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    $('.list_details_in_order_widget .img_is_changed').click(function () {
        var element = $(this);
        var id_detail = element.data('id_detail');
        var type = element.data('type');

        $.post(base_url + "index.php/orders_widget/reset_change_detail", {id_detail: id_detail, type: type}, function (event) {
            if (event.status == 'ok') {
                element.hide();
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    var pathname = window.location.pathname;
    if ((pathname.search(/orders_widget\/list_orders/i) >= 0)
            || (pathname.search(/orders_widget\/search_order/i) >= 0)) {
        $('html').click(function (e) {
            if (!$(e.target).is(".list_orders_widget .discount_value")
                    && !$(e.target).is(".list_orders_widget .img_discount_change") && !$(e.target).is(".list_orders_widget .discount_input")) {
                $('.list_orders_widget .discount_value').show();
                $('.list_orders_widget .discount_input').hide();
                $('.list_orders_widget .img_discount_change').hide();
            }

            if (!$(e.target).is(".list_orders_widget .delivery_value")
                    && !$(e.target).is(".list_orders_widget .img_delivery_change") && !$(e.target).is(".list_orders_widget .delivery_input")) {
                $(".list_orders_widget .delivery_value").show();
                $(".list_orders_widget .delivery_input").hide();
                $(".list_orders_widget .img_delivery_change").hide();
            }

            if (!$(e.target).is(".list_orders_widget .percent_value")
                    && !$(e.target).is(".list_orders_widget .img_percent_change") && !$(e.target).is(".list_orders_widget .percent_input")) {
                $(".list_orders_widget .percent_value").show();
                $(".list_orders_widget .percent_input").hide();
                $(".list_orders_widget .img_percent_change").hide();
            }
        });
    }

    if ((pathname.search(/orders_widget\/list_details/i) >= 0)
            || (pathname.search(/orders_widget\/search_detail/i) >= 0)) {
        $('html').click(function (e) {
            if (!$(e.target).is(".list_details_in_order_widget .vendor_code_value")
                    && !$(e.target).is(".list_details_in_order_widget .img_vendor_code_change") && !$(e.target).is(".list_details_in_order_widget .vendor_code_input")) {
                $('.list_details_in_order_widget .vendor_code_value').show();
                $('.list_details_in_order_widget .vendor_code_input').hide();
                $('.list_details_in_order_widget .img_vendor_code_change').hide();
            }

            if (!$(e.target).is(".list_details_in_order_widget .brand_value")
                    && !$(e.target).is(".list_details_in_order_widget .img_brand_change") && !$(e.target).is(".list_details_in_order_widget .brand_input")) {
                $('.list_details_in_order_widget .brand_value').show();
                $('.list_details_in_order_widget .brand_input').hide();
                $('.list_details_in_order_widget .img_brand_change').hide();
            }

            if (!$(e.target).is(".list_details_in_order_widget .name_value")
                    && !$(e.target).is(".list_details_in_order_widget .img_name_change") && !$(e.target).is(".list_details_in_order_widget .name_input")) {
                $('.list_details_in_order_widget .cell_name').css('white-space', '');
                $('.list_details_in_order_widget .name_value').show();
                $('.list_details_in_order_widget .name_input').hide();
                $('.list_details_in_order_widget .img_name_change').hide();
            }

            if (!$(e.target).is(".list_details_in_order_widget .provider_value")
                    && !$(e.target).is(".list_details_in_order_widget .img_provider_change") && !$(e.target).is(".list_details_in_order_widget .provider_input")) {
                $('.list_details_in_order_widget .provider_value').show();
                $('.list_details_in_order_widget .provider_input').hide();
                $('.list_details_in_order_widget .img_provider_change').hide();
            }

            if (!$(e.target).is(".list_details_in_order_widget .request_price_value")
                    && !$(e.target).is(".list_details_in_order_widget .img_request_price_change") && !$(e.target).is(".list_details_in_order_widget .request_price_input")) {
                $('.list_details_in_order_widget .request_price_value').show();
                $('.list_details_in_order_widget .request_price_input').hide();
                $('.list_details_in_order_widget .img_request_price_change').hide();
            }

            if (!$(e.target).is(".list_details_in_order_widget .retail_price_value")
                    && !$(e.target).is(".list_details_in_order_widget .img_retail_price_change") && !$(e.target).is(".list_details_in_order_widget .retail_price_input")) {
                $('.list_details_in_order_widget .retail_price_value').show();
                $('.list_details_in_order_widget .retail_price_input').hide();
                $('.list_details_in_order_widget .img_retail_price_change').hide();
            }

            if (!$(e.target).is(".list_details_in_order_widget .quantity_value")
                    && !$(e.target).is(".list_details_in_order_widget .img_quantity_change") && !$(e.target).is(".list_details_in_order_widget .quantity_input")) {
                $('.list_details_in_order_widget .quantity_value').show();
                $('.list_details_in_order_widget .quantity_input').hide();
                $('.list_details_in_order_widget .img_quantity_change').hide();
            }
        });
    }

    $('.list_details_in_order_widget .need_calculate_retail_price').click(function () {
        var element = $(this);
        var id_detail = element.data('id_detail');

        $.post(base_url + "index.php/orders_widget/calculate_retail_price", {id_detail: id_detail}, function (event) {
            if (event.status == 'ok') {
                element.hide();
                
                $('#row_detail_' + id_detail + ' .retail_price_value').val(event.new_retail_price);
                $('#row_detail_' + id_detail + ' .retail_price_value').html(event.new_retail_price.toFixed(2));
                element.parent().find('.img_is_changed').show();

                alert(event.message);
            } else {
                alert(event.message);
            }
        }, 'json');
    });

    /* Скрипты для раздела "Заказы (Менеджер) (КОНЕЦ)" */

});

function hide_all_inputs_change_detail() {
    $('.list_details_in_order_widget .vendor_code_value').show();
    $('.list_details_in_order_widget .vendor_code_input').hide();
    $('.list_details_in_order_widget .img_vendor_code_change').hide();

    $('.list_details_in_order_widget .brand_value').show();
    $('.list_details_in_order_widget .brand_input').hide();
    $('.list_details_in_order_widget .img_brand_change').hide();

    $('.list_details_in_order_widget .cell_name').css('white-space', '');
    $('.list_details_in_order_widget .name_value').show();
    $('.list_details_in_order_widget .name_input').hide();
    $('.list_details_in_order_widget .img_name_change').hide();

    $('.list_details_in_order_widget .provider_value').show();
    $('.list_details_in_order_widget .provider_input').hide();
    $('.list_details_in_order_widget .img_provider_change').hide();

    $('.list_details_in_order_widget .request_price_value').show();
    $('.list_details_in_order_widget .request_price_input').hide();
    $('.list_details_in_order_widget .img_request_price_change').hide();

    $('.list_details_in_order_widget .retail_price_value').show();
    $('.list_details_in_order_widget .retail_price_input').hide();
    $('.list_details_in_order_widget .img_retail_price_change').hide();

    $('.list_details_in_order_widget .quantity_value').show();
    $('.list_details_in_order_widget .quantity_input').hide();
    $('.list_details_in_order_widget .img_quantity_change').hide();
}

function hide_all_inputs_change_order() {
    $('.list_orders_widget .discount_value').show();
    $('.list_orders_widget .discount_input').hide();
    $('.list_orders_widget .img_discount_change').hide();

    $(".list_orders_widget .delivery_value").show();
    $(".list_orders_widget .delivery_input").hide();
    $(".list_orders_widget .img_delivery_change").hide();

    $(".list_orders_widget .percent_value").show();
    $(".list_orders_widget .percent_input").hide();
    $(".list_orders_widget .img_percent_change").hide();
}